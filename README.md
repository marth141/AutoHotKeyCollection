# My AutoHotKey Collection
This is a collection of AutoHotKey scripts to accomplish various tasks. This is a brief run down of the applications.
### Excel Copier
Excel Copier was designed with the intent to make it easy to merge two Excel documents. The reason for this is sometimes an employee is needing to move information from a prior company Excel file to a current and updated company Excel file. This application is able to target specific cells to set them equal to each other then uses the .ahk macro abilities to send commands to the Excel application to copy and paste full pages. It is able to utilize Excel COM communication for this job.

### Applications
The crowning jewel of applications is IEr6.00, my variant of the AutoHotKey application, IEr5.76 developed by Daniel Scholle. This application's forte is to hold information entered repeatedly by an employee to be pasted later using HotKeys. These macros are all in a nice GUI environment for ease of use and are stored to memory as a .ini file. My modifications were targetted for making the application more streamlined for users under a different team than Daniel Scholles intended team, which the application was originally developed for.

### Games
These scripts were used for some Flash games to make the game nearly automated. It made it easy to get too good at the game and left no more challenge. A cool script.