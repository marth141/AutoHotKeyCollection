; ### DECLARATION ###
; This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
; This code was declared final as of 06/03/2016.
; Version: 1.0.0
;
; This code was written to copy data from PreDesignInput and Temp from two different Excel documents.
; This is because the Utah Design Tool is still favored by Utah CAD designers.
; The electrical team users a different design tool, so to save some time, this script will
; automatically copy data from the Electrical design tool to the Utah design tool.

MsgBox, Pick a Source Excel File.

;### Opens Source Excel File ###
FileSelectFile, DesignTool1					
x1 := ComObjCreate("Excel.Application")		
TrayTip, Alert, I might take awhile. Excel is slow. If at anytime a popup comes up just say yes. It happens because I'm too fast.
x1.Workbooks.Open(DesignTool1)				
x1.Visible := True

SendInput, #{Left 1}
WinMove, ahk_id x1, 0, 30, 976, 1050

MsgBox, Pick a Destination Excel File.

;### Opens Destination Excel File ###
FileSelectFile, DesignTool2					
x2 := ComObjCreate("Excel.Application")		
TrayTip, Alert, You're so patient. <3
x2.Workbooks.Open(DesignTool2)				
x2.Visible := True

SendInput, #{Right 1}
WinMove, ahk_id x2, 964, 30, 956, 1050

x1.Sheets("Temp").Activate
x1.Sheets("Temp").Range("A1").Select
x2.Sheets("Temp").Activate
x2.Sheets("Temp").Range("A1").Select

TrayTip, Alert, Please take some time to review that the Temp pages are different. Check the service numbers.
Sleep 1000

x1.Sheets("PreDesignInput").Activate
x1.Sheets("PreDesignInput").Range("A1").Select
x2.Sheets("PreDesignInput").Activate
x2.Sheets("PreDesignInput").Range("A1").Select

TrayTip, Starting, I'm about to start doing real work. Hold on. DO NOT TOUCH ANYTHING!!! I should have blocked your inputs but if that doesn't work do not touch!

TrayTip, Starting, Do not touch your controls.


TrayTip, PreDesignInput, Doing this first.
x1.Sheets("PreDesignInput").Activate
x1.Range("A1").Select
x2.Sheets("PreDesignInput").Activate
x2.Range("A1").Select

; ### INTERCONNECTION ###
x2.Range("C3").Value := x1.Range("C3")
x2.Range("C4").Value := x1.Range("C4")
x2.Range("C5").Value2 := "Nathan Casados"
x2.Range("C6").Value := x1.Range("C6")
x2.Range("C7").Value := x1.Range("C7")
x2.Range("C8").Value := x1.Range("C8")
x2.Range("C14").Value := x1.Range("C14")
x2.Range("C15").Value := x1.Range("C15")
x2.Range("C16").Value := x1.Range("C16")
x2.Range("C17").Value := x1.Range("C17")
x2.Range("C18").Value := x1.Range("C18")
x2.Range("C19").Value := x1.Range("C19")
x2.Range("C22").Value := x1.Range("C22")
x2.Range("C23").Value := x1.Range("C23")
x2.Range("C42").Value := x1.Range("C42")
x2.Range("C43").Value := x1.Range("C43")
x2.Range("C44").Value := x1.Range("C44")
x2.Range("F11").Value := x1.Range("F11")
x2.Range("F12").Value := x1.Range("F12")
x2.Range("F13").Value := x1.Range("F13")
x2.Range("F14").Value := x1.Range("F14")
x2.Range("F15").Value := x1.Range("F15")
x2.Range("F16").Value := x1.Range("F16")
x2.Range("F17").Value := x1.Range("F17")
x2.Range("H11").Value := x1.Range("H11")
x2.Range("H12").Value := x1.Range("H12")
x2.Range("H13").Value := x1.Range("H13")

; ### DAILY ###
x2.Range("G21").Value := x1.Range("G21")
x2.Range("G22").Value := x1.Range("G22")
x2.Range("G23").Value := x1.Range("G23")
x2.Range("G24").Value := x1.Range("G24")
x2.Range("G25").Value := x1.Range("G25")
x2.Range("G26").Value := x1.Range("G26")
x2.Range("G27").Value := x1.Range("G27")
x2.Range("G28").Value := x1.Range("G28")
x2.Range("G29").Value := x1.Range("G29")
x2.Range("G30").Value := x1.Range("G30")
x2.Range("G31").Value := x1.Range("G31")
X2.Range("G32").Value := x1.Range("G32")

; ### MONTHLY ###
x2.Range("F21").Value := x1.Range("F21")
x2.Range("F22").Value := x1.Range("F22")
x2.Range("F23").Value := x1.Range("F23")
x2.Range("F24").Value := x1.Range("F24")
x2.Range("F25").Value := x1.Range("F25")
x2.Range("F26").Value := x1.Range("F26")
x2.Range("F27").Value := x1.Range("F27")
x2.Range("F28").Value := x1.Range("F28")
x2.Range("F29").Value := x1.Range("F29")
x2.Range("F30").Value := x1.Range("F30")
x2.Range("F31").Value := x1.Range("F31")
x2.Range("F32").Value := x1.Range("F32")

TrayTip, Temps, Starting this. This might take some time.

Send, !{Tab}
x1.Sheets("Temp").Activate
x2.Sheets("Temp").Activate
Sleep, 1000
x1.Sheets("Temp").Range("A1").Select
Sleep, 500
SendInput, ^a
Sleep, 100
SendInput, ^c
Sleep, 1000

Send, !{Tab}
Sleep, 1000
SendInput, ^{Home}
x2.Sheets("Temp").Range("A1").Select
Sleep, 500
SendInput, ^!v
SendInput, {Down 8}
Sleep, 1000
SendInput, {Enter}

;x1.Sheets("Temp").Activate
x1.Sheets("Temp").Range("A1").Select
;x2.Sheets("Temp").Activate
;x2.Sheets("Temp").Range("A1").Select

return
Exit