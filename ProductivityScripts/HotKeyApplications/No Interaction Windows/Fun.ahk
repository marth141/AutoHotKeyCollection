+f1::
;DetectHiddenWindows, on
WinSet, AlwaysOnTop, On, Floating for YouTube�
WinSet, Transparent, 20, Floating for YouTube�
WinSet, ExStyle, +0x20, Floating for YouTube�
return

+f2::
;DetectHiddenWindows, on
WinSet, AlwaysOnTop, Off, Floating for YouTube�
WinSet, ExStyle, -0x20, Floating for YouTube�
WinSet, Transparent, 200, Floating for YouTube�
Return

+f3::
;DetectHiddenWindows, on
WinSet, AlwaysOnTop, On, SnapTimer
WinSet, Transparent, 100, SnapTimer
WinSet, ExStyle, +0x20, SnapTimer
return

+f4::
;DetectHiddenWindows, on
WinSet, AlwaysOnTop, Off, SnapTimer
WinSet, ExStyle, -0x20, SnapTimer
WinSet, Transparent, 200, SnapTimer
Return

+f5::
;DetectHiddenWindows, on
WinSet, AlwaysOnTop, On, ahk_class ApplicationFrameWindow, Sticky Notes
WinSet, Transparent, 64, ahk_class ApplicationFrameWindow, Sticky Notes
WinSet, ExStyle, +0x20, ahk_class ApplicationFrameWindow, Sticky Notes
return

+f6::
;DetectHiddenWindows, on
WinSet, AlwaysOnTop, Off, ahk_class ApplicationFrameWindow, Sticky Notes
WinSet, ExStyle, -0x20, ahk_class ApplicationFrameWindow, Sticky Notes
WinSet, Transparent, 255, ahk_class ApplicationFrameWindow, Sticky Notes
Return

+f7::
;DetectHiddenWindows, on
WinSet, AlwaysOnTop, On, Task Manager
WinSet, Transparent, 100, Task Manager
WinSet, ExStyle, +0x20, Task Manager
return

+f8::
;DetectHiddenWindows, on
WinSet, AlwaysOnTop, Off, Task Manager
WinSet, ExStyle, -0x20, Task Manager
WinSet, Transparent, 255, Task Manager
Return