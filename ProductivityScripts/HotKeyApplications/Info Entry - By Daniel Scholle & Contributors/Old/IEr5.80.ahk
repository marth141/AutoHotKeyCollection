﻿#SingleInstance Force
#NoEnv
Menu, Tray, Icon, shell32.dll, 261
FileName = % SubStr(A_ScriptName, 1, -4)

Version = 5.80
/* CHANGE NOTES:
Salesforce CAD page update for new drop-down listings
*/

; ###Defaults###
; What the different fields will default to if the .ini file
; is not present when the tool is first run. Most of these are
; found on the Stats tab, others are on the first Clipboard tab.
DefDownNumber=22 		;Down Number- how many times would you have to push down in
				;the SalesForce drop-down list to pick your default module type.
DefRating=0.280			;Default module rating in kiloWatts
DefEfficiency=0.85		;Default module efficiency as a percentage (0.85 = 850 Sunhours)
DefSectionMinimum=900		;Default section minimum production sun hours
DefSystemMinimum=900		;Default system minimum production sun hours
DefStateFull=46 		;How many lines down in the drop-down list is the State?
				;(search for ADAPTATION_NOTE to see a list with numbers and abbreviations)
DefStateAB=UT			;State's two-letter abbreviation.
DefSNumber=5			;Default value for 5 roof sections' worth of rows when the GUI is created.

iniread, IN, %A_MyDocuments%\IEUserSettings.ini, Main, IN, ---
iniread, BoardHide, %A_MyDocuments%\IEUserSettings.ini, Main, BoardHide,0
iniread, TaskBarHide, %A_MyDocuments%\IEUserSettings.ini, Main, TaskBarHide,0
iniread, winX, %A_MyDocuments%\IEUserSettings.ini, Main, xPos, 200
iniread, winY, %A_MyDocuments%\IEUserSettings.ini, Main, yPos, 200
iniread, SavedBackground, %A_MyDocuments%\IEUserSettings.ini, Main, SavedBackground, BLANK
SplitPath, SavedBackground, BGPICText
iniread, ValueR, %A_MyDocuments%\IEUserSettings.ini, Color, ValueR,128
iniread, ValueG, %A_MyDocuments%\IEUserSettings.ini, Color, ValueG,128
iniread, ValueB, %A_MyDocuments%\IEUserSettings.ini, Color, ValueB,128
iniread, ValueGR, %A_MyDocuments%\IEUserSettings.ini, Color, ValueGR,128
iniread, BackColor, %A_MyDocuments%\IEUserSettings.ini, Color, BackColor, A0A0A0
iniread, FontColor, %A_MyDocuments%\IEUserSettings.ini, Color, FontColor, 000000

iniread, SNumber, %FileName%.ini, Main, SNumber, %DefSNumber%
iniread, Util, %FileName%.ini, Main, Util, %A_Space%
iniread, Meter, %FileName%.ini, Main, Meter, %A_Space%
iniread, MDisc, %FileName%.ini, Main, MDisc, %A_Space%
iniread, MBusb, %FileName%.ini, Main, MBusb, %A_Space%
iniread, DownNumber, %FileName%.ini, Main, DownNumber, %DefDownNumber%
iniread, StateFull, %FileName%.ini, Main, StateFull, %DefStateFull%
iniread, StateAB, %FileName%.ini, Main, StateAB, %DefStateAB%
iniread, Rating, %FileName%.ini, Main, Rating,%DefRating%
iniread, Efficiency, %FileName%.ini, Main, Efficiency,%DefEfficiency%
iniread, SectionMinimum, %FileName%.ini, Main, SectionMinimum, %DefSectionMinimum%
iniread, SystemMinimum, %FileName%.ini, Main, SystemMinimum, %DefSystemMinimum%
iniread, AR, %FileName%.ini, Main, AR, 3214567
iniread, FirstName, %FileName%.ini, Main, FirstName, Jamie
iniread, LastName, %FileName%.ini, Main, LastName, Doe
iniread, Address, %FileName%.ini, Main, Address, 123 W Fake Street 
iniread, City, %FileName%.ini, Main, City, Nowhere
iniread, Zip, %FileName%.ini, Main, Zip, 00000
iniread, GoalMin, %FileName%.ini, Main, GoalMin,9001
iniread, Annual, %FileName%.ini, Main, Annual,10000
if SNumber = 0
	SNumber := 1
if SNumber >19
	SNumber := 19
LoopCalcNum=1
loop
{ if (LoopCalcNum <= SNumber)
	{	iniread, RS%LoopCalcNum%T, %FileName%.ini, Main, RoofSection%LoopCalcNum%T,%A_Space%
		iniread, RS%LoopCalcNum%A, %FileName%.ini, Main, RoofSection%LoopCalcNum%A,%A_Space%
		iniread, RS%LoopCalcNum%E, %FileName%.ini, Main, RoofSection%LoopCalcNum%E,%A_Space%
		iniread, RS%LoopCalcNum%M, %FileName%.ini, Main, RoofSection%LoopCalcNum%M,%A_Space%
		iniread, RS%LoopCalcNum%P, %FileName%.ini, Main, RoofSection%LoopCalcNum%P,%A_Space%
		iniread, RS%LoopCalcNum%S, %FileName%.ini, Main, RoofSection%LoopCalcNum%S,%A_Space%
		iniread, RS%LoopCalcNum%F, %FileName%.ini, Main, RoofSection%LoopCalcNum%F,%A_Space%
		iniread, RS%LoopCalcNum%V, %FileName%.ini, Main, RoofSection%LoopCalcNum%V,%A_Space%
		iniread, RS%LoopCalcNum%D, %FileName%.ini, Main, RoofSection%LoopCalcNum%D,%A_Space%
		iniread, RS%LoopCalcNum%R, %FileName%.ini, Main, RoofSection%LoopCalcNum%R,%A_Space%
		iniread, RS%LoopCalcNum%Box, %FileName%.ini, Main, RoofSection%LoopCalcNum%Box,1
		LoopCalcNum ++
		if (LoopCalcNum > SNumber)
			break
	}
else 
 break
}
iniread, TextBox, %FileName%.ini, Main, TextBox,%A_Space%
StringReplace, TextBox, TextBox, $n$, `n, A
iniread, MonthlyTotals, %FileName%.ini, Main, TotalsBox,%A_Space%
StringReplace, MonthlyTotals, MonthlyTotals, $n$, `n, A

if (TaskbarHide = 0)
{Gui, +ToolWindow
}
Gui, Font, s12 c%FontColor%
Gui, add, button, x260 y1 w30 h30 hwndRefresh, `n`n`n`n`nRefresh
GuiButtonIcon(Refresh, "shell32.dll", 239, "s20")
Gui, add, button, xp+33 w30 h30 hwndSave, `n`n`n`n`nSave
GuiButtonIcon(Save, "shell32.dll", 259, "s20")
Gui, add, button, xp+33 w30 h30 hwndClear, `n`n`n`n`nCLEAR
GuiButtonIcon(Clear, "shell32.dll", 132, "s20")
Gui, add, button, xp+33 w30 h30 hwndHide, `n`n`n`n`nHide
GuiButtonIcon(Hide, "shell32.dll", 248, "s20")
Gui, add, button, xp+33 w30 h30 hwndSummary, `n`n`n`n`n`n`n`n`nSummary Export
GuiButtonIcon(Summary, "shell32.dll", 70, "s20")
Gui, add, button, xp+33 w30 h30 hwndCommands, `n`n`n`n`n`n`nCommands
GuiButtonIcon(Commands, "shell32.dll", 22, "s20")
Gui, add, button, xp+33 w30 h30 hwndHow, `n`n`n`n`n?
GuiButtonIcon(How, "shell32.dll", 24, "s20")
Gui, add, button, xp+33 w30 h30 hwndTaskbar, `n`n`n`n`n`nTaskbar
GuiButtonIcon(Taskbar, "shell32.dll", 241, "s20")
Gui, add, button, xp+33 w30 h30 hwndClipboard, `n`n`n`n`n`nClipboard
GuiButtonIcon(Clipboard, "shell32.dll", 74, "s20")

; 210 is the base since the other GUI stuff has to appear
; 23 is the height of each additional row
StartHeight := (210 + (SNumber * 23))

if (BoardHide = 0)
{StartWidth = 880
}
else {
StartWidth = 560
}

Clear := 0
Save := 0
Summary := 0
Refresh := 0
Hide := 0
How := 0
Clipboard := 0
Taskbar := 0
Commands := 0

Gui, Add, Picture, x2 y34 w%StartWidth% h%StartHeight% BackgroundTrans vSavedBackground, %SavedBackground%
Gui, add, text, x570 y5, Enter your own text below
Gui, add, edit, -E0x200 -TabStop xp-10 yp+20 w300 r18 vTextBox,%TextBox%

Gui, Font, cBlack
Gui, add, Tab2, x0 y5 h410 w540, Clipboard||Totals|Stats|BG
Gui, Font, c%FontColor%

; ###Front tab###
Gui, Tab, Clipboard
Gui, add, text, x10 y40 BackgroundTrans, Utl#:
Gui, add, edit, -E0x200  +center xp+37 yp-1 h22 w120 vUtil,%Util%
Gui, add, text, x10 yp+25 BackgroundTrans, Mtr#:
Gui, add, edit, -E0x200  +center xp+37 yp-1 h22 w120 vMeter,%Meter%
Gui, add, text, x10 yp+25 BackgroundTrans, Busbar/Main Disc:
Gui, add, edit, -E0x200  +center yp+25 h22 w40 Number Limit3 vMBusb,%MBusb%
Gui, add, edit, -E0x200  +center xp+80 h22 w40 Number Limit3 vMDisc,%MDisc%
Gui, add, text, x170 y40 BackgroundTrans, DI:
Gui, add, edit, -E0x200  +center xp+25 yp-1 h22 w33 Limit3 vIN,%IN%
Gui, add, text, xp+35 yp+1 BackgroundTrans, #:
Gui, add, edit, -E0x200  +center xp+15 yp-1 h22 w70 Number Limit7 vAR,%AR%
Gui, add, text, xp+73 yp+1 BackgroundTrans, Name:
Gui, add, edit, -E0x200  +center xp+46 yp-1 h22 w95 vFirstName,%FirstName%
Gui, add, edit, -E0x200  +center xp+97 h22 w95 vLastName,%LastName%
Gui, add, text, x170 yp+25 BackgroundTrans, Address:
Gui, add, edit, -E0x200  +center xp+66 yp-1 h22 w320 vAddress,%Address%
Gui, add, edit, -E0x200  +center xp-65 yp+27 h22 w173 vCity,%City%
Gui, Add, DropDownList, -TabStop xp+178 yp-2 w140 R15 Choose%StateFull% AltSubmit gStateInfo vStateFull, Alabama|Alaska|Arizona|Arkansas|California|Colorado|Connecticut|Delaware|D.C.|Florida|Georgia|Hawaii|Idaho|Illinois|Indiana|Iowa|Kansas|Kentucky|Louisiana|Maine|Maryland|Massachusetts|Michigan|Minnesota|Mississippi|Missouri|Montana|Nebraska|Nevada|New Hampshire|New Jersey|New Mexico|New York|North Carolina|North Dakota|Ohio|Oklahoma|Oregon|Pennsylvania|Puerto Rico|Rhode Island|South Carolina|South Dakota|Tennessee|Texas|Utah|Vermont|Virginia|Washington|West Virginia|Wisconsin|Wyoming
Gui, add, edit, -E0x200  +center xp+145 yp+2 h22 w60 Number Limit5 vZip,%Zip%
Gui, Add, Progress, x140 yp+30 w275 h20 cBlue vProdProg
Gui, add, button, -TabStop xp+280 w110 h22, CALCULATE
Gui, font, bold
Gui, add, text, x5 y145 BackgroundTrans, S#:
Gui, add, edit, -E0x200 +center xp+28 h22 w28 Number Limit2 vSNumber, %SNumber%
Gui, add, text, x135 y145 BackgroundTrans, Goal:
Gui, add, edit, -E0x200  +center xp+50 yp-2 h22 w75 Number Limit5 gCalculateTotal vGoalMin, %GoalMin%
Gui, add, text, xp+78 w65 yp+3 BackgroundTrans vGoalTextBLL,
Gui, add, text, xp+2 h22 w65 BackgroundTrans vGoalTextBLR,
Gui, add, text, yp-2 h22 w65 BackgroundTrans vGoalTextBUR,
Gui, add, text, xp-2 h22 w65 BackgroundTrans vGoalTextBUL,
Gui, add, text, xp+1 yp+1 h22 w65 BackgroundTrans vGoalText,
Gui, font, normal
Gui, add, text, xp+140 BackgroundTrans, Usage:
Gui, font, bold
Gui, add, edit, -E0x200  +center xp+60 yp-2 h22 w60 Number Limit5 gCalculateTotal vAnnual, %Annual%
Gui, add, text, x50 yp+22 w60 h22 BackgroundTrans, Totals:
Gui, add, text, xp+120 yp+2 h22 w30 BackgroundTrans vRSSumMBLL,%A_Space%
Gui, add, text, xp+2 h22 w30 BackgroundTrans vRSSumMBLR,%A_Space%
Gui, add, text, yp-2 h22 w30 BackgroundTrans vRSSumMBUR,%A_Space%
Gui, add, text, xp-2 h22 w30 BackgroundTrans vRSSumMBUL,%A_Space%
Gui, add, text, xp+1 yp+1 h22 w30 BackgroundTrans vRSSumM,%A_Space%
Gui, Font, cBlack
Gui, add, text, xp+30 yp+1 h22 w55 BackgroundTrans vRSSumPBLL,%A_Space%
Gui, add, text, xp+2 h22 w55 +left BackgroundTrans vRSSumPBLR,%A_Space%
Gui, add, text, yp-2 h22 w55 +left BackgroundTrans vRSSumPBUR,%A_Space%
Gui, add, text, xp-2 h22 w55 +left BackgroundTrans vRSSumPBUL,%A_Space%
Gui, Font, c%FontColor%
Gui, add, text, xp+1 yp+1 +left h22 w55 BackgroundTrans vRSSumP,%A_Space%
Gui, Font, cBlack
Gui, add, text, xp+60 yp+1 h22 w50 BackgroundTrans vRSSumZBLL,%A_Space%
Gui, add, text, xp+2 h22 w55 +left BackgroundTrans vRSSumZBLR,%A_Space%
Gui, add, text, yp-2 h22 w55 +left BackgroundTrans vRSSumZBUR,%A_Space%
Gui, add, text, xp-2 h22 w55 +left BackgroundTrans vRSSumZBUL,%A_Space%
Gui, Font, c%FontColor%
Gui, add, text, xp+1 yp+1 h22 +left w55 BackgroundTrans vRSSumZ,%A_Space%
Gui, add, text, xp+65 yp+1 h22 w50 BackgroundTrans vRSSumHBLL,%A_Space%
Gui, add, text, xp+2 h22 w50 +left BackgroundTrans vRSSumHBLR,%A_Space%
Gui, add, text, yp-2 h22 w50 +left BackgroundTrans vRSSumHBUR,%A_Space%
Gui, add, text, xp-2 h22 w50 +left BackgroundTrans vRSSumHBUL,%A_Space%
Gui, add, text, xp+1 yp+1 h22 +left w50 BackgroundTrans vRSSumH,%A_Space%
Gui, add, text, xp+80 h22 w60 BackgroundTrans, Offset:
Gui, add, text, xp+60 h22 w50 BackgroundTrans vOffsetP, 0`%
Gui, font, normal

Gui, add, text, x55 yp+19 BackgroundTrans, Eyes
Gui, add, text, xp+49 BackgroundTrans, Tilt°
Gui, add, text, xp+29 BackgroundTrans, Azim°
Gui, add, text, xp+42 BackgroundTrans, Mods
Gui, add, text, xp+47 BackgroundTrans, Prod.
Gui, add, text, xp+48 BackgroundTrans, (Size)
Gui, add, text, xp+41 BackgroundTrans, (Hours)
Gui, add, text, xp+54 BackgroundTrans, S`%
Gui, add, text, xp+30 BackgroundTrans, AP
Gui, add, text, xp+28 BackgroundTrans, Avg'
Gui, add, text, xp+35 BackgroundTrans, LHD'
Gui, add, text, xp+45 BackgroundTrans, Ra"

LoopCalcNum=1
loop
{ if (LoopCalcNum <= SNumber)
	{
		LoopBox = % RS%LoopCalcNum%Box
		LoopEyes = % RS%LoopCalcNum%E
		LoopTilt = % RS%LoopCalcNum%T
		LoopAzimuth = % RS%LoopCalcNum%A
		LoopModules = % RS%LoopCalcNum%M
		LoopProd = % RS%LoopCalcNum%P
		LoopShading = % RS%LoopCalcNum%S
		LoopFeet = % RS%LoopCalcNum%F
		LoopAverage = % RS%LoopCalcNum%V
		LoopLHD = % RS%LoopCalcNum%D
		LoopSpacing = % RS%LoopCalcNum%R
		Gui, add, text, x5 yp+22 BackgroundTrans, S%LoopCalcNum%
		Gui, Add, Checkbox, xp+30 yp+4 w13 h13 -TabStop Checked%LoopBox% vRS%LoopCalcNum%Box gToggleSection,%A_Space%
		Gui, add, edit, -E0x200  +center xp+18 yp-3 h22 w50 -TabStop vRS%LoopCalcNum%E,%LoopEyes%
		Gui, add, edit, -E0x200  +center xp+54 h22 w25 Number Limit2 vRS%LoopCalcNum%T,%LoopTilt%
		Gui, add, edit, -E0x200  +center xp+26 h22 w40 Number Limit3 vRS%LoopCalcNum%A,%LoopAzimuth%
		Gui, add, edit, -E0x200  +center xp+44 h22 Number Limit3 w30 gCalculateSection vRS%LoopCalcNum%M,%LoopModules%
		Gui, add, edit, -E0x200  +center xp+31 h22 w65 limit7 gCalculateSection vRS%LoopCalcNum%P,%LoopProd%
		Gui, add, text, xp+70 yp+3 h16 w60 BackgroundTrans vRS%LoopCalcNum%Z,%A_Space%
		Gui, add, text, xp+45 yp+1 h16 w40 BackgroundTrans vRS%LoopCalcNum%HBLL,%A_Space%
		Gui, add, text, xp+2 h16 w40 BackgroundTrans vRS%LoopCalcNum%HBLR,%A_Space%
		Gui, add, text, yp-2 h16 w40 BackgroundTrans vRS%LoopCalcNum%HBUR,%A_Space%
		Gui, add, text, xp-2 h16 w40 BackgroundTrans vRS%LoopCalcNum%HBUL,%A_Space%
		Gui, add, text, xp+1 yp+1 h16 w40 -TabStop BackgroundTrans vRS%LoopCalcNum%H,%A_Space%
		Gui, add, edit, -E0x200  +center xp+43 yp-3 h22 w25 Number Limit3 vRS%LoopCalcNum%S,%LoopShading%
		Gui, add, edit, -E0x200  +center xp+26 h22 w30 -TabStop Number Limit3 vRS%LoopCalcNum%F,%LoopFeet%
		Gui, add, edit, -E0x200  +center xp+31 h22 w30 -TabStop Number Limit2 vRS%LoopCalcNum%V,%LoopAverage%
		Gui, add, edit, -E0x200  +center xp+31 h22 w50 -TabStop limit5 vRS%LoopCalcNum%D,%LoopLHD%
		Gui, add, edit, -E0x200  +center xp+51 h22 w30 -TabStop Number Limit2 vRS%LoopCalcNum%R,%LoopSpacing%
		LoopCalcNum ++
		if (LoopCalcNum > SNumber)
			break
	}
else 
 break
}

Gui, Tab, Stats
Gui, add, text, x20 y40 h22 BackgroundTrans, Down Number:
Gui, add, edit, -E0x200  +center -TabStop xp+110 yp-1 h22 w30 Limit2 gMPMP vDownNumber,%DownNumber%
Gui, add, text, xp+40 h22 BackgroundTrans, (change this number for your module type)
Gui, add, text, x20 yp+25 h22 BackgroundTrans, Module Rating (Watts):
Gui, add, edit, -E0x200  +center -TabStop xp+170 yp-1 h22 w50 Limit5 gMPMP vRating,%Rating%
Gui, add, text, x20 yp+25 h22 BackgroundTrans, Module Efficiency`%:
Gui, add, edit, -E0x200  +center -TabStop xp+150 yp-1 h22 w40 Limit4 gMPMP vEfficiency, %Efficiency%
Gui, add, text, x20 yp+25 h22 BackgroundTrans, Minimum Per-Module Production:
Gui, font, bold
Gui, add, text, xp+235 h18 w100 BackgroundTrans vMinPerModPro, 0W
Gui, font, normal
Gui, add, text, x20 yp+25 h22 BackgroundTrans, Section Minimum:
Gui, add, edit, -E0x200  +center -TabStop xp+150 yp-1 h22 w40 Number Limit4 vSectionMinimum,%SectionMinimum%
Gui, add, text, x20 yp+25 h22 BackgroundTrans, System Minimum:
Gui, add, edit, -E0x200  +center -TabStop xp+150 yp-1 h22 w40 Number Limit4 vSystemMinimum,%SystemMinimum%

Gui, Tab, BG
; ###COLOR PICKER###
; from www.autohotkey.com/board/topic/6233-quick-and-dirty-color-picker/
Gui, Font, s10 cBlack
ColorCodeNote = Adapted from Lego_coder's quick-and-dirty color picker from the Autohotkey forums.`nChange the background image, font color, and box background colors here.`nDefaults to grey if the .ini file is absent.
Gui, Add, text, x20 y40 BackgroundTrans, %ColorCodeNote%
Gui, add, text, xp+2 BackgroundTrans,%ColorCodeNote%
Gui, add, text, yp-2 BackgroundTrans,%ColorCodeNote%
Gui, add, text, xp-2 BackgroundTrans,%ColorCodeNote%
Gui, Font, cWhite
Gui, add, text, xp+1 yp+1 BackgroundTrans,%ColorCodeNote%
Gui, Font, s12 c%FontColor%
Gui, Add, text, x20 y88 +right BackgroundTrans, Color Preview
Gui, Add, ListView, xp+0 yp+23 h25 w100 ReadOnly 0x4000 +Background000000 VColor_Block
Gui, Add, Button, xp+105 yp-18 w55 h24 +center gColorSet, ->BG
Gui, Add, Button, xp+0 yp+26 w55 h24 +center gFontSet, ->Font
Gui, Add, Button, xp+58 yp-26 w90 h24 gBGBrowse, Choose BG
Gui, Add, text, xp+95 yp-10 BackgroundTrans vBGPICText, Current background image:`n"%BGPICText%".
Gui, Add, Button, x475 y95 +center gColorDefault, Defaults
Gui, Add, Text, x20 y143 BackgroundTrans, Red
Gui, add, edit, -E0x200  +center xp+50 yp-4 w50 Number Limit3 +right vValueR gChange_ValueR,%ValueR%
Gui, Add, UpDown, Range0-255 Wrap, %ValueR%
Gui, Add, Slider, xp-45 yp+30 w110 Range0-255 vValueRSlider gChange_ValueRSlider AltSubmit, %ValueR%
Gui, Add, Text, xp+115 y143 BackgroundTrans, Green
Gui, add, edit, -E0x200  +center xp+50 yp-4 w50 Number Limit3  +right vValueG gChange_ValueG, %ValueG%
Gui, Add, UpDown, Range0-255 Wrap, %ValueG%
Gui, Add, Slider, xp-45 yp+30 w110 BackgroundTrans Range0-255 vValueGSlider gChange_ValueGSlider AltSubmit, %ValueG%
Gui, Add, Text, xp+115 y143 BackgroundTrans, Blue
Gui, add, edit, -E0x200  +center xp+50 yp-4 w50 Number Limit3  +right vValueB gChange_ValueB, %ValueB%
Gui, Add, UpDown, Range0-255 Wrap, %ValueB%
Gui, Add, Slider, xp-45 yp+30 w110 Range0-255 vValueBSlider gChange_ValueBSlider AltSubmit, %ValueB%
Gui, Add, Text, xp+115 y143 BackgroundTrans, B/W
Gui, add, edit, -E0x200  +center xp+50 yp-4 w50 Number Limit3  +right vValueGR gChange_ValueGR, %ValueGR%
Gui, Add, UpDown, Range0-255 Wrap, %ValueGR%
Gui, Add, Slider, xp-45 yp+30 w110 Range0-255 vValueGRSlider gChange_ValueGRSlider AltSubmit, %ValueGR%
Gui, Add, text, x20 yp+41 +right BackgroundTrans, Color Hex Code:
Gui, add, edit, -E0x200  +center xp+120 yp-5 w65 ReadOnly +right VColor_Value, %BackColor%
; ###End Miguel's color picker code for now###

Gui, Tab, Totals
Gui, Font, cBlack
TotalsNote = Using Ctrl-Alt-Click, highlight the total monthly productions column in the`nSolmetric Report .PDF and copy-paste the data into the box. Use Ctrl-4`nto paste it into the Energy Production part of the Salesforce CAD page.
Gui, Add, text, x20 y40 BackgroundTrans, %TotalsNote%
Gui, add, text, xp+2 BackgroundTrans,%TotalsNote%
Gui, add, text, yp-2 BackgroundTrans,%TotalsNote%
Gui, add, text, xp-2 BackgroundTrans,%TotalsNote%
Gui, Font, cWhite
Gui, add, text, xp+1 yp+1 BackgroundTrans,%TotalsNote%
Gui, Font, c%FontColor%
Gui, add, edit, -E0x200  x20 yp+80 w450 h100 gMonthTotal vMonthlyTotals, %MonthlyTotals%

if (winx = "")
{Gui, Show, x200 y200 w%StartWidth% h%StartHeight%, InfoEntry %Version% (Ctrl-Space to show/hide)
}
else {
Gui, Show, x%winX% y%winY% w%StartWidth% h%StartHeight%, InfoEntry %Version% (Ctrl-Space to show/hide)
}
Gui, Color, %BackColor%, %BackColor%,
Menu, tray, NoStandard
Menu, tray, add, About InfoEntry, AboutThis
Menu, tray, add, Show/Hide, ButtonHide
Menu, tray, add, Exit, guiclose
return

ColorDefault:
GuiControl, Text, ValueR, 128
GuiControl, Text, ValueRSlider, 128
GuiControl, Text, ValueG, 128
GuiControl, Text, ValueGSlider, 128
GuiControl, Text, ValueB, 128
GuiControl, Text, ValueBSlider, 128
GuiControl, Text, ValueGRSlider, 128
GuiControl, Text, ValueGR, 128
BackColor = A0A0A0
FontColor = 000000
Gosub Show_New_Color
Gui, Color, 808080, A0A0A0
return

ColorSet:
BackColor = %New_Color_Value%
Gui, Color, 808080, %New_Color_Value%
return

FontSet:
FontColor = %New_Color_Value%
Tooltip, Save and refresh`nfor changes to occur.
SetTimer, RemoveToolTip, 1500
return

; ###Resume Miguel's color-picking stuff###
Change_ValueRSlider:
GuiControlGet, ValueRSlider
GuiControl, Text, ValueR, %ValueRSlider%
gosub Show_New_Color
Return

Change_ValueR:
GuiControlGet, ValueR
GuiControl, Text, ValueRSlider, %ValueR%
Gosub Show_New_Color
Return

Change_ValueGSlider:
GuiControlGet, ValueGSlider
GuiControl, Text, ValueG, %ValueGSlider%
Gosub Show_New_Color
Return

Change_ValueG:
GuiControlGet, ValueG
GuiControl, Text, ValueGSlider, %ValueG%
Gosub Show_New_Color
Return

Change_ValueBSlider:
GuiControlGet, ValueBSlider
GuiControl, Text, ValueB, %ValueBSlider%
Gosub Show_New_Color
Return 

Change_ValueB:
GuiControlGet, ValueB
GuiControl, Text, ValueBSlider, %ValueB%
Gosub Show_New_Color
Return

Change_ValueGRSlider:
GuiControlGet, ValueGRSlider
GuiControl, Text, ValueR, %ValueGRSlider%
GuiControl, Text, ValueG, %ValueGRSlider%
GuiControl, Text, ValueB, %ValueGRSlider%
GuiControl, Text, ValueGR, %ValueGRSlider%
Gosub Show_New_Color
Return 

Change_ValueGR:
GuiControlGet, ValueGR
GuiControl, Text, ValueRSlider, %ValueGR%
GuiControl, Text, ValueGSlider, %ValueGR%
GuiControl, Text, ValueBSlider, %ValueGR%
GuiControl, Text, ValueGRSlider, %ValueGR%
Gosub Show_New_Color
Return

Show_New_Color:
Gui submit, nohide
SetFormat, integer, hex
ValueR += 0
ValueG += 0
ValueB += 0
SetFormat, integer, d
Stringright,ValueR,ValueR,StrLen(ValueR)-2
If (StrLen(ValueR)=1)
ValueR=0%ValueR%
Stringright,ValueG,ValueG,StrLen(ValueG)-2
If (StrLen(ValueG)=1)
ValueG=0%ValueG%
Stringright,ValueB,ValueB,StrLen(ValueB)-2
If (StrLen(ValueB)=1)
ValueB=0%ValueB%
New_Color_Value=%ValueR%%ValueG%%ValueB%
GuiControl, Text, Color_Value, %New_Color_Value%
GuiControl, +Background%New_Color_Value%, Color_Block
Return
; ###End Miguel's color-picking code###

BGBrowse:
FileSelectFile, SavedBackground, 3, , Choose an image file, Images (*.bmp; *.jpg; *.png)
SplitPath, SavedBackground, BGPICText
Gui, Submit, nohide
GuiControl,,BGPICText, The current BG picture is:`n"%BGPICText%"
GuiControl,,SavedBackground, *w560 *h380 %SavedBackground%
Return

; ADAPTATION_NOTE
; When changing this tool for use in a different office,
; the number corresponding to each State may be used
; in the DefStateFull entry at the beginning of this script.
StateInfo:
Gui, Submit, Nohide
if StateFull = 1
	{
	GuiControl,, StateABBox,AL
	StateAB = AL
	State =Alabama
	}
if StateFull = 2
	{
	GuiControl,, StateABBox,AK
	StateAB = AK
	State =Alaska
	}
if StateFull = 3
	{
	GuiControl,, StateABBox,AZ	
	StateAB = AZ
	State =Arizona
	}
if StateFull = 4
	{
	GuiControl,, StateABBox,AR	
	StateAB = AR
	State =Arkansas
	}
if StateFull = 5
	{
	GuiControl,, StateABBox,CA	
	StateAB = CA
	State =California
	}
if StateFull = 6
	{
	GuiControl,, StateABBox,CO	
	StateAB = CO
	State =Colorado
	}
if StateFull = 7
	{
	GuiControl,, StateABBox,CT	
	StateAB = CT
	State =Connecticut
	}
if StateFull = 8
	{
	GuiControl,, StateABBox,DE	
	StateAB = DE
	State =Delaware
	}
if StateFull = 9
	{
	GuiControl,, StateABBox,DC
	StateAB = DC
	State =DC
	}
if StateFull = 10
	{
	GuiControl,, StateABBox,FL	
	StateAB = FL
	State =Florida
	}
if StateFull = 11
	{
	GuiControl,, StateABBox,GA	
	StateAB = GA
	State =Georgia
	}
if StateFull = 12
	{
	GuiControl,, StateABBox,HI	
	StateAB = HI
	State =Hawaii
	}
if StateFull = 13
	{
	GuiControl,, StateABBox,ID	
	StateAB = ID
	State =Idaho
	}
if StateFull = 14
	{
	GuiControl,, StateABBox,IL	
	StateAB = IL
	State =Illinois
	}
if StateFull = 15
	{
	GuiControl,, StateABBox,IN	
	StateAB = IN
	State =Indiana
	}
if StateFull = 16
	{
	GuiControl,, StateABBox,IA	
	StateAB = IA
	State =Iowa
	}
if StateFull = 17
	{
	GuiControl,, StateABBox,KS	
	StateAB = KS
	State =Kansas
	}
if StateFull = 18
	{
	GuiControl,, StateABBox,KY	
	StateAB = KY
	State =Kentucky
	}
if StateFull = 19
	{
	GuiControl,, StateABBox,LA	
	StateAB = LA
	State =Louisiana
	}
if StateFull = 20
	{
	GuiControl,, StateABBox,ME	
	StateAB = ME
	State =Maine
	}
if StateFull = 21
	{
	GuiControl,, StateABBox,MD	
	StateAB = MD
	DownNumber := 7
	GuiControl,, DownNumber,%DownNumber%
	State =Maryland
	}
if StateFull = 22
	{
	GuiControl,, StateABBox,MA	
	StateAB = MA
	DownNumber := 11
	GuiControl,, DownNumber,%DownNumber%
	State =Massachusetts
	}
if StateFull = 23
	{
	GuiControl,, StateABBox,MI	
	StateAB = MI
	State =Michigan
	}
if StateFull = 24
	{
	GuiControl,, StateABBox,MN	
	StateAB = MN
	State =Minnesota
	}
if StateFull = 25
	{
	GuiControl,, StateABBox,MS	
	StateAB = MS
	State =Mississippi
	}
if StateFull = 26
	{
	GuiControl,, StateABBox,MO	
	StateAB = MO
	State =Missouri
	}
if StateFull = 27
	{
	GuiControl,, StateABBox,MT	
	StateAB = MT
	State =Montana
	}
if StateFull = 28
	{
	GuiControl,, StateABBox,NE	
	StateAB = NE
	State =Nebraska
	}
if StateFull = 29
	{
	GuiControl,, StateABBox,NV	
	StateAB = NV
	State =Nevada
	}
if StateFull = 30
	{
	GuiControl,, StateABBox,NH	
	StateAB = NH
	State =New Hampshire
	}
if StateFull = 31
	{
	GuiControl,, StateABBox,NJ	
	StateAB = NJ
	State =New Jersey
	}
if StateFull = 32
	{
	GuiControl,, StateABBox,NM	
	StateAB = NM
	State =New Mexico
	}
if StateFull = 33
	{
	GuiControl,, StateABBox,NY	
	StateAB = NY
	State =New York
	DownNumber := 30
	GuiControl,, DownNumber,%DownNumber%
	}
if StateFull = 34
	{
	GuiControl,, StateABBox,NC	
	StateAB = NC
	State =North Carolina
	}
if StateFull = 35
	{
	GuiControl,, StateABBox,ND	
	StateAB = ND
	State =North Dakota
	}
if StateFull = 36
	{
	GuiControl,, StateABBox,OH	
	StateAB = OH
	State =Ohio
	}
if StateFull = 37
	{
	GuiControl,, StateABBox,OK	
	StateAB = OK
	State =Oklahoma
	}
if StateFull = 38
	{
	GuiControl,, StateABBox,OR	
	StateAB = OR
	State =Oregon
	}
if StateFull = 39
	{
	GuiControl,, StateABBox,PA	
	StateAB = PA
	State =Pennsylvania
	}
if StateFull = 40
	{
	GuiControl,, StateABBox,PR
	StateAB = PR
	State =Puerto Rico
	}
if StateFull = 41
	{
	GuiControl,, StateABBox,RI	
	StateAB = RI
	State =Rhode Island
	}
if StateFull = 42
	{
	GuiControl,, StateABBox,SC	
	StateAB = SC
	State =South Carolina
	}
if StateFull = 43
	{
	GuiControl,, StateABBox,SD	
	StateAB = SD
	State =South Dakota
	}
if StateFull = 44
	{
	GuiControl,, StateABBox,TN	
	StateAB = TN
	State =Tennessee
	}
if StateFull = 45
	{
	GuiControl,, StateABBox,TX	
	StateAB = TX
	State =Texas
	}
if StateFull = 46
	{
	GuiControl,, StateABBox,UT	
	StateAB = UT
	DownNumber := 22
	GuiControl,, DownNumber,%DownNumber%
	State =Utah
	}
if StateFull = 47
	{
	GuiControl,, StateABBox,VT	
	StateAB = VT
	State =Vermont
	}
if StateFull = 48
	{
	GuiControl,, StateABBox,VA	
	StateAB = VA
	State =Virginia
	}
if StateFull = 49
	{
	GuiControl,, StateABBox,WA	
	StateAB = WA
	State =Washington
	}
if StateFull = 50
	{
	GuiControl,, StateABBox,WV	
	StateAB = WV
	State =West Virginia
	}
if StateFull = 51
	{
	GuiControl,, StateABBox,WI	
	StateAB = WI
	State =Wisconsin
	}
if StateFull = 52
	{
	GuiControl,, StateABBox,WY	
	StateAB = WY
	State =Wyoming
	}
return

MonthTotal:
Gui, Submit, nohide
StringReplace, MonthlyTotals, MonthlyTotals, `n, `t, A
GuiControl,, MonthlyTotals,%MonthlyTotals%
return

ToggleSection:
Gui, Submit, nohide
if (CalcAll = False)
	{
		StringReplace, SectionNo, A_GuiControl, RS,, A
		StringReplace, SectionNo, SectionNo, Box,, A
	}
If (RS%SectionNo%Box = 1)
	{Gui, Font, c%FontColor%
	GuiControl, Font, RS%SectionNo%E
	GuiControl, Font, RS%SectionNo%T
	GuiControl, Font, RS%SectionNo%A
	GuiControl, Font, RS%SectionNo%M
	GuiControl, Font, RS%SectionNo%P
	GuiControl, Font, RS%SectionNo%Z
	GuiControl, Font, RS%SectionNo%S
	GuiControl, Font, RS%SectionNo%F
	GuiControl, Font, RS%SectionNo%V
	GuiControl, Font, RS%SectionNo%D
	GuiControl, Font, RS%SectionNo%R
	}else
		{Gui, Font, c505050
		GuiControl, Font, RS%SectionNo%E
		GuiControl, Font, RS%SectionNo%T
		GuiControl, Font, RS%SectionNo%A
		GuiControl, Font, RS%SectionNo%M
		GuiControl, Font, RS%SectionNo%P
		GuiControl, Font, RS%SectionNo%Z
		GuiControl, Font, RS%SectionNo%S
		GuiControl, Font, RS%SectionNo%F
		GuiControl, Font, RS%SectionNo%V
		GuiControl, Font, RS%SectionNo%D
		GuiControl, Font, RS%SectionNo%R
		}
If (CalcAll = True)
	{return
	} else
	{gosub CalculateTotal
	gosub CalculateSection
	}
return

CalculateSection:
Gui, Submit, nohide
if (CalcAll != True)
	{
		StringReplace, SectionNo, A_GuiControl, RS,, A
		StringReplace, SectionNo, SectionNo, P,, A
		StringReplace, SectionNo, SectionNo, M,, A
		StringReplace, SectionNo, SectionNo, Box,, A
	}
If Clear = True
	{
	return
	}
RS%SectionNo%Z := Rating * RS%SectionNo%M
RS%SectionNo%H := RS%SectionNo%P / RS%SectionNo%Z
StringLeft, RS%SectionNo%Z, RS%SectionNo%Z, 5
If (RS%SectionNo%H > 100 && RS%SectionNo%H < 1000)
	{StringLeft, RS%SectionNo%H, RS%SectionNo%H, 3
	} else
	{StringLeft, RS%SectionNo%H, RS%SectionNo%H, 4
	}
If (RS%SectionNo%H < 100)
	{StringLeft, RS%SectionNo%H, RS%SectionNo%H, 2
	}
GuiControl,, RS%SectionNo%Z, % RS%SectionNo%Z
GuiControl,, RS%SectionNo%H, % RS%SectionNo%H
GuiControl,, RS%SectionNo%HBLL, % RS%SectionNo%H
GuiControl,, RS%SectionNo%HBLR, % RS%SectionNo%H
GuiControl,, RS%SectionNo%HBUR, % RS%SectionNo%H
GuiControl,, RS%SectionNo%HBUL, % RS%SectionNo%H

If (RS%SectionNo%Box = 1)
{If RS%SectionNo%H >= %SystemMinimum%
	{GuiControl, +c00FF00, RS%SectionNo%H
	GuiControl, +c000000, RS%SectionNo%HBLL
	GuiControl, +c000000, RS%SectionNo%HBLR
	GuiControl, +c000000, RS%SectionNo%HBUR
	GuiControl, +c000000, RS%SectionNo%HBUL
	} else
		{If RS%SectionNo%H < %SectionMinimum%
			{GuiControl, +c0000FF, RS%SectionNo%H, 
			GuiControl, +c25FFFF, RS%SectionNo%HBLL
			GuiControl, +c25FFFF, RS%SectionNo%HBLR
			GuiControl, +c25FFFF, RS%SectionNo%HBUR
			GuiControl, +c25FFFF, RS%SectionNo%HBUL
			} else
			{If RS%SectionNo%H >= %SectionMinimum%
				{GuiControl, +cFFFF00, RS%SectionNo%H
				GuiControl, +c000000, RS%SectionNo%HBLL
				GuiControl, +c000000, RS%SectionNo%HBLR
				GuiControl, +c000000, RS%SectionNo%HBUR
				GuiControl, +c000000, RS%SectionNo%HBUL
				}
			}
		}
	{If (CalcAll = True)
		{return
		} else
		{gosub CalculateTotal
		}
	}
}else
	{GuiControl, +c000000, RS%SectionNo%H
	GuiControl, +c505050, RS%SectionNo%HBLL
	GuiControl, +c505050, RS%SectionNo%HBLR
	GuiControl, +c505050, RS%SectionNo%HBUR
	GuiControl, +c505050, RS%SectionNo%HBUL
	}
Return

CalculateTotal:
Gui, Submit, nohide

If (Clear = True)
	{
	return
	}
LoopCalcNum=1
RSSumM := 0
RSSumP := 0
loop
{ if (LoopCalcNum <= SNumber)
	{
		If ((RS%LoopCalcNum%Box = 1) and (RS%LoopCalcNum%M != "") and (RS%LoopCalcNum%P != ""))
			{RSSumM += % RS%LoopCalcNum%M
			RSSumP += % RS%LoopCalcNum%P
			} else
			{RSSumM += 0
			RSSumP += 0
			}
		LoopCalcNum ++
		if (LoopCalcNum > SNumber)
			break
	}
else 
 break
}
RSSumZ := Rating * RSSumM
RSSumH := RSSumP / (Rating * RSSumM)
if (RSSumZ <10)
	{StringLeft, RSSumZ, RSSumZ, 5
	}else
	{StringLeft, RSSumZ, RSSumZ, 6
	}
StringLeft, RSSumH, RSSumH, 4
ProdPercentage := 100 * (RSSumP / GoalMin)
GuiControl,, ProdProg, %ProdPercentage%

; Sample colors
;	c00FF00 Green
;	c0000FF Blue
;	cFFFF00 Yellow
;	c000000 Black
;	cFFFFFF White
;	c25FFFF Cyan-ish

;Only if there are more than 16 modules!
If RSSumM >=%16%
{GuiControl, +c00FF00, RSSumM
GuiControl, +c000000, RSSumMBLL
GuiControl, +c000000, RSSumMBLR
GuiControl, +c000000, RSSumMBUR
GuiControl, +c000000, RSSumMBUL
} else
	{GuiControl, +c0000FF, RSSumM
	GuiControl, +c25FFFF, RSSumMBLL
	GuiControl, +c25FFFF, RSSumMBLR
	GuiControl, +c25FFFF, RSSumMBUR
	GuiControl, +c25FFFF, RSSumMBUL
	}
If RSSumH >= %SystemMinimum%
{GuiControl, +c00FF00, RSSumH
GuiControl, +c000000, RSSumHBLL
GuiControl, +c000000, RSSumHBLR
GuiControl, +c000000, RSSumHBUR
GuiControl, +c000000, RSSumHBUL
} else
	{GuiControl, +c0000FF, RSSumH
	GuiControl, +c25FFFF, RSSumHBLL
	GuiControl, +c25FFFF, RSSumHBLR
	GuiControl, +c25FFFF, RSSumHBUR
	GuiControl, +c25FFFF, RSSumHBUL
	}

if (GoalMin < 1)
{GuiControl, +c000000, GoalText
GuiControl, +cFF0000, GoalTextBLL
GuiControl, +cFF0000, GoalTextBLR
GuiControl, +cFF0000, GoalTextBUR
GuiControl, +cFF0000, GoalTextBUL
GuiControl,, GoalText,$GOAL
GuiControl,, GoalTextBLL,$GOAL
GuiControl,, GoalTextBLR,$GOAL
GuiControl,, GoalTextBUR,$GOAL
GuiControl,, GoalTextBUL,$GOAL
} else
		{if (RSSumP > Annual)
			{GuiControl, +cFFFF00, GoalText
			GuiControl, +cFFFF00, ProdProg
			GuiControl, +c000000, GoalTextBLL
			GuiControl, +c000000, GoalTextBLR
			GuiControl, +c000000, GoalTextBUR
			GuiControl, +c000000, GoalTextBUL
			GuiControl,, GoalText,ABOVE!
			GuiControl,, GoalTextBLL,ABOVE!
			GuiControl,, GoalTextBLR,ABOVE!
			GuiControl,, GoalTextBUR,ABOVE!
			GuiControl,, GoalTextBUL,ABOVE!
			} else
				{if (RSSumP < (GoalMin - (Rating *1000)))
					{GuiControl, +c0000FF, GoalText
					GuiControl, +c0000FF, ProdProg
					GuiControl, +c25FFFF, GoalTextBLL
					GuiControl, +c25FFFF, GoalTextBLR
					GuiControl, +c25FFFF, GoalTextBUR
					GuiControl, +c25FFFF, GoalTextBUL
					GuiControl,, GoalText,BELOW
					GuiControl,, GoalTextBLL,BELOW
					GuiControl,, GoalTextBLR,BELOW
					GuiControl,, GoalTextBUR,BELOW
					GuiControl,, GoalTextBUL,BELOW
					} else
						{GuiControl, +c00FF00, GoalText
						GuiControl, +c00FF00, ProdProg
						GuiControl, +c000000, GoalTextBLL
						GuiControl, +c000000, GoalTextBLR
						GuiControl, +c000000, GoalTextBUR
						GuiControl, +c000000, GoalTextBUL
						GuiControl,, GoalText,MET!
						GuiControl,, GoalTextBLL,MET!
						GuiControl,, GoalTextBLR,MET!
						GuiControl,, GoalTextBUR,MET!
						GuiControl,, GoalTextBUL,MET!
						}
				}
		}
OffsetPercentage := 100* (RSSumP / Annual)
StringLeft, OffsetPercentage, OffsetPercentage, 5
GuiControl,,OffsetP,%OffsetPercentage%`%
GuiControl,,RSSumM, %RSSumM%
GuiControl,,RSSumMBUL, %RSSumM%
GuiControl,,RSSumMBLL, %RSSumM%
GuiControl,,RSSumMBLR, %RSSumM%
GuiControl,,RSSumMBUR, %RSSumM%
GuiControl,,RSSumPBUL,%RSSumP%
GuiControl,,RSSumPBLL,%RSSumP%
GuiControl,,RSSumPBLR,%RSSumP%
GuiControl,,RSSumPBUR,%RSSumP%
GuiControl,,RSSumP,%RSSumP%
GuiControl,,RSSumZBUL,%RSSumZ%
GuiControl,,RSSumZBUR,%RSSumZ%
GuiControl,,RSSumZBLL,%RSSumZ%
GuiControl,,RSSumZBLR,%RSSumZ%
GuiControl,,RSSumZ,%RSSumZ%
GuiControl,,RSSumH,%RSSumH%
GuiControl,,RSSumHBLL,%RSSumH%
GuiControl,,RSSumHBUL,%RSSumH%
GuiControl,,RSSumHBLR,%RSSumH%
GuiControl,,RSSumHBUR,%RSSumH%
return

ButtonCALCULATE:
CalculateAll:
Gui, Submit, nohide
CalcAll := True
gosub StateInfo
gosub MonthTotal
gosub MPMP
LoopCalcNum=1
loop
{ if (LoopCalcNum <= SNumber)
	{
		SectionNo = %LoopCalcNum%
		GoSub ToggleSection
		GoSub CalculateSection
		LoopCalcNum ++
		if (LoopCalcNum > SNumber)
			break
	}
else 
 break
}
gosub CalculateTotal
CalcAll := False
return

MPMP:
Gui, Submit, nohide
MinPerModPro := Rating * (1000 * Efficiency)
StringLeft, MinPerModPro, MinPerModPro, 3
GuiControl,,MinPerModPro, %MinPerModPro% kWh
return

ButtonRefresh:
WinGetPos, winX, winY,
IniWrite, %BoardHide%, %A_MyDocuments%\IEUserSettings.ini, Main, BoardHide
IniWrite, %TaskBarHide%, %A_MyDocuments%\IEUserSettings.ini, Main, TaskBarHide
IniWrite, %winX%, %A_MyDocuments%\IEUserSettings.ini, Main, xPos
IniWrite, %winY%, %A_MyDocuments%\IEUserSettings.ini, Main, yPos
reload
return

ButtonhwndSave:
ButtonSave:
Gui, Submit, nohide
IniWrite, %winX%, %A_MyDocuments%\IEUserSettings.ini, Main, xPos
IniWrite, %winY%, %A_MyDocuments%\IEUserSettings.ini, Main, yPos
IniWrite, %SavedBackground%, %A_MyDocuments%\IEUserSettings.ini, Main, SavedBackground
IniWrite, %IN%, %A_MyDocuments%\IEUserSettings.ini, main, IN
IniWrite, %BoardHide%, %A_MyDocuments%\IEUserSettings.ini, Main, BoardHide
IniWrite, %TaskBarHide%, %A_MyDocuments%\IEUserSettings.ini, Main, TaskBarHide
SetFormat, integer, d
ValueR += 0
ValueG += 0
ValueB += 0
IniWrite, %ValueR%, %A_MyDocuments%\IEUserSettings.ini, Color, ValueR
IniWrite, %ValueG%, %A_MyDocuments%\IEUserSettings.ini, Color, ValueG
IniWrite, %ValueB%, %A_MyDocuments%\IEUserSettings.ini, Color, ValueB
IniWrite, %ValueGR%, %A_MyDocuments%\IEUserSettings.ini, Color, ValueGR
IniWrite, %BackColor%, %A_MyDocuments%\IEUserSettings.ini, Color, BackColor
IniWrite, %FontColor%, %A_MyDocuments%\IEUserSettings.ini, Color, FontColor
IniWrite, %SNumber%, %A_ScriptDir%\%FileName%.ini, Main, SNumber
IniWrite, %Util%, %A_ScriptDir%\%FileName%.ini, Main, Util
IniWrite, %Meter%, %A_ScriptDir%\%FileName%.ini, Main, Meter
IniWrite, %MDisc%, %A_ScriptDir%\%FileName%.ini, Main, MDisc
IniWrite, %MBusb%, %A_ScriptDir%\%FileName%.ini, Main, MBusb
IniWrite, %DownNumber%, %A_ScriptDir%\%FileName%.ini, Main, DownNumber
IniWrite, %Rating%, %A_ScriptDir%\%FileName%.ini, Main, Rating
IniWrite, %Efficiency%, %A_ScriptDir%\%FileName%.ini, Main, Efficiency
IniWrite, %StateFull%, %A_ScriptDir%\%FileName%.ini, Main, StateFull
IniWrite, %StateAB%, %A_ScriptDir%\%FileName%.ini, Main, StateAB
IniWrite, %SystemMinimum%, %A_ScriptDir%\%FileName%.ini, Main, SystemMinimum
IniWrite, %SectionMinimum%, %A_ScriptDir%\%FileName%.ini, Main, SectionMinimum
IniWrite, %AR%, %A_ScriptDir%\%FileName%.ini, main, AR
IniWrite, %FirstName%, %A_ScriptDir%\%FileName%.ini, main, FirstName
IniWrite, %LastName%, %A_ScriptDir%\%FileName%.ini, main, LastName
IniWrite, %Address%, %A_ScriptDir%\%FileName%.ini, main, Address
IniWrite, %City%, %A_ScriptDir%\%FileName%.ini, main, City
IniWrite, %Zip%, %A_ScriptDir%\%FileName%.ini, main, Zip
StringReplace, TextBox, TextBox, `n, $n$, A
IniWrite, %TextBox%, %A_ScriptDir%\%FileName%.ini, Main, TextBox
StringReplace, MonthlyTotals, MonthlyTotals, `n, $n$, A
IniWrite, %MonthlyTotals%, %A_ScriptDir%\%FileName%.ini, Main, TotalsBox
IniWrite, %GoalMin%, %A_ScriptDir%\%FileName%.ini, main, GoalMin
IniWrite, %Annual%, %A_ScriptDir%\%FileName%.ini, main, Annual
LoopCalcNum=1
loop
{ if (LoopCalcNum <= SNumber)
	{
		LoopTilt = % RS%LoopCalcNum%T
		LoopAzimuth = % RS%LoopCalcNum%A
		LoopEyes= % RS%LoopCalcNum%E
		LoopModules= % RS%LoopCalcNum%M
		LoopProd= % RS%LoopCalcNum%P
		LoopShading= % RS%LoopCalcNum%S
		LoopFeet= % RS%LoopCalcNum%F
		LoopHeight= % RS%LoopCalcNum%V
		LoopLHD= % RS%LoopCalcNum%D
		LoopSpacing= % RS%LoopCalcNum%R
		LoopBox= % RS%LoopCalcNum%Box
		IniWrite, %LoopBox%, %A_ScriptDir%\%FileName%.ini, Main, RoofSection%LoopCalcNum%Box
		IniWrite, %LoopEyes%, %A_ScriptDir%\%FileName%.ini, Main, RoofSection%LoopCalcNum%E
		IniWrite, %LoopTilt%, %A_ScriptDir%\%FileName%.ini, Main, RoofSection%LoopCalcNum%T
		IniWrite, %LoopAzimuth%, %A_ScriptDir%\%FileName%.ini, Main, RoofSection%LoopCalcNum%A
		IniWrite, %LoopModules%, %A_ScriptDir%\%FileName%.ini, Main, RoofSection%LoopCalcNum%M
		IniWrite, %LoopProd%, %A_ScriptDir%\%FileName%.ini, Main, RoofSection%LoopCalcNum%P
		IniWrite, %LoopShading%, %A_ScriptDir%\%FileName%.ini, Main, RoofSection%LoopCalcNum%S
		IniWrite, %LoopFeet%, %A_ScriptDir%\%FileName%.ini, Main, RoofSection%LoopCalcNum%F
		IniWrite, %LoopHeight%, %A_ScriptDir%\%FileName%.ini, Main, RoofSection%LoopCalcNum%V
		IniWrite, %LoopLHD%, %A_ScriptDir%\%FileName%.ini, Main, RoofSection%LoopCalcNum%D
		IniWrite, %LoopSpacing%, %A_ScriptDir%\%FileName%.ini, Main, RoofSection%LoopCalcNum%R
		LoopCalcNum ++
		if (LoopCalcNum > SNumber)
			break
	}
else 
 break
}
return

ButtonCLEAR:
SetControlDelay, 0
Gui, Submit, nohide
Clear := True
Guicontrol,, Util,
Guicontrol,, Meter,
Guicontrol,, MDisc,
Guicontrol,, MBusb,
Guicontrol,, AR,
Guicontrol,, FirstName,
Guicontrol,, LastName,
Guicontrol,, Address,
Guicontrol,, City,
Guicontrol,, Zip,
Guicontrol,, TextBox,
Guicontrol,, GoalMin,
Guicontrol,, Annual,
LoopCalcNum=1
loop
{ if (LoopCalcNum <= SNumber)
	{	Guicontrol,, RS%LoopCalcNum%T
		Guicontrol,, RS%LoopCalcNum%A
		Guicontrol,, RS%LoopCalcNum%E
		Guicontrol,, RS%LoopCalcNum%M
		Guicontrol,, RS%LoopCalcNum%Z
		Guicontrol,, RS%LoopCalcNum%P
		Guicontrol,, RS%LoopCalcNum%S
		Guicontrol,, RS%LoopCalcNum%F
		Guicontrol,, RS%LoopCalcNum%H
		Guicontrol,, RS%LoopCalcNum%HBLL
		Guicontrol,, RS%LoopCalcNum%HBLR
		Guicontrol,, RS%LoopCalcNum%HBUL
		Guicontrol,, RS%LoopCalcNum%HBUR
		Guicontrol,, RS%LoopCalcNum%V
		Guicontrol,, RS%LoopCalcNum%D
		Guicontrol,, RS%LoopCalcNum%R
		LoopCalcNum ++
		if (LoopCalcNum > SNumber)
			break
	}
else 
 break
}
Guicontrol,, MonthlyTotals
MonthlyTotals := ""
SetControlDelay, 20
;Start the LoopCalcNum at 11 since there's 10 other buttons first!
LoopCalcNum=11
loop
{ if (LoopCalcNum <= (SNumber + 10))
	{	Control, Check,, Button%LoopCalcNum%
		LoopCalcNum ++
		if (LoopCalcNum > (SNumber + 10))
			break
	}
else 
 break
}
Clear := False
gosub CalculateTotal
return

ButtonHide:
IfWinNotActive, InfoEntry
{
	Gui,Show
	return
}
	Gui, Hide
return

ButtonSummaryExport:
Gui, Submit, NoHide
FileAppend,
(
Roof	Tilt	Azim	Modules	Size	Prod	Hours`n
), %AR%_SYSTEM_SUMMARY.txt
LoopCalcNum=1
loop
{ if (LoopCalcNum <= SNumber)
	{
		LoopTilt = % RS%LoopCalcNum%T 
		LoopAzimuth = % RS%LoopCalcNum%A
		LoopModules = % RS%LoopCalcNum%M
		LoopSize = % RS%LoopCalcNum%Z
		LoopProd = % RS%LoopCalcNum%P
		LoopHours = % RS%LoopCalcNum%H
		FileAppend,
		(
		%LoopCalcNum%	%LoopTilt%	%LoopAzimuth%	%LoopModules%	%LoopSize%	%LoopProd%	%LoopHours%`n
		), %AR%_SYSTEM_SUMMARY.txt
		LoopCalcNum ++
		if (LoopCalcNum > SNumber)
			break
	}
else 
 break
}
return

^0::
if SNumber = 0
	SNumber := 1
LoopCalcNum=1
loop
{ if (LoopCalcNum <= SNumber)
	{	
		LoopModules = % RS%LoopCalcNum%M
		LoopProduction = % RS%LoopCalcNum%P
		LoopHours = % RS%LoopCalcNum%H
		If (LoopModules = "" OR LoopProduction = "")
			{break
			}
		If (LoopHours > 100 && LoopHours < 1000)
			{StringLeft, LoopHours, LoopHours, 3
			} else
			{StringLeft, LoopHours, LoopHours, 4
			}
		If (LoopHours < 100)
			{StringLeft, LoopHours, LoopHours, 2
			}
		sendinput, Section %LoopCalcNum%- %LoopModules% modules at %LoopProduction% kWh (%LoopHours% Sun-hours)`n
		LoopCalcNum ++
		if (LoopCalcNum > SNumber)
			break
	}
else 
 break
}
return

ButtonCommands:
MsgBox,,InfoEntry- Revision No. %Version%,Hold Control and Left-click on items on the Clipboard tab to see tooltips about each one (tips disappear in three seconds)`n`nKey Shortcuts:`nCtrl+Space: Show/Hide`tCtrl+S: Save`nF5/Ctrl+R: Refresh`t`tWindows+Q: Exit`nCtrl+1: S#`nCtrl+2: First and Last Name (useful for emails)`nCtrl+3: Context-sensitive entries for Solmetric, Eco-X, MSI, AutoCAD, Pictometry, Nearmaps, and Google (when used in these last three, it enters the address)`n(For Solmetric and Exo-X make sure you click in the AR box before using it! For AutoCAD, click in the Initials box first! For MSI, click in the first box of the Contact section on the last page!)`nCtrl+4: Monthly totals for Salesforce (read the Totals tab) and the Design Tool (Click in the Jan box before using)`nCtrl-Shift+4:Read the monthly totals boxes in the Salesforce CAD page (useful for as-built designs where the estimated production did not change).`nCtrl-Shift+3: For Permit Designs, Redesigns, or As-Builts, the address data can be read from the AutoCAD drawing properties box by selecting the S# and using the shortcut. The same shortcut used in a blank space in the Design Tool will retrieve the same information, as well as things like the Meter No. and Panel Ratings.`nNumpad Enter: Identical to  the Calculate button when double-tapped anywhere in the GUI.`n`n(In the Select Inverter menu of the PV Designer)`nShift+1: Choose 3800 inverter`nShift+2: Choose 7600 inverter`nShift+3: Choose 11400 inverter`n(In Cobblestone)`n\ : Go to the next SunEye`nFor the older Solmetric program, you can quickly change the inverter type by holding Shift (for Enphase), Control (for SolarEdge strings), or Alt (for SE's 10000 string) and right-clicking the Inverter number (note that this defaults to 260-Watt modules).`n`nCtrl+[Numpad 1-9]: Enter Roof Sections' 1-9 data into the Design Tool, Eco-X, or Salesforce.`nCtrl+Shift+[Numpad 0-9]: Enter Roof Sections' 10-19 data.`nAlt+[Roof Section 1-19 shortcuts]: Retrieve that Roof Section's data from the CAD Object page in Salesforce (click in the section's name first!).`n`n(For the Design Tool, start in the number-of-modules box; for Eco-X, start in the Average Roof Height box; for Salesforce, start in the Azimuth box. Keep in mind that the Salesforce shortcut defaults to the Trina 260-type modules!)`n`nIn AutoCAD, the up and down arrows can be used in the Drawing Properties page after clicking in a box to move up and down between boxes.`nAdditionally, the "``" (accent) key in the top-left corner of the keyboard behaves the same as the "`'" (single quote) key when AutoCAD is active, so entering dimensions can be done with the right hand on the number pad and the left hand in its rest position on the other side.
return

Button?:
AboutThis:
MsgBox,,InfoEntry- Revision No. %Version%,For quick information, control-left-click on something!`n`nFor use as a digital clipboard & text-entering shortcut tool for projects' commonly-used data.`n`tNote that it is intended to be both forward- and backward-compatible, so that the saved data from one version may be used with another. This is especially useful if the saved data is kept with the project, so that re-designed projects can be finished more quickly.`n`tThis GUI was largely made through trial-and-error over a period of ̷m̷o̷n̷t̷h̷s̷  years, with some portions extracted from the volunteered works made available by other Autohotkey users.`nThe Stats tab has the module rating and so on, in case they need to be adjusted on a per-design basis or when working on other offices' accounts. To adapt the script for your office's defaults, look at the beginning of the script (using Notepad) to find the Defaults and change them to match what you use.`n`nAny suggestions, ideas, and so on are welcome!`n`t`t`t`t`t`-DŠ
return

GuiEscape:
^Space::
GoSub ButtonHide
return

#q::
guiclose:
WinGetPos, winX, winY,
IniWrite, %BoardHide%, %A_MyDocuments%\IEUserSettings.ini, Main, BoardHide
IniWrite, %TaskBarHide%, %A_MyDocuments%\IEUserSettings.ini, Main, TaskBarHide
IniWrite, %winX%, %A_MyDocuments%\IEUserSettings.ini, Main, xPos
IniWrite, %winY%, %A_MyDocuments%\IEUserSettings.ini, Main, yPos
ExitApp
return

#IfWinActive, InfoEntry
`::'
#IfWinActive

#IfWinActive, InfoEntry
$NumpadEnter::
if NPE_Presses > 0
{
    NPE_Presses += 1
    return
}
NPE_Presses = 1
SetTimer, KeyNPE, 200 ; 200ms wait
return

KeyNPE:
SetTimer, KeyNPE, off
if NPE_Presses = 1
{
	send, {NumpadEnter}
}
else if NPE_Presses >= 2
{
	Gui, Submit, nohide
	GoSub CalculateAll
}
NPE_Presses = 0
return
#IfWinActive

#IfWinActive, InfoEntry
^s::
GoSub ButtonSave
return
#IfWinActive

^1::
Gui, Submit, NoHide
if (AR != "$NUM")
{
	sendinput %AR%
}
return

^2::
Gui, Submit, NoHide
sendinput %FirstName% %LastName%
return

^3::
Gui, Submit, NoHide
Process, Exist, SunEye.exe
PID1 := Errorlevel
;Eco-X's title page is "Eco X Client", use that instead of the PID
Process, Exist, acadlt.exe
PID2 := Errorlevel
Process, Exist, MountingSystemsAppHost.exe
PID3 := Errorlevel
Process, Exist, EXCEL.exe
PID4 := Errorlevel
IfWinActive ahk_pid %PID4%
{
	gosub EnterEcoFastenExcel
	return
}
IfWinActive ahk_pid %PID1%
{
	gosub EnterSOLMETRIC
	return
}
IfWinActive, Eco X Client,
{
	gosub EnterECOX
	return
}
IfWinActive, EcoFasten Calculator,
{
	gosub EnterECOFasten
	return
}
IfWinActive ahk_pid %PID2%
{
	gosub EnterAUTOCAD
	return
}
IfWinActive ahk_pid %PID3%
{
	gosub EnterMSI
	return
}
#if, WinActive("Cobblestone CAD Home") || WinActive("New Tab") || WinActive("PhotoMaps") || WinActive("Pictometry Online")
{
	If Address contains #,
		sendraw %Address%,
	else
		sendinput %Address%,
	sendinput %City% %StateAB%
	return
}
return

EnterEcoFastenExcel:
sendinput %AR%
sendinput {return 3}
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
sendinput {return}
sendinput %City%
sendinput {return}
sendinput %StateAB%
sendinput {return}
sendinput %Zip%
return

EnterSOLMETRIC:
sendinput %AR%
sendinput {tab}
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
sendinput {tab 2}
sendinput %City%
sendinput {tab}
sendinput %StateAB%
sendinput {tab}
sendinput %Zip%
return

EnterECOX:
Gui, Submit, NoHide
sendinput %AR%
sendinput {tab}
sendinput {down}
sendinput {tab 2}
sendinput %AR%
sendinput {tab 4}
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
sendinput {tab 2}
sendinput %City%
sendinput {tab }
sendinput {down %StateFull%}
sendinput {tab 2}
sendinput %Zip%
return

EnterECOFasten:
Gui, Submit, NoHide
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
sendinput {tab}
sendinput %City%
sendinput {tab }
sendinput %State%
sendinput {tab}
sendinput %Zip%
sendinput {tab}
sendinput -
sendinput {tab}
sendinput -
return

EnterMSI:
sendinput %AR%
sendinput {shift down}{tab 2}{shift up}
sendinput %City%, %StateAB%, %Zip%
sendinput {shift}{tab 3}
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
return

EnterAUTOCAD:
Gui, Submit, NoHide
If StateAB contains UT,
	GoSub EnterAUTOCAD_AZ
else {
BlockInput, MouseMove
sendinput %IN%
MouseMove, 0, 17, 0, R
Click
sendinput %AR%
MouseMove, 0, 17, 0, R
Click
sendinput %LastName%
MouseMove, 0, 17, 0, R
Click
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
MouseMove, 0, 17, 0, R
Click
sendinput %City%
MouseMove, 0, 34, 0, R
Click
sendinput %Zip%
BlockInput, MouseMoveOff
}
return
#IfWinActive


; Corrected by N Casados
EnterAUTOCAD_AZ:
Gui, Submit, NoHide
BlockInput, MouseMove
sendinput %IN%
MouseMove, 0, 17, 0, R
Click
sendinput %LastName%
MouseMove, 0, 17, 0, R
Click
sendinput %AR%
MouseMove, 0, 17, 0, R
Click
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
MouseMove, 0, 17, 0, R
Click
sendinput %City%
MouseMove, 0, 34, 0, R
Click
sendinput %Zip%
MouseMove, 0, 17, 0, R
Click
sendinput %Util%
MouseMove, 0, 17, 0, R
Click
sendinput, %Meter%
MouseMove, 0, 17, 0, R
Click
sendinput %RSSumM%
sendinput {WheelDown 6}
MouseMove, 0, -17, 0, R
Click
sendinput %RS1T%
MouseMove, 0, 17, 0, R
Click
sendinput %RS1A%
MouseMove, 0, 17, 0, R
Click
sendinput %RS1M%
MouseMove, 0, 17, 0, R
Click
sendinput %RS1H%
MouseMove, 0, 17, 0, R
Click
sendinput %RS2T%
MouseMove, 0, 17, 0, R
Click
sendinput %RS2A%
sendinput {WheelDown 4}
MouseMove, 0, -187, 0, R
Click
sendinput %RS2M%
MouseMove, 0, 17, 0, R
Click
sendinput %RS2H%
MouseMove, 0, 17, 0, R
Click
sendinput %RS3T%
MouseMove, 0, 17, 0, R
Click
sendinput %RS3A%
MouseMove, 0, 17, 0, R
Click
sendinput %RS3M%
MouseMove, 0, 17, 0, R
Click
sendinput %RS3H%
MouseMove, 0, 17, 0, R
Click
sendinput %RS4T%
MouseMove, 0, 17, 0, R
Click
sendinput %RS4A%
MouseMove, 0, 17, 0, R
Click
sendinput %RS4M%
MouseMove, 0, 17, 0, R
Click
sendinput %RS4H%
MouseMove, 0, 17, 0, R
Click
sendinput %RS5T%
MouseMove, 0, 17, 0, R
Click
sendinput %RS5A%
MouseMove, 0, 17, 0, R
Click
sendinput %RS5M%
MouseMove, 0, 17, 0, R
Click
sendinput %RS5H%
sendinput {WheelDown 5}
MouseMove, 0, -238, 0, R
Click
sendinput %RS6T%
MouseMove, 0, 17, 0, R
Click
sendinput %RS6A%
MouseMove, 0, 17, 0, R
Click
sendinput %RS6M%
MouseMove, 0, 17, 0, R
Click
sendinput %RS6H%
MouseMove, 0, 17, 0, R
Click
sendinput %RS7T%
MouseMove, 0, 17, 0, R
Click
sendinput %RS7A%
MouseMove, 0, 17, 0, R
Click
sendinput %RS7M%
MouseMove, 0, 17, 0, R
Click
sendinput %RS7H%
MouseMove, 0, 17, 0, R
Click
sendinput %RS8T%
MouseMove, 0, 17, 0, R
Click
sendinput %RS8A%
MouseMove, 0, 17, 0, R
Click
sendinput %RS8M%
MouseMove, 0, 17, 0, R
Click
sendinput %RS8H%
MouseMove, 0, 17, 0, R
Click
sendinput %RS9T%
MouseMove, 0, 17, 0, R
Click
sendinput %RS9A%
MouseMove, 0, 17, 0, R
Click
sendinput %RS9M%
MouseMove, 0, -34, 0, R
Click
sendinput %RS9H%
SendInput, {WheelDown 10}		
MouseMove, 0, 17, 0, R		
Click		
SendInput, %OffsetPercentage%
Send {Enter}
BlockInput, MouseMoveOff
return

; Insert from N.Casados
; Send SolarEdge 3800 in PV Designer
#IfWinActive Select Inverter
+1::
sendInput, {tab}
sendInput, s
sendInput, {tab}
sendInput, {home}
sendInput, {down 2}
return
#IfWinActive

; Send SolarEdge 7600 in PV Designer
#IfWinActive Select Inverter
+2::
sendInput, {tab}
sendInput, s
sendInput, {tab}
sendInput, {home}
sendInput, {down 6}
return
#IfWinActive

; Send SolarEdge 11400 in PV Designer
#IfWinActive Select Inverter
+3::
sendInput, {tab}
sendInput, s
sendInput, {tab}
sendInput, {home}
sendInput, {down 1}
return
#IfWinActive

; Advances forward one SunEye in Cobblestone
#IfWinActive Cobblestone CAD Home
\::
Send {Right 1}
return
#IfWinActive
; End N.Casados code insert

^4::
IfWinActive, Prototype CAD Edit ~ Salesforce - Unlimited Edition - Google Chrome,
{
	Gui, Submit, nohide
	sendinput %MonthlyTotals%
	; faster than using 'sendraw'
}
	Process, Exist, Excel.exe
	PID4 := Errorlevel
	IfWinActive ahk_pid %PID4%
{
	Gui, Submit, nohide
	sendinput %MonthlyTotals%
}
return

^+4::
Gui, Submit, nohide
MonthlyTotals =
Clipboard =
send +{tab}
loop, 12
{
	send {tab}^c
	ClipWait, 1
	If (MonthlyTotals = "") {
	MonthlyTotals := Clipboard
	} else {
	MonthlyTotals := MonthlyTotals . "`t" . Clipboard
	}
}
GuiControl,, MonthlyTotals,%MonthlyTotals%
return

^NumpadIns:: ;aka Ctrl-Shift-Numpad0
^NumpadEnd:: ;aka Ctrl-Shift-Numpad1
^NumpadDown:: ;aka Ctrl-Shift-Numpad2
^NumpadPgDn:: ;aka Ctrl-Shift-Numpad3
^NumpadLeft:: ;aka Ctrl-Shift-Numpad4
^NumpadClear:: ;aka Ctrl-Shift-Numpad5
^NumpadRight:: ;aka Ctrl-Shift-Numpad6
^NumpadHome:: ;aka Ctrl-Shift-Numpad7
^NumpadUp:: ;aka Ctrl-Shift-Numpad8
^NumpadPgUp:: ;aka Ctrl-Shift-Numpad9
keywait, Shift, ;Or else it will send the same thing as Shift+NUMBER!
^Numpad1::
^Numpad2::
^Numpad3::
^Numpad4::
^Numpad5::
^Numpad6::
^Numpad7::
^Numpad8::
^Numpad9::
StringReplace, SectionNo, A_ThisHotkey, ^Numpad,,A
StringReplace, SectionNo, SectionNo, PgUp,19,A
StringReplace, SectionNo, SectionNo, Up,18,A
StringReplace, SectionNo, SectionNo, Home,17,A
StringReplace, SectionNo, SectionNo, Right,16,A
StringReplace, SectionNo, SectionNo, Clear,15,A
StringReplace, SectionNo, SectionNo, Left,14,A
StringReplace, SectionNo, SectionNo, PgDn,13,A
StringReplace, SectionNo, SectionNo, Down,12,A
StringReplace, SectionNo, SectionNo, End,11,A
StringReplace, SectionNo, SectionNo, Ins,10,A
If (RS%SectionNo%Box = 0)
	{
		return
	}
Gui, Submit, NoHide
	LoopSpacing := SectionNo
	LoopLHD := SectionNo
	LoopHeight := SectionNo
	loop,
	{
		if LoopSpacing > 0
		{
			if (RS%LoopSpacing%R != "")
			{RSPC := RS%LoopSpacing%R
			break
			return
			}
			else
			{ LoopSpacing := LoopSpacing - 1
			}
		}
		else break
	}
	loop,
	{
		if LoopLHD > 0
		{
			if (RS%LoopLHD%D != "")
			{LHD := RS%LoopLHD%D
			break
			return
			}
			else
			{ LoopLHD := LoopLHD - 1
			}
		}
		else break
	}
	loop,
	{
		if LoopHeight > 0
		{
			if (RS%LoopHeight%V != "")
			{AVRH := RS%LoopHeight%V
			break
			return
			}
			else
			{ LoopHeight := LoopHeight - 1
			}
		}
		else break
	}
	LoopAzimuth := RS%SectionNo%A
	LoopTilt := RS%SectionNo%T
	LoopProduction := RS%SectionNo%P
	LoopHours := RS%SectionNo%H
	LoopSize := RS%SectionNo%S
	LoopModules := RS%SectionNo%M
	LoopMounting := RS%SectionNo%F
	Process, Exist, acadlt.exe
	PID2 := Errorlevel
	IfWinActive ahk_pid %PID2%
	{
		If StateAB contains UT
		{
			sendinput %LoopTilt%
			MouseMove, 0, 17, 0, R
			Click
			sendinput %LoopAzimuth%
			MouseMove, 0, 17, 0, R
			Click		
			sendinput %LoopModules%
			MouseMove, 0, 17, 0, R
			Click		
			sendinput %LoopHours%
			MouseMove, 0, 17, 0, R
			Click	
			return
		}
	}
	IfWinActive, Prototype CAD Edit,
	{
		sendinput %LoopAzimuth%
		sendinput {tab}
		sendinput %LoopTilt%
		sendinput {tab 3}
		sendinput %LoopProduction%
		sendinput {tab}
		sendinput %LoopSize%
		sendinput {tab}
		sendinput {down %DownNumber%}
		sendinput {tab}
		sendinput  %LoopModules%
		SendInput {tab}
		return
	}
	Process, Exist, Excel.exe
	PID4 := Errorlevel
	IfWinActive ahk_pid %PID4%
	{
		sendinput %LoopModules%
		sendinput {tab}
		sendinput %LoopTilt%
		sendinput {tab}
		sendinput %LoopAzimuth%
		sendinput {tab}
		sendinput %LoopProduction%
		sendinput {tab 3}
		sendinput %LoopMounting%
		sendinput {Enter}
		return
	}
	IfWinActive, Eco X Client,
	{
	sendinput %AVRH%
	sendinput {tab 2}
	sendinput %LHD%
	sendinput {tab 2}
	sendinput %LoopTilt%
	sendinput {tab 6}
	sendinput %RSPC%
	sendinput {tab 2}
	sendinput {space}
	if (StateFull = 21) or (StateFull = 31)
		{
		sendinput {tab}
		sendinput {space}
		}
	}
return

#IfWinActive, InfoEntry
^LButton::
MouseGetPos,,,,HelperText
ControlGetText, HelperText, %HelperText%, InfoEntry
if HelperText = Down Number:
{tooltip How many times you would have to press 'down'`nto select module type in Salesforce
}
else if HelperText = Utl#:
{tooltip Utility number
}
else if HelperText = Mtr#:
{tooltip Meter number
}
else if HelperText = Main Disc/Busbar
{tooltip Main Disconnect and Busbar ratings
}
else if HelperText = DI:
{tooltip Designer's initials
}
else if HelperText = S#:
{tooltip Refresh the GUI to have this many roof sections.
}
else if HelperText = `n`n`n`n`n`n`n`n`nSummary Export
{tooltip Export the system's roof section data to a text file.
}
else if HelperText = `n`n`n`n`n`nClipboard
{tooltip Show/hide the clipboard to the right
}
else if HelperText = `n`n`n`n`n`nTaskbar
{tooltip Show/hide the GUI's Taskbar icon and text
}
else if HelperText = `n`n`n`n`n`n`nCommands
{tooltip View commands and shortcuts
}
else if HelperText = `n`n`n`n`nRefresh
{tooltip Refresh the tool without saving anything
}
else if HelperText = `n`n`n`n`nSave
{tooltip Save information to a separate .ini file
}
else if HelperText = `n`n`n`n`nCLEAR
{tooltip Clear/reset all fields
}
else if HelperText = `n`n`n`n`nHide
{tooltip Hide the GUI `n(show again with Ctrl+Space)
}
else if HelperText = `n`n`n`n`n?
{tooltip About the tool
}
else if HelperText = #:
{tooltip Service / AR Number for the account
}
else if HelperText = Name:
{tooltip Customer's first and last name
}
else if HelperText = Goal:
{tooltip Production goal as specified in the Design Tool.
}
else if HelperText = Usage:
{tooltip Annual usage as specified in the Design Tool;`nused for calculating offset percentage and`nlater in the CAD page.
}
else if HelperText = Totals:
{tooltip Totals of each roof section's information.`nCheck or uncheck a roof section below`nto add or remove it from the calculation.
}
else if HelperText = Eyes
{tooltip Sun eyes belonging to`na roof section (i.e. 1-5; 6-10).
}
else if HelperText = Tilt°
{tooltip Tilt in degrees of each roof section.
}
else if HelperText = Azim°
{tooltip Azimuth in degrees of each roof section.
}
else if HelperText = Mods
{tooltip Number of modules on each roof section.
}
else if HelperText = Prod.
{tooltip Solmetric-estimated production for each roof section.
}
else if HelperText = (Size)
{tooltip Calculated size of each roof section.
}
else if HelperText = (Hours)
{tooltip Calculated sun-hours of each roof section.
}
else if HelperText = S`%
{tooltip Solmetric-reported shading percentage`nfor each roof section.
}
else if HelperText = AP
{tooltip Number of attachment points / mounting feet`nfor each roof section.
}
else if HelperText = Avg'
{tooltip Average roof height in whole feet for each roof section;`nif left empty subsequent roof sections will inherit the`nnumber from the previous roof section.
}
else if HelperText = LHD'
{tooltip Least horizontal dimension in whole feet for each`nroof section; will also be inherited if left blank.
}
else if HelperText = Ra"
{tooltip Rafter spacing in whole inches for each roof section;`nwill also be inherited if left blank.
}
else if HelperText = Module Rating (Watts):
{tooltip Module Rating in Watts (i.e. 250; 255; or 260).
}
else if HelperText = Module Efficiency`%:
{tooltip Module minimum efficiency as a percentage (0.80; 0.85; 0.95).
}
else if HelperText = Per-Module Minimum Production:
{tooltip Minimum per-module production; calculated from above rating and efficiency.
}
else if HelperText = Section Minimum:
{tooltip Minimum production in sun hours for each roof section.
}
else if HelperText = System Minimum:
{tooltip Minimum production in sun hours for the entire system.
}
else tooltip Ctrl-Click on something to see available information.
; remove the tooltip after 3 seconds
SetTimer, RemoveToolTip, 3000
return
#IfWinActive

RemoveToolTip:
SetTimer, RemoveToolTip, Off
ToolTip
return

#IfWinActive ahk_class AfxMDIFrame100u
`::'
#IfWinActive

#IfWinActive ahk_class AfxMDIFrame110u
`::'
#IfWinActive

Up::
Process, Exist, acadlt.exe
PID2 := Errorlevel
IfWinActive ahk_pid %PID2%
{
	MouseMove, 0, -17, 0, R
	Click
	return
}
else send {Up}
return

Down::
Process, Exist, acadlt.exe
PID2 := Errorlevel
IfWinActive ahk_pid %PID2%
{
	MouseMove, 0, 17, 0, R
	Click
	return
}
else send {Down}
return

~F5::
^r::
^RButton::
IfWinActive, InfoEntry
{
GoSub ButtonRefresh
return
}
Process, Exist, SunEye.exe
PID1 := Errorlevel
IfWinActive ahk_pid %PID1%
{
	GoSub CalculateAll
	If (RSSumM <= 7)
		{Msgbox, Too few modules!`nCancel the design!
		return
		}
	else
	{MouseClick, right
	sendinput {down}
	sendinput {enter}
	sendinput {tab}
	sendinput "solaredge"
	sendinput {tab}
	Sleep, 300
		If ((RSSumM >= 8) and (RSSumM <= 19))
			sendinput {down 28} ;Set3800Inverter
		If ((RSSumM >= 20) and (RSSumM <= 30))
			sendinput {down 31} ;Set6000Inverter
		If ((RSSumM >= 31) and (RSSumM <= 38))
			sendinput {down 32} ;Set7600Inverter
		If ((RSSumM >= 39) and (RSSumM <= 57))
			sendinput {down 27} ;Set11400Inverter
		If ((RSSumM >= 58) and (RSSumM <= 76))
			sendinput {down 32} ;Set7600Inverter
	sendinput {enter}
	Sleep, 700
	MouseClick, left
	sendinput {left}
	sendinput {down}
	MouseMove, 0, 16, 0, R
	return
	}
}
return

#IfWinActive Solmetric SunEye
+RButton::
	Keywait RButton
	MouseClick, right
	sendinput {down}
	sendinput {enter}
	sendinput {tab}
	sendinput "enphase"
	sendinput {tab}
	sendinput {down 16}
	sendinput {enter}
	Sleep, 700
	MouseClick, left
	sendinput {left}
	sendinput {down}
	return
return
#IfWinActive

!RButton::
Process, Exist, SunEye.exe
PID1 := Errorlevel
IfWinActive ahk_pid %PID1%
{
	MouseClick, right
	sendinput {down}
	sendinput {enter}
	sendinput {tab}
	sendinput "solaredge"
	sendinput {tab}
	sendinput {down 26}
	sendinput {enter}
	Sleep, 700
	MouseClick, left
	sendinput {left}
	sendinput {down}
	return
}
else send {Alt}{RButton}
return

GuiButtonIcon(Handle, File, Index := 1, Options := "" )
{
	RegExMatch(Options, "i)w\K\d+", W), (W="") ? W := 16 :
	RegExMatch(Options, "i)h\K\d+", H), (H="") ? H := 16 :
	RegExMatch(Options, "i)s\K\d+", S), S ? W := H := S :
	RegExMatch(Options, "i)l\K\d+", L), (L="") ? L := 0 :
	RegExMatch(Options, "i)t\K\d+", T), (T="") ? T := 0 :
	RegExMatch(Options, "i)r\K\d+", R), (R="") ? R := 0 :
	RegExMatch(Options, "i)b\K\d+", B), (B="") ? B := 0 :
	RegExMatch(Options, "i)a\K\d+", A), (A="") ? A := 4 :
	Psz := A_PtrSize = "" ? 4 : A_PtrSize, DW := "UInt", Ptr := A_PtrSize = "" ? DW : "Ptr"
	VarSetCapacity( button_il, 20 + Psz, 0 )
	NumPut( normal_il := DllCall( "ImageList_Create", DW, W, DW, H, DW, 0x21, DW, 1, DW, 1 ), button_il, 0, Ptr )	; Width & Height
	NumPut( L, button_il, 0 + Psz, DW )		; Left Margin
	NumPut( T, button_il, 4 + Psz, DW )		; Top Margin
	NumPut( R, button_il, 8 + Psz, DW )		; Right Margin
	NumPut( B, button_il, 12 + Psz, DW )	; Bottom Margin	
	NumPut( A, button_il, 16 + Psz, DW )	; Alignment
	SendMessage, BCM_SETIMAGELIST := 5634, 0, &button_il,, AHK_ID %Handle%
	return IL_Add( normal_il, File, Index )
}

ButtonClipboard:
if (BoardHide = 1)
	{BoardHide = 0
	Gui, Hide
	Gui, Show, w880
	return
	} else {BoardHide = 1
	Gui, Hide
	Gui, Show, w560
	return
	}
return

ButtonTaskbar:
if (TaskBarHide = 1)
	{TaskBarHide = 0
	Gui, +ToolWindow
	Gui, Show,
	return
	} else {TaskBarHide = 1
	Gui, -ToolWindow
	Gui, Show,
	return
	}
return

~^!LButton::
Gui, Submit, NoHide
Clipboard=
keywait LButton
IfWinActive ahk_class AcrobatSDIWindow
{
	sleep 50
	send ^c
	ClipWait
	GuiControl,, MonthlyTotals,%Clipboard%
	Gui, Submit, nohide
	StringReplace, MonthlyTotals, MonthlyTotals, `n, `t, A
	GuiControl,, MonthlyTotals,%MonthlyTotals%
	return
}
return

!F2::
send {F2}%FileName%{Return}
return


^+3::
Process, Exist, excel.exe
PID1 := Errorlevel
Process, Exist, acadlt.exe
PID2 := Errorlevel
keywait alt
IfWinActive ahk_pid %PID1%
{
	Gui, Submit, NoHide
	XL := ComObjActive("Excel.Application")
	;XL1 = PreDesignInput
	;XL2 = Predesign
	;XL.Sheets(XL1).Activate
	Util := XL.Range("UtilityAccountNumber").Value
	Guicontrol,,Util,%Util%
	Meter := XL.Range("UtilityMeter").value
	Guicontrol,,Meter,%Meter%
	Address := XL.Range("Address").value
	Guicontrol,,Address,%Address%
	City := XL.Range("City").value
	Guicontrol,,City,%City%
	Zip := XL.Range("Zip").value
	Guicontrol,,Zip,%Zip%
	MBusb := XL.Range("Busbar").value
	StringTrimRight, MBusb, MBusb, 7
	Guicontrol,,MBusb,%MBusb%
	MDisc := XL.Range("MainDisc").value
	StringTrimRight, MDisc, MDisc, 7
	Guicontrol,,MDisc,%MDisc%
	GoalMin := XL.Range("CustomerUsage").value
	StringTrimRight, GoalMin, GoalMin, 7
	Guicontrol,,GoalMin,%GoalMin%
	Annual := XL.Range("CustomerUsage").value
	StringTrimRight, Annual, Annual, 7
	Guicontrol,,Annual,%Annual%
	AR := XL.Range("AR").value
	StringTrimLeft, AR, AR,2
	GuiControl,,AR,%AR%
	FullName := XL.Range("CustomerName").value
	StringSplit, FullName, FullName, %A_Space%
	If (FullName5 != "")
	   GuiControl,,LastName,%FullName2% %FullName3% %FullName4% %FullName5%
	else
	{
		If (FullName4 != "")
		   GuiControl,,LastName,%FullName2% %FullName3% %FullName4%
		else
		{
			If (FullName3 != "")
			   GuiControl,,LastName,%FullName2% %FullName3%
			else
			{
				   GuiControl,,LastName,%FullName2%
			}
		}
	}
	GuiControl,,FirstName,%FullName1%
}
IfWinActive ahk_pid %PID2%
{
	Gui, Submit, NoHide
	send ^c
	ClipWait, 1
	Guicontrol,,AR,%Clipboard%
	MouseMove, 0, 17, 0, R
	Click
	send ^c
	ClipWait, 1
	Guicontrol,,LastName,%Clipboard%
	MouseMove, 0, 17, 0, R
	Click
	send ^c
	ClipWait, 1
	Guicontrol,,Address,%Clipboard%
	MouseMove, 0, 17, 0, R
	Click
	send ^c
	ClipWait, 1
	Guicontrol,,City,%Clipboard%
	MouseMove, 0, 34, 0, R
	Click
	send ^c
	ClipWait, 1
	Guicontrol,,Zip,%Clipboard%
}
return


^!Numpad1::
^!Numpad2::
^!Numpad3::
^!Numpad4::
^!Numpad5::
^!Numpad6::
^!Numpad7::
^!Numpad8::
StringReplace, SectionNo, A_ThisHotkey, ^!Numpad,,A
keywait, Alt, ;Won't trigger until Alt is released
RS%SectionNo%Box := 0
Process, Exist, excel.exe
PID1 := Errorlevel
IfWinActive ahk_pid %PID1%
{
Gui, Submit, NoHide
; Retrieves a roof's section's data and stores it
; in the number row of whichever was used as the
; hotkey trigger
send ^c
ClipWait, 1
send {esc}
StringReplace, Clipboard, Clipboard, %A_Tab%,,A
StringReplace, Clipboard, Clipboard, `r`n,,A
sleep 15
Guicontrol,,RS%SectionNo%M,%Clipboard%
send {tab}^c
ClipWait, 1
send {esc}
StringReplace, Clipboard, Clipboard, %A_Tab%,,A
StringReplace, Clipboard, Clipboard, `r`n,,A
sleep 15
Guicontrol,,RS%SectionNo%T,%Clipboard%
send {tab}^c
ClipWait, 1
send {esc}
StringReplace, Clipboard, Clipboard, %A_Tab%,,A
StringReplace, Clipboard, Clipboard, `r`n,,A
sleep 15
Guicontrol,,RS%SectionNo%A,%Clipboard%
send {tab}^c
ClipWait, 1
send {esc}
StringReplace, Clipboard, Clipboard, %A_Tab%,,A
StringReplace, Clipboard, Clipboard, `r`n,,A
sleep 15
Guicontrol,,RS%SectionNo%P,%Clipboard%
send {tab 3}^c
ClipWait, 1
send {esc}
StringReplace, Clipboard, Clipboard, %A_Tab%,,A
StringReplace, Clipboard, Clipboard, `r`n,,A
sleep 15
Guicontrol,,RS%SectionNo%F,%Clipboard%
}
IfWinActive, Prototype CAD Edit
{
Gui, Submit, NoHide
send {tab 2}^c
Guicontrol,,RS%SectionNo%A,%Clipboard%
send {tab}
send ^c
Guicontrol,,RS%SectionNo%T,%Clipboard%
send {tab 3}
send ^c
StringReplace, Clipboard, Clipboard, `,,,A ;Remove comma
StringTrimRight,Clipboard,Clipboard,2 ;Remove last two zeroes
Guicontrol,,RS%SectionNo%P,%Clipboard%	
send {tab}
send ^c
StringTrimRight,Clipboard,Clipboard,4 ;Remove zeroes and decimal
Guicontrol,,RS%SectionNo%S,%Clipboard%
send {tab 11}^c
Guicontrol,,RS%SectionNo%M,%Clipboard%
RS%SectionNo%Box = 1
}
return
;#IfWinActive

!L::
listvars
return