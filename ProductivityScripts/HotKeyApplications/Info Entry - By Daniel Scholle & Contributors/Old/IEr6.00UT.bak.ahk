﻿#SingleInstance Force
#NoEnv
Menu, Tray, Icon, shell32.dll, 261
; Name the .ini file based on whatever this script is named.
; This is important since it will generate a .ini file to
; save, load, and refresh information using the same name.
FileName = % SubStr(A_ScriptName, 1, -4)

; Version numbering
Version = 6.00
/* CHANGE NOTES:
Added a bunch of features for UT CAD.
*/

; ###Defaults###
; What the different fields will default to if the .ini file
; is not present when the tool is first run
DefStateFull=46 		;How many lines down in the drop-down list is the State?
						;(search for ADAPTATION_note to see a list with numbers and abbreviations)
DefStateAB=UT			;State's two-letter abbreviation.
DefRating=0.265			;Default module rating in kiloWatts
DefEfficiency=0.85		;Default module efficiency as a percentage
DefSectionMinimum=950	;Default section minimum production sun hours
DefSystemMinimum=950	;Default system minimum production sun hours
MM=6 					;Minimum modules system-wide, for use in the totals calculation
DefDownNumber=11 		;Down Number- how many times would you have to push down in
						; the SalesForce drop-down list to pick your office's default module type.
						
; ###LOTS AND LOTS OF LOADING###
; First is the GUI's position and other basic stuff
; that doesn't carry over from one design to another
iniread, SavedIN, %A_MyDocuments%\IEUserSettings.ini, Main, IN, ---
iniread, BoardHide, %A_MyDocuments%\IEUserSettings.ini, Main, BoardHide,0
iniread, TaskBarHide, %A_MyDocuments%\IEUserSettings.ini, Main, TaskBarHide,0
iniread, winX, %A_MyDocuments%\IEUserSettings.ini, Main, xPos, 200
iniread, winY, %A_MyDocuments%\IEUserSettings.ini, Main, yPos, 200
iniread, BGPIC, %A_MyDocuments%\IEUserSettings.ini, Main, SavedBackground, BLANK
SplitPath, BGPIC, BGPICText
iniread, SavedValueR, %A_MyDocuments%\IEUserSettings.ini, Color, ValueR,128
iniread, SavedValueG, %A_MyDocuments%\IEUserSettings.ini, Color, ValueG,128
iniread, SavedValueB, %A_MyDocuments%\IEUserSettings.ini, Color, ValueB,128
iniread, SavedValueGR, %A_MyDocuments%\IEUserSettings.ini, Color, ValueGR,128
iniread, SavedBackColor, %A_MyDocuments%\IEUserSettings.ini, Color, BackColor, A0A0A0
iniread, SavedFontColor, %A_MyDocuments%\IEUserSettings.ini, Color, FontColor, 000000
; Next is the design-specific stuff
iniread, DownNumber, %FileName%.ini, Main, DownNumber, %DefDownNumber%
iniread, StateFull, %FileName%.ini, Main, StateFull, %DefStateFull%
iniread, StateAB, %FileName%.ini, Main, StateAB, %DefStateAB%
iniread, Rating, %FileName%.ini, Main, Rating,%DefRating%
iniread, Efficiency, %FileName%.ini, Main, Efficiency,%DefEfficiency%
iniread, SectionMinimum, %FileName%.ini, Main, SectionMinimum, %DefSectionMinimum%
iniread, SystemMinimum, %FileName%.ini, Main, SystemMinimum, %DefSystemMinimum%
iniread, SavedAR, %FileName%.ini, Main, AR, 3214567
iniread, SavedLastName, %FileName%.ini, Main, LastName, Doe
iniread, SavedAddress, %FileName%.ini, Main, Address, 123 W Fake Street 
iniread, SavedCity, %FileName%.ini, Main, City, Nowhere
iniread, SavedZip, %FileName%.ini, Main, Zip, 00000
iniread, GoalMin, %FileName%.ini, Main, GoalMin,9001
iniread, Annual, %FileName%.ini, Main, Annual,10000
iniread, SavedRoofSection1T, %FileName%.ini, Main, RoofSection1T,%A_Space%
iniread, SavedRoofSection1A, %FileName%.ini, Main, RoofSection1A,%A_Space%
iniread, SavedRoofSection1E, %FileName%.ini, Main, RoofSection1E,%A_Space%
iniread, SavedRoofSection1M, %FileName%.ini, Main, RoofSection1M,%A_Space%
iniread, SavedRoofSection1Z, %FileName%.ini, Main, RoofSection1Z,%A_Space%
iniread, SavedRoofSection1P, %FileName%.ini, Main, RoofSection1P,%A_Space%
iniread, SavedRoofSection1S, %FileName%.ini, Main, RoofSection1S,%A_Space%
iniread, SavedRoofSection1F, %FileName%.ini, Main, RoofSection1F,%A_Space%
iniread, SavedRoofSection1H, %FileName%.ini, Main, RoofSection1H,%A_Space%
iniread, SavedRoofSection1V, %FileName%.ini, Main, RoofSection1V,%A_Space%
iniread, SavedRoofSection1D, %FileName%.ini, Main, RoofSection1D,%A_Space%
iniread, SavedRoofSection1R, %FileName%.ini, Main, RoofSection1R,%A_Space%
iniread, SavedRoofSection2T, %FileName%.ini, Main, RoofSection2T,%A_Space%
iniread, SavedRoofSection2A, %FileName%.ini, Main, RoofSection2A,%A_Space%
iniread, SavedRoofSection2E, %FileName%.ini, Main, RoofSection2E,%A_Space%
iniread, SavedRoofSection2M, %FileName%.ini, Main, RoofSection2M,%A_Space%
iniread, SavedRoofSection2Z, %FileName%.ini, Main, RoofSection2Z,%A_Space%
iniread, SavedRoofSection2P, %FileName%.ini, Main, RoofSection2P,%A_Space%
iniread, SavedRoofSection2S, %FileName%.ini, Main, RoofSection2S,%A_Space%
iniread, SavedRoofSection2F, %FileName%.ini, Main, RoofSection2F,%A_Space%
iniread, SavedRoofSection2H, %FileName%.ini, Main, RoofSection2H,%A_Space%
iniread, SavedRoofSection2V, %FileName%.ini, Main, RoofSection2V,%A_Space%
iniread, SavedRoofSection2D, %FileName%.ini, Main, RoofSection2D,%A_Space%
iniread, SavedRoofSection2R, %FileName%.ini, Main, RoofSection2R,%A_Space%
iniread, SavedRoofSection3T, %FileName%.ini, Main, RoofSection3T,%A_Space%
iniread, SavedRoofSection3A, %FileName%.ini, Main, RoofSection3A,%A_Space%
iniread, SavedRoofSection3E, %FileName%.ini, Main, RoofSection3E,%A_Space%
iniread, SavedRoofSection3M, %FileName%.ini, Main, RoofSection3M,%A_Space%
iniread, SavedRoofSection3Z, %FileName%.ini, Main, RoofSection3Z,%A_Space%
iniread, SavedRoofSection3P, %FileName%.ini, Main, RoofSection3P,%A_Space%
iniread, SavedRoofSection3S, %FileName%.ini, Main, RoofSection3S,%A_Space%
iniread, SavedRoofSection3F, %FileName%.ini, Main, RoofSection3F,%A_Space%
iniread, SavedRoofSection3H, %FileName%.ini, Main, RoofSection3H,%A_Space%
iniread, SavedRoofSection3V, %FileName%.ini, Main, RoofSection3V,%A_Space%
iniread, SavedRoofSection3D, %FileName%.ini, Main, RoofSection3D,%A_Space%
iniread, SavedRoofSection3R, %FileName%.ini, Main, RoofSection3R,%A_Space%
iniread, SavedRoofSection4T, %FileName%.ini, Main, RoofSection4T,%A_Space%
iniread, SavedRoofSection4A, %FileName%.ini, Main, RoofSection4A,%A_Space%
iniread, SavedRoofSection4E, %FileName%.ini, Main, RoofSection4E,%A_Space%
iniread, SavedRoofSection4M, %FileName%.ini, Main, RoofSection4M,%A_Space%
iniread, SavedRoofSection4Z, %FileName%.ini, Main, RoofSection4Z,%A_Space%
iniread, SavedRoofSection4P, %FileName%.ini, Main, RoofSection4P,%A_Space%
iniread, SavedRoofSection4S, %FileName%.ini, Main, RoofSection4S,%A_Space%
iniread, SavedRoofSection4F, %FileName%.ini, Main, RoofSection4F,%A_Space%
iniread, SavedRoofSection4H, %FileName%.ini, Main, RoofSection4H,%A_Space%
iniread, SavedRoofSection4V, %FileName%.ini, Main, RoofSection4V,%A_Space%
iniread, SavedRoofSection4D, %FileName%.ini, Main, RoofSection4D,%A_Space%
iniread, SavedRoofSection4R, %FileName%.ini, Main, RoofSection4R,%A_Space%
iniread, SavedRoofSection5T, %FileName%.ini, Main, RoofSection5T,%A_Space%
iniread, SavedRoofSection5A, %FileName%.ini, Main, RoofSection5A,%A_Space%
iniread, SavedRoofSection5E, %FileName%.ini, Main, RoofSection5E,%A_Space%
iniread, SavedRoofSection5M, %FileName%.ini, Main, RoofSection5M,%A_Space%
iniread, SavedRoofSection5Z, %FileName%.ini, Main, RoofSection5Z,%A_Space%
iniread, SavedRoofSection5P, %FileName%.ini, Main, RoofSection5P,%A_Space%
iniread, SavedRoofSection5S, %FileName%.ini, Main, RoofSection5S,%A_Space%
iniread, SavedRoofSection5F, %FileName%.ini, Main, RoofSection5F,%A_Space%
iniread, SavedRoofSection5H, %FileName%.ini, Main, RoofSection5H,%A_Space%
iniread, SavedRoofSection5V, %FileName%.ini, Main, RoofSection5V,%A_Space%
iniread, SavedRoofSection5D, %FileName%.ini, Main, RoofSection5D,%A_Space%
iniread, SavedRoofSection5R, %FileName%.ini, Main, RoofSection5R,%A_Space%
iniread, SavedRoofSection6T, %FileName%.ini, Main, RoofSection6T,%A_Space%
iniread, SavedRoofSection6A, %FileName%.ini, Main, RoofSection6A,%A_Space%
iniread, SavedRoofSection6E, %FileName%.ini, Main, RoofSection6E,%A_Space%
iniread, SavedRoofSection6M, %FileName%.ini, Main, RoofSection6M,%A_Space%
iniread, SavedRoofSection6Z, %FileName%.ini, Main, RoofSection6Z,%A_Space%
iniread, SavedRoofSection6P, %FileName%.ini, Main, RoofSection6P,%A_Space%
iniread, SavedRoofSection6S, %FileName%.ini, Main, RoofSection6S,%A_Space%
iniread, SavedRoofSection6F, %FileName%.ini, Main, RoofSection6F,%A_Space%
iniread, SavedRoofSection6H, %FileName%.ini, Main, RoofSection6H,%A_Space%
iniread, SavedRoofSection6V, %FileName%.ini, Main, RoofSection6V,%A_Space%
iniread, SavedRoofSection6D, %FileName%.ini, Main, RoofSection6D,%A_Space%
iniread, SavedRoofSection6R, %FileName%.ini, Main, RoofSection6R,%A_Space%
iniread, SavedRoofSection7T, %FileName%.ini, Main, RoofSection7T,%A_Space%
iniread, SavedRoofSection7A, %FileName%.ini, Main, RoofSection7A,%A_Space%
iniread, SavedRoofSection7E, %FileName%.ini, Main, RoofSection7E,%A_Space%
iniread, SavedRoofSection7M, %FileName%.ini, Main, RoofSection7M,%A_Space%
iniread, SavedRoofSection7Z, %FileName%.ini, Main, RoofSection7Z,%A_Space%
iniread, SavedRoofSection7P, %FileName%.ini, Main, RoofSection7P,%A_Space%
iniread, SavedRoofSection7S, %FileName%.ini, Main, RoofSection7S,%A_Space%
iniread, SavedRoofSection7F, %FileName%.ini, Main, RoofSection7F,%A_Space%
iniread, SavedRoofSection7H, %FileName%.ini, Main, RoofSection7H,%A_Space%
iniread, SavedRoofSection7V, %FileName%.ini, Main, RoofSection7V,%A_Space%
iniread, SavedRoofSection7D, %FileName%.ini, Main, RoofSection7D,%A_Space%
iniread, SavedRoofSection7R, %FileName%.ini, Main, RoofSection7R,%A_Space%
iniread, SavedRoofSection8T, %FileName%.ini, Main, RoofSection8T,%A_Space%
iniread, SavedRoofSection8A, %FileName%.ini, Main, RoofSection8A,%A_Space%
iniread, SavedRoofSection8E, %FileName%.ini, Main, RoofSection8E,%A_Space%
iniread, SavedRoofSection8M, %FileName%.ini, Main, RoofSection8M,%A_Space%
iniread, SavedRoofSection8Z, %FileName%.ini, Main, RoofSection8Z,%A_Space%
iniread, SavedRoofSection8P, %FileName%.ini, Main, RoofSection8P,%A_Space%
iniread, SavedRoofSection8S, %FileName%.ini, Main, RoofSection8S,%A_Space%
iniread, SavedRoofSection8F, %FileName%.ini, Main, RoofSection8F,%A_Space%
iniread, SavedRoofSection8H, %FileName%.ini, Main, RoofSection8H,%A_Space%
iniread, SavedRoofSection8V, %FileName%.ini, Main, RoofSection8V,%A_Space%
iniread, SavedRoofSection8D, %FileName%.ini, Main, RoofSection8D,%A_Space%
iniread, SavedRoofSection8R, %FileName%.ini, Main, RoofSection8R,%A_Space%
iniread, SavedRoofSection9T, %FileName%.ini, Main, RoofSection9T,%A_Space%
iniread, SavedRoofSection9A, %FileName%.ini, Main, RoofSection9A,%A_Space%
iniread, SavedRoofSection9E, %FileName%.ini, Main, RoofSection9E,%A_Space%
iniread, SavedRoofSection9M, %FileName%.ini, Main, RoofSection9M,%A_Space%
iniread, SavedRoofSection9Z, %FileName%.ini, Main, RoofSection9Z,%A_Space%
iniread, SavedRoofSection9P, %FileName%.ini, Main, RoofSection9P,%A_Space%
iniread, SavedRoofSection9S, %FileName%.ini, Main, RoofSection9S,%A_Space%
iniread, SavedRoofSection9F, %FileName%.ini, Main, RoofSection9F,%A_Space%
iniread, SavedRoofSection9H, %FileName%.ini, Main, RoofSection9H,%A_Space%
iniread, SavedRoofSection9V, %FileName%.ini, Main, RoofSection9V,%A_Space%
iniread, SavedRoofSection9D, %FileName%.ini, Main, RoofSection9D,%A_Space%
iniread, SavedRoofSection9R, %FileName%.ini, Main, RoofSection9R,%A_Space%
iniread, SavedRoofSection1Box, %FileName%.ini, Main, RoofSection1Box,1
iniread, SavedRoofSection2Box, %FileName%.ini, Main, RoofSection2Box,1
iniread, SavedRoofSection3Box, %FileName%.ini, Main, RoofSection3Box,1
iniread, SavedRoofSection4Box, %FileName%.ini, Main, RoofSection4Box,1
iniread, SavedRoofSection5Box, %FileName%.ini, Main, RoofSection5Box,1
iniread, SavedRoofSection6Box, %FileName%.ini, Main, RoofSection6Box,1
iniread, SavedRoofSection7Box, %FileName%.ini, Main, RoofSection7Box,1
iniread, SavedRoofSection8Box, %FileName%.ini, Main, RoofSection8Box,1
iniread, SavedRoofSection9Box, %FileName%.ini, Main, RoofSection9Box,1
iniread, SavedDirN, %FileName%.ini, Main, DirN,%A_Space%
iniread, SavedDirE, %FileName%.ini, Main, DirE,%A_Space%
iniread, SavedDirS, %FileName%.ini, Main, DirS,%A_Space%
iniread, SavedDirW, %FileName%.ini, Main, DirW,%A_Space%
iniread, SavedTextBoxRaw, %FileName%.ini, Main, TextBox,%A_Space%
StringReplace, SavedTextBox, SavedTextBoxRaw, $n$, `n, A
iniread, SavedTotalsRaw, %FileName%.ini, Main, TotalsBox,%A_Space%
StringReplace, SavedTotals, SavedTotalsRaw, $n$, `n, A

; ###Toggle for whether to show the taskbar icon or not
if (TaskbarHide = 0)
{Gui, +ToolWindow
}

; ###Set the default font size here###
Gui, Font, s12 c%SavedFontColor%

; ###Buttons across the top###
Gui, add, button, x260 y1 w30 h30 hwndRefresh, `n`n`n`n`nRefresh
GuiButtonIcon(Refresh, "shell32.dll", 239, "s20")
Gui, add, button, xp+33 w30 h30 hwndSave, `n`n`n`n`nSave ;works as intended
GuiButtonIcon(Save, "shell32.dll", 259, "s20")
Gui, add, button, xp+33 w30 h30 hwndClear, `n`n`n`n`nCLEAR
GuiButtonIcon(Clear, "shell32.dll", 132, "s20")
Gui, add, button, xp+33 w30 h30 hwndHide, `n`n`n`n`nHide
GuiButtonIcon(Hide, "shell32.dll", 248, "s20")
Gui, add, button, xp+33 w30 h30 hwndSummary, `n`n`n`n`n`n`n`n`nSummary Export
GuiButtonIcon(Summary, "shell32.dll", 70, "s20")
Gui, add, button, xp+33 w30 h30 hwndCommands, `n`n`n`n`n`n`nCommands
GuiButtonIcon(Commands, "shell32.dll", 22, "s20")
Gui, add, button, xp+33 w30 h30 hwndHow, `n`n`n`n`n?
GuiButtonIcon(How, "shell32.dll", 24, "s20")
Gui, add, button, xp+33 w30 h30 hwndTaskbar, `n`n`n`n`n`nTaskbar
GuiButtonIcon(Taskbar, "shell32.dll", 241, "s20")
Gui, add, button, xp+33 w30 h30 hwndClipboard, `n`n`n`n`n`nClipboard
GuiButtonIcon(Clipboard, "shell32.dll", 74, "s20")

; ###Textbox###
Gui, add, text, x570 y5, Enter your own text below
Gui, add, edit, -E0x200 -TabStop xp-10 yp+20 w300 r18 vTextBox,%SavedTextBox%

; ###Background Picture###
Gui, Add, Picture, x2 y34 w560 h380 BackgroundTrans vIEBGPic, %BGPIC%

; ###Tabs###
Gui, Font, cBlack
Gui, add, Tab2, x0 y5 h410 w540, Clipboard||Totals|Stats|BG
Gui, Font, c%SavedFontColor%

; ###Front tab, the one most commonly used###
Gui, Tab, Clipboard
; ###Compass Directions###
Gui, font, bold
Gui, add, text, x70 y77 BackgroundTrans, E
Gui, font, normal
Gui, add, edit, -E0x200 +center xp+16 yp-1 h22 w40 gNewDirections Number Limit3 vDirE,%SavedDirE%
Gui, font, bold
Gui, add, text, x42 y77 BackgroundTrans, W
Gui, font, normal
Gui, add, text, xp-34 h22 w27  BackgroundTrans vDirW,%SavedDirW%
Gui, font, bold
Gui, add, text, x57 y60 BackgroundTrans, N
Gui, font, normal
Gui, add, text, xp-7 yp-18 h22 w27 BackgroundTrans vDirN,%SavedDirN%
Gui, font, bold
Gui, add, text, x57 y94 BackgroundTrans, S
Gui, font, normal
Gui, add, text, xp-5 yp+20 h22 w27  BackgroundTrans vDirS,%SavedDirS%

; ###Project Info###
Gui, add, text, x140 y40 BackgroundTrans, DI:
Gui, add, edit, -E0x200  +center xp+25 yp-1 h22 w33 Limit3 vIN,%SavedIN%
Gui, add, text, xp+35 yp+1 BackgroundTrans, #:
Gui, add, edit, -E0x200  +center xp+15 yp-1 h22 w80 Number Limit7 vAR,%SavedAR%
Gui, add, text, xp+85 yp+1 BackgroundTrans, Last Name:
Gui, add, edit, -E0x200  +center xp+85 yp-1 h22 w150 vLastName,%SavedLastName%
Gui, add, text, x140 yp+25 BackgroundTrans, Address:
Gui, add, edit, -E0x200  +center xp+65 yp-1 h22 w330 vAddress,%SavedAddress%
Gui, add, edit, -E0x200  +center xp-65 yp+27 h22 w150 vCity,%SavedCity%
; ###State stuff###
; Note that the DefStateFull value is what tells the drop-down list how far down to go before
; using that value as the default.
Gui, Add, DropDownList, -TabStop xp+155 yp-2 w150 R15 Choose%StateFull% AltSubmit gStateInfo vStateFull, Alabama|Alaska|Arizona|Arkansas|California|Colorado|Connecticut|Delaware|D.C.|Florida|Georgia|Hawaii|Idaho|Illinois|Indiana|Iowa|Kansas|Kentucky|Louisiana|Maine|Maryland|Massachusetts|Michigan|Minnesota|Mississippi|Missouri|Montana|Nebraska|Nevada|New Hampshire|New Jersey|New Mexico|New York|North Carolina|North Dakota|Ohio|Oklahoma|Oregon|Pennsylvania|Puerto Rico|Rhode Island|South Carolina|South Dakota|Tennessee|Texas|Utah|Vermont|Virginia|Washington|West Virginia|Wisconsin|Wyoming
;Gui, add, text, xp+165 yp+2 w25 vStateABBox, %StateAB%
Gui, add, edit, -E0x200  +center xp+155 yp+2 h22 w60 Number Limit5 vZip,%SavedZip%

; ###Progress Indicator and Calculate Button
; Its position is supposed to be on the same level as the Goal
; and below the rating/minumum information
Gui, Add, Progress, x140 yp+30 w275 h20 cBlue vProdProg
Gui, add, button, -TabStop xp+280 w110 h22, CALCULATE

; ###Goal and Totals line###
; This line is supposed to be between the Goal
; and the column names, with the totals above
; their respective columns
Gui, font, bold
Gui, add, text, x135 y145 BackgroundTrans, Goal:
Gui, add, edit, -E0x200  +center xp+50 yp-2 h22 w75 Number Limit5 gCalculateTotal vGoalMin, %GoalMin%
Gui, add, text, xp+78 w65 yp+3 BackgroundTrans vGoalTextBLL,
Gui, add, text, xp+2 h22 w65 BackgroundTrans vGoalTextBLR,
Gui, add, text, yp-2 h22 w65 BackgroundTrans vGoalTextBUR,
Gui, add, text, xp-2 h22 w65 BackgroundTrans vGoalTextBUL,
Gui, add, text, xp+1 yp+1 h22 w65 BackgroundTrans vGoalText,
Gui, font, normal
Gui, add, text, xp+140 BackgroundTrans, Usage:
Gui, font, bold
Gui, add, edit, -E0x200  +center xp+60 yp-2 h22 w60 Number Limit5 gCalculateTotal vAnnual, %Annual%
Gui, add, text, x50 yp+22 w60 h22 BackgroundTrans, Totals:
;y position is presently 130
Gui, add, text, xp+120 yp+2 h22 w30 BackgroundTrans vRoofSectionSigmaMBLL,%SavedRoofSectionSigmaM%
Gui, add, text, xp+2 h22 w30 BackgroundTrans vRoofSectionSigmaMBLR,%SavedRoofSectionSigmaM%
Gui, add, text, yp-2 h22 w30 BackgroundTrans vRoofSectionSigmaMBUR,%SavedRoofSectionSigmaM%
Gui, add, text, xp-2 h22 w30 BackgroundTrans vRoofSectionSigmaMBUL,%SavedRoofSectionSigmaM%
Gui, add, text, xp+1 yp+1 h22 w30 BackgroundTrans vRoofSectionSigmaM,%SavedRoofSectionSigmaM%
Gui, add, text, xp+30 +left h22 w55 BackgroundTrans vRoofSectionSigmaP,%SavedRoofSectionSigmaP%
Gui, add, text, xp+60 h22 w55 BackgroundTrans vRoofSectionSigmaZ,%SavedRoofSectionSigmaZ%
Gui, add, text, xp+65 yp+1 h22 w50 BackgroundTrans vRoofSectionSigmaHBLL,%SavedRoofSectionSigmaH%
Gui, add, text, xp+2 h22 w50 +left BackgroundTrans vRoofSectionSigmaHBLR,%SavedRoofSectionSigmaH%
Gui, add, text, yp-2 h22 w50 +left BackgroundTrans vRoofSectionSigmaHBUR,%SavedRoofSectionSigmaH%
Gui, add, text, xp-2 h22 w50 +left BackgroundTrans vRoofSectionSigmaHBUL,%SavedRoofSectionSigmaH%
Gui, add, text, xp+1 yp+1 h22 +left w50 BackgroundTrans vRoofSectionSigmaH,%SavedRoofSectionSigmaH%
; make this next one aligned with the Annual Usage text above
Gui, add, text, xp+80 h22 w60 BackgroundTrans, Offset:
Gui, add, text, xp+60 h22 w50 BackgroundTrans vOffsetP, 0`%
Gui, font, normal

; ###Section Labels###
Gui, add, text, x55 yp+19 BackgroundTrans, Eyes
Gui, add, text, xp+49 BackgroundTrans, Tilt°
Gui, add, text, xp+29 BackgroundTrans, Azim°
Gui, add, text, xp+42 BackgroundTrans, Mods
Gui, add, text, xp+47 BackgroundTrans, Prod.
Gui, add, text, xp+48 BackgroundTrans, (Size)
Gui, add, text, xp+41 BackgroundTrans, (Hours)
Gui, add, text, xp+54 BackgroundTrans, S`%
Gui, add, text, xp+30 BackgroundTrans, AP
Gui, add, text, xp+28 BackgroundTrans, Avg'
Gui, add, text, xp+35 BackgroundTrans, LHD'
Gui, add, text, xp+45 BackgroundTrans, Ra"

; ###Roof Section 1 information###
;Gui, add, text, x5 y205, S1
Gui, add, text, x5 yp+19 BackgroundTrans, S1
Gui, Add, Checkbox, xp+25 yp+4 w13 h13 -TabStop Checked%SavedRoofSection1Box% vRoofSection1Box gAddSection1,%A_Space%
Gui, add, edit, -E0x200  +center xp+18 yp-3 h22 w50 -TabStop vRoofSection1E,%SavedRoofSection1E%
Gui, add, edit, -E0x200  +center xp+54 h22 w25 Number Limit2 vRoofSection1T,%SavedRoofSection1T%
Gui, add, edit, -E0x200  +center xp+26 h22 w40 Number Limit3 vRoofSection1A,%SavedRoofSection1A%
Gui, add, edit, -E0x200  +center xp+44 h22 Number Limit3 w30 gCalculateSection1 vRoofSection1M,%SavedRoofSection1M%
Gui, add, edit, -E0x200  +center xp+31 h22 w65 limit7 gCalculateSection1 vRoofSection1P,%SavedRoofSection1P%
Gui, add, text, xp+70 yp+3 h16 w60 BackgroundTrans vRoofSection1Z,%SavedRoofSection1Z%
Gui, add, text, xp+45 yp+1 h16 w40 BackgroundTrans vRoofSection1HBLL,%SavedRoofSection1H%
Gui, add, text, xp+2 h16 w40 BackgroundTrans vRoofSection1HBLR,%SavedRoofSection1H%
Gui, add, text, yp-2 h16 w40 BackgroundTrans vRoofSection1HBUR,%SavedRoofSection1H%
Gui, add, text, xp-2 h16 w40 BackgroundTrans vRoofSection1HBUL,%SavedRoofSection1H%
Gui, add, text, xp+1 yp+1 h16 w40 BackgroundTrans vRoofSection1H,%SavedRoofSection1H%
Gui, add, edit, -E0x200  +center xp+43 yp-3 h22 w25 Number Limit3 vRoofSection1S,%SavedRoofSection1S%
Gui, add, edit, -E0x200  +center xp+26 h22 w30 -TabStop Number Limit3 vRoofSection1F,%SavedRoofSection1F%
Gui, add, edit, -E0x200  +center xp+31 h22 w30 -TabStop Number Limit2 vRoofSection1V,%SavedRoofSection1V%
Gui, add, edit, -E0x200  +center xp+31 h22 w50 -TabStop limit5 vRoofSection1D,%SavedRoofSection1D%
Gui, add, edit, -E0x200  +center xp+51 h22 w30 -TabStop Number Limit2 vRoofSection1R,%SavedRoofSection1R%

; ###Roof Section 2 information###
Gui, add, text, x5 yp+22 BackgroundTrans, S2
Gui, Add, Checkbox, xp+25 yp+4 w13 h13 -TabStop Checked%SavedRoofSection2Box% vRoofSection2Box gAddSection2,%A_Space%
Gui, add, edit, -E0x200  +center xp+18 yp-3 h22 w50 -TabStop vRoofSection2E,%SavedRoofSection2E%
Gui, add, edit, -E0x200  +center xp+54 h22 w25 Number Limit2 vRoofSection2T,%SavedRoofSection2T%
Gui, add, edit, -E0x200  +center xp+26 h22 w40 Number Limit3 vRoofSection2A,%SavedRoofSection2A%
Gui, add, edit, -E0x200  +center xp+44 h22 Number Limit3 w30 gCalculateSection2 vRoofSection2M,%SavedRoofSection2M%
Gui, add, edit, -E0x200  +center xp+31 h22 w65 limit7 gCalculateSection2 vRoofSection2P,%SavedRoofSection2P%
Gui, add, text, xp+70 yp+3 h16 w60 BackgroundTrans vRoofSection2Z,%SavedRoofSection2Z%
Gui, add, text, xp+45 yp+1 h16 w40 BackgroundTrans vRoofSection2HBLL,%SavedRoofSection2H%
Gui, add, text, xp+2 h16 w40 BackgroundTrans vRoofSection2HBLR,%SavedRoofSection2H%
Gui, add, text, yp-2 h16 w40 BackgroundTrans vRoofSection2HBUR,%SavedRoofSection2H%
Gui, add, text, xp-2 h16 w40 BackgroundTrans vRoofSection2HBUL,%SavedRoofSection2H%
Gui, add, text, xp+1 yp+1 h16 w40 BackgroundTrans vRoofSection2H,%SavedRoofSection2H%
Gui, add, edit, -E0x200  +center xp+43 yp-3 h22 w25 Number Limit3 vRoofSection2S,%SavedRoofSection2S%
Gui, add, edit, -E0x200  +center xp+26 h22 w30 -TabStop Number Limit3 vRoofSection2F,%SavedRoofSection2F%
Gui, add, edit, -E0x200  +center xp+31 h22 w30 -TabStop Number Limit2 vRoofSection2V,%SavedRoofSection2V%
Gui, add, edit, -E0x200  +center xp+31 h22 w50 -TabStop limit5 vRoofSection2D,%SavedRoofSection2D%
Gui, add, edit, -E0x200  +center xp+51 h22 w30 -TabStop Number Limit2 vRoofSection2R,%SavedRoofSection2R%

; ###Roof Section 3 information###
Gui, add, text, x5 yp+22 BackgroundTrans, S3
Gui, Add, Checkbox, xp+25 yp+4 w13 h13 -TabStop Checked%SavedRoofSection3Box% vRoofSection3Box gAddSection3,%A_Space%
Gui, add, edit, -E0x200  +center xp+18 yp-3 h22 w50 -TabStop vRoofSection3E,%SavedRoofSection3E%
Gui, add, edit, -E0x200  +center xp+54 h22 w25 Number Limit2 vRoofSection3T,%SavedRoofSection3T%
Gui, add, edit, -E0x200  +center xp+26 h22 w40 Number Limit3 vRoofSection3A,%SavedRoofSection3A%
Gui, add, edit, -E0x200  +center xp+44 h22 Number Limit3 w30 gCalculateSection3 vRoofSection3M,%SavedRoofSection3M%
Gui, add, edit, -E0x200  +center xp+31 h22 w65 limit7 gCalculateSection3 vRoofSection3P,%SavedRoofSection3P%
Gui, add, text, xp+70 yp+3 h16 w60 BackgroundTrans vRoofSection3Z,%SavedRoofSection3Z%
Gui, add, text, xp+45 yp+1 h16 w40 BackgroundTrans vRoofSection3HBLL,%SavedRoofSection3H%
Gui, add, text, xp+2 h16 w40 BackgroundTrans vRoofSection3HBLR,%SavedRoofSection3H%
Gui, add, text, yp-2 h16 w40 BackgroundTrans vRoofSection3HBUR,%SavedRoofSection3H%
Gui, add, text, xp-2 h16 w40 BackgroundTrans vRoofSection3HBUL,%SavedRoofSection3H%
Gui, add, text, xp+1 yp+1 h16 w40 BackgroundTrans vRoofSection3H,%SavedRoofSection3H%
Gui, add, edit, -E0x200  +center xp+43 yp-3 h22 w25 Number Limit3 vRoofSection3S,%SavedRoofSection3S%
Gui, add, edit, -E0x200  +center xp+26 h22 w30 -TabStop Number Limit3 vRoofSection3F,%SavedRoofSection3F%
Gui, add, edit, -E0x200  +center xp+31 h22 w30 -TabStop Number Limit2 vRoofSection3V,%SavedRoofSection3V%
Gui, add, edit, -E0x200  +center xp+31 h22 w50 -TabStop limit5 vRoofSection3D,%SavedRoofSection3D%
Gui, add, edit, -E0x200  +center xp+51 h22 w30 -TabStop Number Limit2 vRoofSection3R,%SavedRoofSection3R%

; ###Roof Section 4 information###
Gui, add, text, x5 yp+22 BackgroundTrans, S4
Gui, Add, Checkbox, xp+25 yp+4 w13 h13 -TabStop Checked%SavedRoofSection4Box% vRoofSection4Box gAddSection4,%A_Space%
Gui, add, edit, -E0x200  +center xp+18 yp-3 h22 w50 -TabStop vRoofSection4E,%SavedRoofSection4E%
Gui, add, edit, -E0x200  +center xp+54 h22 w25 Number Limit2 vRoofSection4T,%SavedRoofSection4T%
Gui, add, edit, -E0x200  +center xp+26 h22 w40 Number Limit3 vRoofSection4A,%SavedRoofSection4A%
Gui, add, edit, -E0x200  +center xp+44 h22 Number Limit3 w30 gCalculateSection4 vRoofSection4M,%SavedRoofSection4M%
Gui, add, edit, -E0x200  +center xp+31 h22 w65 limit7 gCalculateSection4 vRoofSection4P,%SavedRoofSection4P%
Gui, add, text, xp+70 yp+3 h16 w60 BackgroundTrans vRoofSection4Z,%SavedRoofSection4Z%
Gui, add, text, xp+45 yp+1 h16 w40 BackgroundTrans vRoofSection4HBLL,%SavedRoofSection4H%
Gui, add, text, xp+2 h16 w40 BackgroundTrans vRoofSection4HBLR,%SavedRoofSection4H%
Gui, add, text, yp-2 h16 w40 BackgroundTrans vRoofSection4HBUR,%SavedRoofSection4H%
Gui, add, text, xp-2 h16 w40 BackgroundTrans vRoofSection4HBUL,%SavedRoofSection4H%
Gui, add, text, xp+1 yp+1 h16 w40 BackgroundTrans vRoofSection4H,%SavedRoofSection4H%
Gui, add, edit, -E0x200  +center xp+43 yp-3 h22 w25 Number Limit3 vRoofSection4S,%SavedRoofSection4S%
Gui, add, edit, -E0x200  +center xp+26 h22 w30 -TabStop Number Limit3 vRoofSection4F,%SavedRoofSection4F%
Gui, add, edit, -E0x200  +center xp+31 h22 w30 -TabStop Number Limit2 vRoofSection4V,%SavedRoofSection4V%
Gui, add, edit, -E0x200  +center xp+31 h22 w50 -TabStop limit5 vRoofSection4D,%SavedRoofSection4D%
Gui, add, edit, -E0x200  +center xp+51 h22 w30 -TabStop Number Limit2 vRoofSection4R,%SavedRoofSection4R%

; ###Roof Section 5 information###
Gui, add, text, x5 yp+22 BackgroundTrans, S5
Gui, Add, Checkbox, xp+25 yp+4 w13 h13 -TabStop Checked%SavedRoofSection5Box% vRoofSection5Box gAddSection5,%A_Space%
Gui, add, edit, -E0x200  +center xp+18 yp-3 h22 w50 -TabStop vRoofSection5E,%SavedRoofSection5E%
Gui, add, edit, -E0x200  +center xp+54 h22 w25 Number Limit2 vRoofSection5T,%SavedRoofSection5T%
Gui, add, edit, -E0x200  +center xp+26 h22 w40 Number Limit3 vRoofSection5A,%SavedRoofSection5A%
Gui, add, edit, -E0x200  +center xp+44 h22 Number Limit3 w30 gCalculateSection5 vRoofSection5M,%SavedRoofSection5M%
Gui, add, edit, -E0x200  +center xp+31 h22 w65 limit7 gCalculateSection5 vRoofSection5P,%SavedRoofSection5P%
Gui, add, text, xp+70 yp+3 h16 w60 BackgroundTrans vRoofSection5Z,%SavedRoofSection5Z%
Gui, add, text, xp+45 yp+1 h16 w40 BackgroundTrans vRoofSection5HBLL,%SavedRoofSection5H%
Gui, add, text, xp+2 h16 w40 BackgroundTrans vRoofSection5HBLR,%SavedRoofSection5H%
Gui, add, text, yp-2 h16 w40 BackgroundTrans vRoofSection5HBUR,%SavedRoofSection5H%
Gui, add, text, xp-2 h16 w40 BackgroundTrans vRoofSection5HBUL,%SavedRoofSection5H%
Gui, add, text, xp+1 yp+1 h16 w40 BackgroundTrans vRoofSection5H,%SavedRoofSection5H%
Gui, add, edit, -E0x200  +center xp+43 yp-3 h22 w25 Number Limit3 vRoofSection5S,%SavedRoofSection5S%
Gui, add, edit, -E0x200  +center xp+26 h22 w30 -TabStop Number Limit3 vRoofSection5F,%SavedRoofSection5F%
Gui, add, edit, -E0x200  +center xp+31 h22 w30 -TabStop Number Limit2 vRoofSection5V,%SavedRoofSection5V%
Gui, add, edit, -E0x200  +center xp+31 h22 w50 -TabStop limit5 vRoofSection5D,%SavedRoofSection5D%
Gui, add, edit, -E0x200  +center xp+51 h22 w30 -TabStop Number Limit2 vRoofSection5R,%SavedRoofSection5R%

; ###Roof Section 6 information###
Gui, add, text, x5 yp+22 BackgroundTrans, S6
Gui, Add, Checkbox, xp+25 yp+4 w13 h13 -TabStop Checked%SavedRoofSection6Box% vRoofSection6Box gAddSection6,%A_Space%
Gui, add, edit, -E0x200  +center xp+18 yp-3 h22 w50 -TabStop vRoofSection6E,%SavedRoofSection6E%
Gui, add, edit, -E0x200  +center xp+54 h22 w25 Number Limit2 vRoofSection6T,%SavedRoofSection6T%
Gui, add, edit, -E0x200  +center xp+26 h22 w40 Number Limit3 vRoofSection6A,%SavedRoofSection6A%
Gui, add, edit, -E0x200  +center xp+44 h22 Number Limit3 w30 gCalculateSection6 vRoofSection6M,%SavedRoofSection6M%
Gui, add, edit, -E0x200  +center xp+31 h22 w65 limit7 gCalculateSection6 vRoofSection6P,%SavedRoofSection6P%
Gui, add, text, xp+70 yp+3 h16 w60 BackgroundTrans vRoofSection6Z,%SavedRoofSection6Z%
Gui, add, text, xp+45 yp+1 h16 w40 BackgroundTrans vRoofSection6HBLL,%SavedRoofSection6H%
Gui, add, text, xp+2 h16 w40 BackgroundTrans vRoofSection6HBLR,%SavedRoofSection6H%
Gui, add, text, yp-2 h16 w40 BackgroundTrans vRoofSection6HBUR,%SavedRoofSection6H%
Gui, add, text, xp-2 h16 w40 BackgroundTrans vRoofSection6HBUL,%SavedRoofSection6H%
Gui, add, text, xp+1 yp+1 h16 w40 BackgroundTrans vRoofSection6H,%SavedRoofSection6H%
Gui, add, edit, -E0x200  +center xp+43 yp-3 h22 w25 Number Limit3 vRoofSection6S,%SavedRoofSection6S%
Gui, add, edit, -E0x200  +center xp+26 h22 w30 -TabStop Number Limit3 vRoofSection6F,%SavedRoofSection6F%
Gui, add, edit, -E0x200  +center xp+31 h22 w30 -TabStop Number Limit2 vRoofSection6V,%SavedRoofSection6V%
Gui, add, edit, -E0x200  +center xp+31 h22 w50 -TabStop limit5 vRoofSection6D,%SavedRoofSection6D%
Gui, add, edit, -E0x200  +center xp+51 h22 w30 -TabStop Number Limit2 vRoofSection6R,%SavedRoofSection6R%

; ###Roof Section 7 information###
Gui, add, text, x5 yp+22 BackgroundTrans, S7
Gui, Add, Checkbox, xp+25 yp+4 w13 h13 -TabStop Checked%SavedRoofSection7Box% vRoofSection7Box gAddSection7,%A_Space%
Gui, add, edit, -E0x200  +center xp+18 yp-3 h22 w50 -TabStop vRoofSection7E,%SavedRoofSection7E%
Gui, add, edit, -E0x200  +center xp+54 h22 w25 Number Limit2 vRoofSection7T,%SavedRoofSection7T%
Gui, add, edit, -E0x200  +center xp+26 h22 w40 Number Limit3 vRoofSection7A,%SavedRoofSection7A%
Gui, add, edit, -E0x200  +center xp+44 h22 Number Limit3 w30 gCalculateSection7 vRoofSection7M,%SavedRoofSection7M%
Gui, add, edit, -E0x200  +center xp+31 h22 w65 limit7 gCalculateSection7 vRoofSection7P,%SavedRoofSection7P%
Gui, add, text, xp+70 yp+3 h16 w60 BackgroundTrans vRoofSection7Z,%SavedRoofSection7Z%
Gui, add, text, xp+45 yp+1 h16 w40 BackgroundTrans vRoofSection7HBLL,%SavedRoofSection7H%
Gui, add, text, xp+2 h16 w40 BackgroundTrans vRoofSection7HBLR,%SavedRoofSection7H%
Gui, add, text, yp-2 h16 w40 BackgroundTrans vRoofSection7HBUR,%SavedRoofSection7H%
Gui, add, text, xp-2 h16 w40 BackgroundTrans vRoofSection7HBUL,%SavedRoofSection7H%
Gui, add, text, xp+1 yp+1 h16 w40 BackgroundTrans vRoofSection7H,%SavedRoofSection7H%
Gui, add, edit, -E0x200  +center xp+43 yp-3 h22 w25 Number Limit3 vRoofSection7S,%SavedRoofSection7S%
Gui, add, edit, -E0x200  +center xp+26 h22 w30 -TabStop Number Limit3 vRoofSection7F,%SavedRoofSection7F%
Gui, add, edit, -E0x200  +center xp+31 h22 w30 -TabStop Number Limit2 vRoofSection7V,%SavedRoofSection7V%
Gui, add, edit, -E0x200  +center xp+31 h22 w50 -TabStop limit5 vRoofSection7D,%SavedRoofSection7D%
Gui, add, edit, -E0x200  +center xp+51 h22 w30 -TabStop Number Limit2 vRoofSection7R,%SavedRoofSection7R%

; ###Roof Section 8 information###
Gui, add, text, x5 yp+22 BackgroundTrans, S8
Gui, Add, Checkbox, xp+25 yp+4 w13 h13 -TabStop Checked%SavedRoofSection8Box% vRoofSection8Box gAddSection8,%A_Space%
Gui, add, edit, -E0x200  +center xp+18 yp-3 h22 w50 -TabStop vRoofSection8E,%SavedRoofSection8E%
Gui, add, edit, -E0x200  +center xp+54 h22 w25 Number Limit2 vRoofSection8T,%SavedRoofSection8T%
Gui, add, edit, -E0x200  +center xp+26 h22 w40 Number Limit3 vRoofSection8A,%SavedRoofSection8A%
Gui, add, edit, -E0x200  +center xp+44 h22 Number Limit3 w30 gCalculateSection8 vRoofSection8M,%SavedRoofSection8M%
Gui, add, edit, -E0x200  +center xp+31 h22 w65 limit7 gCalculateSection8 vRoofSection8P,%SavedRoofSection8P%
Gui, add, text, xp+70 yp+3 h16 w60 BackgroundTrans vRoofSection8Z,%SavedRoofSection8Z%
Gui, add, text, xp+45 yp+1 h16 w40 BackgroundTrans vRoofSection8HBLL,%SavedRoofSection8H%
Gui, add, text, xp+2 h16 w40 BackgroundTrans vRoofSection8HBLR,%SavedRoofSection8H%
Gui, add, text, yp-2 h16 w40 BackgroundTrans vRoofSection8HBUR,%SavedRoofSection8H%
Gui, add, text, xp-2 h16 w40 BackgroundTrans vRoofSection8HBUL,%SavedRoofSection8H%
Gui, add, text, xp+1 yp+1 h16 w40 BackgroundTrans vRoofSection8H,%SavedRoofSection8H%
Gui, add, edit, -E0x200  +center xp+43 yp-3 h22 w25 Number Limit3 vRoofSection8S,%SavedRoofSection8S%
Gui, add, edit, -E0x200  +center xp+26 h22 w30 -TabStop Number Limit3 vRoofSection8F,%SavedRoofSection8F%
Gui, add, edit, -E0x200  +center xp+31 h22 w30 -TabStop Number Limit2 vRoofSection8V,%SavedRoofSection8V%
Gui, add, edit, -E0x200  +center xp+31 h22 w50 -TabStop limit5 vRoofSection8D,%SavedRoofSection8D%
Gui, add, edit, -E0x200  +center xp+51 h22 w30 -TabStop Number Limit2 vRoofSection8R,%SavedRoofSection8R%

; ###Roof Section 9 information###
Gui, add, text, x5 yp+22 BackgroundTrans, S9
Gui, Add, Checkbox, xp+25 yp+4 w13 h13 -TabStop Checked%SavedRoofSection9Box% vRoofSection9Box gAddSection9,%A_Space%
Gui, add, edit, -E0x200  +center xp+18 yp-3 h22 w50 -TabStop vRoofSection9E,%SavedRoofSection9E%
Gui, add, edit, -E0x200  +center xp+54 h22 w25 Number Limit2 vRoofSection9T,%SavedRoofSection9T%
Gui, add, edit, -E0x200  +center xp+26 h22 w40 Number Limit3 vRoofSection9A,%SavedRoofSection9A%
Gui, add, edit, -E0x200  +center xp+44 h22 Number Limit3 w30 gCalculateSection9 vRoofSection9M,%SavedRoofSection9M%
Gui, add, edit, -E0x200  +center xp+31 h22 w65 limit7 gCalculateSection9 vRoofSection9P,%SavedRoofSection9P%
Gui, add, text, xp+70 yp+3 h16 w60 BackgroundTrans vRoofSection9Z,%SavedRoofSection9Z%
Gui, add, text, xp+45 yp+1 h16 w40 BackgroundTrans vRoofSection9HBLL,%SavedRoofSection9H%
Gui, add, text, xp+2 h16 w40 BackgroundTrans vRoofSection9HBLR,%SavedRoofSection9H%
Gui, add, text, yp-2 h16 w40 BackgroundTrans vRoofSection9HBUR,%SavedRoofSection9H%
Gui, add, text, xp-2 h16 w40 BackgroundTrans vRoofSection9HBUL,%SavedRoofSection9H%
Gui, add, text, xp+1 yp+1 h16 w40 BackgroundTrans vRoofSection9H,%SavedRoofSection9H%
Gui, add, edit, -E0x200  +center xp+43 yp-3 h22 w25 Number Limit3 vRoofSection9S,%SavedRoofSection9S%
Gui, add, edit, -E0x200  +center xp+26 h22 w30 -TabStop Number Limit3 vRoofSection9F,%SavedRoofSection9F%
Gui, add, edit, -E0x200  +center xp+31 h22 w30 -TabStop Number Limit2 vRoofSection9V,%SavedRoofSection9V%
Gui, add, edit, -E0x200  +center xp+31 h22 w50 -TabStop limit5 vRoofSection9D,%SavedRoofSection9D%
Gui, add, edit, -E0x200  +center xp+51 h22 w30 -TabStop Number Limit2 vRoofSection9R,%SavedRoofSection9R%
; ###End of the first tab###

Gui, Tab, Stats
; ###Rating and Miniumum stuff###
Gui, add, text, x20 y40 h22 BackgroundTrans, Down Number:
Gui, add, edit, -E0x200  +center -TabStop xp+110 yp-1 h22 w30 Limit2 gMPMP vDownNumber,%DownNumber%
Gui, add, text, xp+40 h22 BackgroundTrans, (change this number for your module type)
Gui, add, text, x20 yp+25 h22 BackgroundTrans, Module Rating (Watts):
Gui, add, edit, -E0x200  +center -TabStop xp+170 yp-1 h22 w50 Limit5 gMPMP vRating,%Rating%
Gui, add, text, x20 yp+25 h22 BackgroundTrans, Module Efficiency`%:
Gui, add, edit, -E0x200  +center -TabStop xp+150 yp-1 h22 w40 Limit4 gMPMP vEfficiency, %Efficiency%
Gui, add, text, x20 yp+25 h22 BackgroundTrans, Per-Module Minimum Production:
Gui, font, bold
Gui, add, text, xp+235 h18 w100 BackgroundTrans vPerModProMin, 0W
Gui, font, normal
Gui, add, text, x20 yp+25 h22 BackgroundTrans, Section Minimum:
Gui, add, edit, -E0x200  +center -TabStop xp+150 yp-1 h22 w40 Number Limit4 vSectionMinimum,%SectionMinimum%
Gui, add, text, x20 yp+25 h22 BackgroundTrans, System Minimum:
Gui, add, edit, -E0x200  +center -TabStop xp+150 yp-1 h22 w40 Number Limit4 vSystemMinimum,%SystemMinimum%

Gui, Tab, BG
Gui, Font, cBlack
; ###COLOR PICKER###
; This lets the user choose their own background
; color for eyestrain prevention, etc.
; Transplanted from Lego_coder / Miguel Agullo's submission
; from www.autohotkey.com/board/topic/6233-quick-and-dirty-color-picker/
Gui, Add, text, x30 y40 BackgroundTrans, Adapted from Lego_coder's`nquick-and-dirty color picker from the Autohotkey forums:`n www.autohotkey.com/board/topic/6233-quick-and-dirty-color-picker/
Gui, add, text, xp+2 BackgroundTrans,Adapted from Lego_coder's`nquick-and-dirty color picker from the Autohotkey forums:`n www.autohotkey.com/board/topic/6233-quick-and-dirty-color-picker/ ;
Gui, add, text, yp-2 BackgroundTrans,Adapted from Lego_coder's`nquick-and-dirty color picker from the Autohotkey forums:`n www.autohotkey.com/board/topic/6233-quick-and-dirty-color-picker/ ;
Gui, add, text, xp-2 BackgroundTrans,Adapted from Lego_coder's`nquick-and-dirty color picker from the Autohotkey forums:`n www.autohotkey.com/board/topic/6233-quick-and-dirty-color-picker/ ;
Gui, Font, cWhite
Gui, add, text, xp+1 yp+1 BackgroundTrans,Adapted from Lego_coder's`nquick-and-dirty color picker from the Autohotkey forums:`n www.autohotkey.com/board/topic/6233-quick-and-dirty-color-picker/ ;
Gui, Font, c%SavedFontColor%
Gui, Add, Text, x20 y115 BackgroundTrans, Red
Gui, add, edit, -E0x200  +center xp+50 yp-3 w50 Number Limit3 +right vRed_Value gChange_Red_Value,%SavedValueR%
Gui, Add, UpDown, Range0-255 Wrap, %SavedValueR%
Gui, Add, Slider, xp-45 yp+30 w110 Range0-255 vRed_Slider gChange_Red_Slider AltSubmit, %SavedValueR%
Gui, Add, Text, x20 yp+40 BackgroundTrans, Green
Gui, add, edit, -E0x200  +center xp+50 yp-3 w50 Number Limit3  +right vGreen_Value gChange_Green_Value, %SavedValueG%
Gui, Add, UpDown, Range0-255 Wrap, %SavedValueG%
Gui, Add, Slider, xp-45 yp+30 w110 BackgroundTrans Range0-255 vGreen_Slider gChange_Green_Slider AltSubmit, %SavedValueG%
Gui, Add, Text, x20 yp+40 BackgroundTrans, Blue
Gui, add, edit, -E0x200  +center xp+50 yp-3 w50 Number Limit3  +right vBlue_Value gChange_Blue_Value, %SavedValueB%
Gui, Add, UpDown, Range0-255 Wrap, %SavedValueB%
Gui, Add, Slider, xp-45 yp+30 w110 Range0-255 vBlue_Slider gChange_Blue_Slider AltSubmit, %SavedValueB%
Gui, Add, Text, x20 yp+40 BackgroundTrans, B/W
Gui, add, edit, -E0x200  +center xp+50 yp-3 w50 Number Limit3  +right vGrey_Value gChange_Grey_Value, %SavedValueGR%
Gui, Add, UpDown, Range0-255 Wrap, %SavedValueGR%
Gui, Add, Slider, xp-45 yp+30 w110 Range0-255 vGrey_Slider gChange_Grey_Slider AltSubmit, %SavedValueGR%
Gui, Add, ListView, x140 y140 h120 w120 ReadOnly 0x4000 +Background000000 VColor_Block
Gui, Add, text, xp+35 yp+120 +right BackgroundTrans, Preview
Gui, Add, text, xp-35 yp+30 +right BackgroundTrans, Hex:
Gui, add, edit, -E0x200  +center xp+35 yp-5 w75 ReadOnly +right VColor_Value, %SavedBackColor%
Gui, Add, Button, xp-15 yp+50 w55 +center gColorSet, Set as background
Gui, Add, Button, xp+110 w55 +center gFontSet, Set as font
Gui, Add, Button, xp+65 +center gColorDefault, Defaults
Gui, Add, text, x140 y100 BackgroundTrans vBGText, The current BG picture is:`n"%BGPICText%".
Gui, Add, Button, gBGBrowse x360 y100 w90 h20, Choose BG
Gui, Add, text, x270 y150 BackgroundTrans, Move the sliders/press the number`narrows to change colors as desired.`nUse the Save button to preserve the`nselection. Keep in mind it will default`nto grey if it cannot find the .ini file`nused elsewhere in this script.
; ###End Miguel's color picker code for now###

; ###The monthly-totals converting tab###
; This is where the user pastes the information
; to be converted from a column of twelve numbers
; into a string of numbers with a Tab between each.
Gui, Tab, Totals
Gui, Add, text, x150 y55 BackgroundTrans, Using Ctrl-Alt-Click, highlight the total monthly`nproductions in the Solmetric Report and paste`nthem in the left box. A preview will appear below.`nUse Ctrl-4 to paste it into the Energy Production`npart of the Salesforce CAD creation phase.
Gui, add, edit, -E0x200  xp-120 yp-0 w100 h255 gMonthTotal vMonthlyTotalRaw, %SavedTotals%
Gui, add, edit, -E0x200  xp+110 yp+110 w330 h70 ReadOnly vMonthlyTotalFormatted,%MonthlyTotal%
; ###End of the Totals tab##

if (BoardHide = 0)
{StartWidth = 880
}
else {
StartWidth = 560
}
if (winx = "")
{Gui, Show, x200 y200 w%StartWidth%, InfoEntry %Version% (Ctrl-Space to show/hide)
}
else {
Gui, Show, x%winX% y%winY% w%StartWidth%, InfoEntry %Version% (Ctrl-Space to show/hide)
}
Gui, Color, 808080, %SavedBackColor%,
Menu, tray, NoStandard
Menu, tray, add, About InfoEntry, AboutThis
Menu, tray, add, Show/Hide, ButtonHide
Menu, tray, add, Exit, guiclose
return

; ###COMMANDS###
; DO NOT CHANGE THESE!
; These are the different routine calculations and
; processes that the tool performs in the background.

ColorDefault:
GuiControl, Text, Red_Value, 128
GuiControl, Text, Red_Slider, 128
GuiControl, Text, Green_Value, 128
GuiControl, Text, Green_Slider, 128
GuiControl, Text, Blue_Value, 128
GuiControl, Text, Blue_Slider, 128
GuiControl, Text, Grey_Slider, 128
GuiControl, Text, Grey_Value, 128
SavedBackColor = A0A0A0
SavedFontColor = 000000
Gosub Show_New_Color
Gui, Color, 808080, A0A0A0
return

; ###Set the background Color###
ColorSet:
SavedBackColor = %New_Color_Value%
Gui, Color, 808080, %New_Color_Value%
return

; ###Set the font Color###
FontSet:
SavedFontColor = %New_Color_Value%
Tooltip, Save and refresh`nfor changes to occur.
SetTimer, RemoveToolTip, 1500
return

; ###Resume Miguel's color-picking stuff###
Change_Red_Slider:
GuiControlGet, Red_Slider
GuiControl, Text, Red_Value, %Red_Slider%
gosub Show_New_Color
Return

Change_Red_Value:
GuiControlGet, Red_Value
GuiControl, Text, Red_Slider, %Red_Value%
Gosub Show_New_Color
Return

Change_Green_Slider:
GuiControlGet, Green_Slider
GuiControl, Text, Green_Value, %Green_Slider%
Gosub Show_New_Color
Return

Change_Green_Value:
GuiControlGet, Green_Value
GuiControl, Text, Green_Slider, %Green_Value%
Gosub Show_New_Color
Return

Change_Blue_Slider:
GuiControlGet, Blue_Slider
GuiControl, Text, Blue_Value, %Blue_Slider%
Gosub Show_New_Color
Return 

Change_Blue_Value:
GuiControlGet, Blue_Value
GuiControl, Text, Blue_Slider, %Blue_Value%
Gosub Show_New_Color
Return

Change_Grey_Slider:
GuiControlGet, Grey_Slider
GuiControl, Text, Red_Value, %Grey_Slider%
GuiControl, Text, Green_Value, %Grey_Slider%
GuiControl, Text, Blue_Value, %Grey_Slider%
GuiControl, Text, Grey_Value, %Grey_Slider%
Gosub Show_New_Color
Return 

Change_Grey_Value:
GuiControlGet, Grey_Value
GuiControl, Text, Red_Slider, %Grey_Value%
GuiControl, Text, Green_Slider, %Grey_Value%
GuiControl, Text, Blue_Slider, %Grey_Value%
GuiControl, Text, Grey_Slider, %Grey_Value%
Gosub Show_New_Color
Return

Show_New_Color:
Gui submit, nohide
SetFormat, integer, hex
Red_Value += 0
Green_Value += 0
Blue_Value += 0
SetFormat, integer, d
Stringright,Red_Value,Red_Value,StrLen(Red_Value)-2
If (StrLen(Red_Value)=1)
Red_Value=0%Red_Value%
Stringright,Green_Value,Green_Value,StrLen(Green_Value)-2
If (StrLen(Green_Value)=1)
Green_Value=0%Green_Value%
Stringright,Blue_Value,Blue_Value,StrLen(Blue_Value)-2
If (StrLen(Blue_Value)=1)
Blue_Value=0%Blue_Value%
New_Color_Value=%Red_Value%%Green_Value%%Blue_Value%
GuiControl, Text, Color_Value, %New_Color_Value%
GuiControl, +Background%New_Color_Value%, Color_Block
Return
; ###End Miguel's color-picking code###

; ###Background Browser Button###
BGBrowse:
FileSelectFile, BGPIC, 3, , Choose an image file, Images (*.bmp; *.jpg; *.png)
SplitPath, BGPIC, BGPICText
Gui, Submit, nohide
GuiControl,,BGText, The current BG picture is:`n"%BGPICText%"
GuiControl,,IEBGPic, *w560 *h380 %BGPIC%
Return


; ###Calculate the various directions###
NewDirections:
Gui, Submit, nohide
NewDirS := DirE + 90
NewDirW := DirE + 180
NewDirN := DirE + 270
; ###(if North is greater than 360, roll it back around)###
If NewDirN >= 360
{NewDirN := NewDirN - 360
}
GuiControl,, DirW, %NewDirW%
GuiControl,, DirN, %NewDirN%
GuiControl,, DirS, %NewDirS%
return

; ###Substitute the state data from the drop-down list###
; ADAPTATION_NOTE
; When changing this tool for use in a different office,
; the number corresponding to each State may be used
; above in the DefStateFull entry
StateInfo:
Gui, Submit, Nohide
if StateFull = 1
	{
	GuiControl,, StateABBox,AL
	StateAB = AL
	State =Alabama
	}
if StateFull = 2
	{
	GuiControl,, StateABBox,AK
	StateAB = AK
	State =Alaska
	}
if StateFull = 3
	{
	GuiControl,, StateABBox,AZ	
	StateAB = AZ
	State =Arizona
	}
if StateFull = 4
	{
	GuiControl,, StateABBox,AR	
	StateAB = AR
	State =Arkansas
	}
if StateFull = 5
	{
	GuiControl,, StateABBox,CA	
	StateAB = CA
	State =California
	}
if StateFull = 6
	{
	GuiControl,, StateABBox,CO	
	StateAB = CO
	State =Colorado
	}
if StateFull = 7
	{
	GuiControl,, StateABBox,CT	
	StateAB = CT
	State =Connecticut
	}
if StateFull = 8
	{
	GuiControl,, StateABBox,DE	
	StateAB = DE
	State =Delaware
	}
if StateFull = 9
	{
	GuiControl,, StateABBox,DC
	StateAB = DC
	State =DC
	}
if StateFull = 10
	{
	GuiControl,, StateABBox,FL	
	StateAB = FL
	State =Florida
	}
if StateFull = 11
	{
	GuiControl,, StateABBox,GA	
	StateAB = GA
	State =Georgia
	}
if StateFull = 12
	{
	GuiControl,, StateABBox,HI	
	StateAB = HI
	State =Hawaii
	}
if StateFull = 13
	{
	GuiControl,, StateABBox,ID	
	StateAB = ID
	State =Idaho
	}
if StateFull = 14
	{
	GuiControl,, StateABBox,IL	
	StateAB = IL
	State =Illinois
	}
if StateFull = 15
	{
	GuiControl,, StateABBox,IN	
	StateAB = IN
	State =Indiana
	}
if StateFull = 16
	{
	GuiControl,, StateABBox,IA	
	StateAB = IA
	State =Iowa
	}
if StateFull = 17
	{
	GuiControl,, StateABBox,KS	
	StateAB = KS
	State =Kansas
	}
if StateFull = 18
	{
	GuiControl,, StateABBox,KY	
	StateAB = KY
	State =Kentucky
	}
if StateFull = 19
	{
	GuiControl,, StateABBox,LA	
	StateAB = LA
	State =Louisiana
	}
if StateFull = 20
	{
	GuiControl,, StateABBox,ME	
	StateAB = ME
	State =Maine
	}
if StateFull = 21
	{
	GuiControl,, StateABBox,MD	
	StateAB = MD
	DownNumber := 8
	GuiControl,, DownNumber,%DownNumber%
	State =Maryland
	}
if StateFull = 22
	{
	GuiControl,, StateABBox,MA	
	StateAB = MA
	State =Massachusetts
	}
if StateFull = 23
	{
	GuiControl,, StateABBox,MI	
	StateAB = MI
	State =Michigan
	}
if StateFull = 24
	{
	GuiControl,, StateABBox,MN	
	StateAB = MN
	State =Minnesota
	}
if StateFull = 25
	{
	GuiControl,, StateABBox,MS	
	StateAB = MS
	State =Mississippi
	}
if StateFull = 26
	{
	GuiControl,, StateABBox,MO	
	StateAB = MO
	State =Missouri
	}
if StateFull = 27
	{
	GuiControl,, StateABBox,MT	
	StateAB = MT
	State =Montana
	}
if StateFull = 28
	{
	GuiControl,, StateABBox,NE	
	StateAB = NE
	State =Nebraska
	}
if StateFull = 29
	{
	GuiControl,, StateABBox,NV	
	StateAB = NV
	State =Nevada
	}
if StateFull = 30
	{
	GuiControl,, StateABBox,NH	
	StateAB = NH
	State =New Hampshire
	}
if StateFull = 31
	{
	GuiControl,, StateABBox,NJ	
	StateAB = NJ
	State =New Jersey
	}
if StateFull = 32
	{
	GuiControl,, StateABBox,NM	
	StateAB = NM
	State =New Mexico
	}
if StateFull = 33
	{
	GuiControl,, StateABBox,NY	
	StateAB = NY
	State =New York
	DownNumber := 23
	GuiControl,, DownNumber,%DownNumber%
	}
if StateFull = 34
	{
	GuiControl,, StateABBox,NC	
	StateAB = NC
	State =North Carolina
	}
if StateFull = 35
	{
	GuiControl,, StateABBox,ND	
	StateAB = ND
	State =North Dakota
	}
if StateFull = 36
	{
	GuiControl,, StateABBox,OH	
	StateAB = OH
	State =Ohio
	}
if StateFull = 37
	{
	GuiControl,, StateABBox,OK	
	StateAB = OK
	State =Oklahoma
	}
if StateFull = 38
	{
	GuiControl,, StateABBox,OR	
	StateAB = OR
	State =Oregon
	}
if StateFull = 39
	{
	GuiControl,, StateABBox,PA	
	StateAB = PA
	State =Pennsylvania
	}
if StateFull = 40
	{
	GuiControl,, StateABBox,PR
	StateAB = PR
	State =Puerto Rico
	}
if StateFull = 41
	{
	GuiControl,, StateABBox,RI	
	StateAB = RI
	State =Rhode Island
	}
if StateFull = 42
	{
	GuiControl,, StateABBox,SC	
	StateAB = SC
	State =South Carolina
	}
if StateFull = 43
	{
	GuiControl,, StateABBox,SD	
	StateAB = SD
	State =South Dakota
	}
if StateFull = 44
	{
	GuiControl,, StateABBox,TN	
	StateAB = TN
	State =Tennessee
	}
if StateFull = 45
	{
	GuiControl,, StateABBox,TX	
	StateAB = TX
	State =Texas
	}
if StateFull = 46
	{
	GuiControl,, StateABBox,UT	
	StateAB = UT
	DownNumber := 9
	GuiControl,, DownNumber,%DownNumber%
	State =Utah
	}
if StateFull = 47
	{
	GuiControl,, StateABBox,VT	
	StateAB = VT
	State =Vermont
	}
if StateFull = 48
	{
	GuiControl,, StateABBox,VA	
	StateAB = VA
	State =Virginia
	}
if StateFull = 49
	{
	GuiControl,, StateABBox,WA	
	StateAB = WA
	State =Washington
	}
if StateFull = 50
	{
	GuiControl,, StateABBox,WV	
	StateAB = WV
	State =West Virginia
	}
if StateFull = 51
	{
	GuiControl,, StateABBox,WI	
	StateAB = WI
	State =Wisconsin
	}
if StateFull = 52
	{
	GuiControl,, StateABBox,WY	
	StateAB = WY
	State =Wyoming
	}
return

; ###Convert the monthly totals into a usable string of info###
MonthTotal:
Gui, Submit, nohide
StringReplace, MonthlyTotal, MonthlyTotalRaw, `n, `t, A
GuiControl,, MonthlyTotalFormatted,%MonthlyTotal%
return

; ###Add/Remove roof sections to the total calculations####
; If the box at the beginning of each roof section is unchecked,
; grey out that section's data and remove it from the calculations

AddSection1:
Gui, Submit, nohide
ToggleNumber = 1
gosub ToggleSectionNumber
return

AddSection2:
Gui, Submit, nohide
ToggleNumber = 2
gosub ToggleSectionNumber
return

AddSection3:
Gui, Submit, nohide
ToggleNumber = 3
gosub ToggleSectionNumber
return

AddSection4:
Gui, Submit, nohide
ToggleNumber = 4
gosub ToggleSectionNumber
return

AddSection5:
Gui, Submit, nohide
ToggleNumber = 5
gosub ToggleSectionNumber
return

AddSection6:
Gui, Submit, nohide
ToggleNumber = 6
gosub ToggleSectionNumber
return

AddSection7:
Gui, Submit, nohide
ToggleNumber = 7
gosub ToggleSectionNumber
return

AddSection8:
Gui, Submit, nohide
ToggleNumber = 8
gosub ToggleSectionNumber
return

AddSection9:
Gui, Submit, nohide
ToggleNumber = 9
gosub ToggleSectionNumber
return

ToggleSectionNumber:
If (RoofSection%ToggleNumber%Box = 1)
	{Gui, Font, c%SavedFontColor%
	GuiControl, Font, RoofSection%ToggleNumber%E
	GuiControl, Font, RoofSection%ToggleNumber%T
	GuiControl, Font, RoofSection%ToggleNumber%A
	GuiControl, Font, RoofSection%ToggleNumber%M
	GuiControl, Font, RoofSection%ToggleNumber%P
	GuiControl, Font, RoofSection%ToggleNumber%Z
	GuiControl, Font, RoofSection%ToggleNumber%S
	GuiControl, Font, RoofSection%ToggleNumber%F
	GuiControl, Font, RoofSection%ToggleNumber%V
	GuiControl, Font, RoofSection%ToggleNumber%D
	GuiControl, Font, RoofSection%ToggleNumber%R
	}else
		{Gui, Font, c505050
		GuiControl, Font, RoofSection%ToggleNumber%E
		GuiControl, Font, RoofSection%ToggleNumber%T
		GuiControl, Font, RoofSection%ToggleNumber%A
		GuiControl, Font, RoofSection%ToggleNumber%M
		GuiControl, Font, RoofSection%ToggleNumber%P
		GuiControl, Font, RoofSection%ToggleNumber%Z
		GuiControl, Font, RoofSection%ToggleNumber%S
		GuiControl, Font, RoofSection%ToggleNumber%F
		GuiControl, Font, RoofSection%ToggleNumber%V
		GuiControl, Font, RoofSection%ToggleNumber%D
		GuiControl, Font, RoofSection%ToggleNumber%R
		}
If (CalcAll = True)
	{return
	} else
	{gosub CalculateTotal
	gosub CalculateSection%ToggleNumber%
	}
return

; ###Calculate the size and sun hours###
; Each roof section's modules and production fields are set
; to recalculate any time something is entered, but if
; the user pastes the number or types too quickly,
; the calculations will likely be thrown off until
; they are changed again or the Calculate button is used.

CalculateSection1:
Gui, Submit, nohide
If Clear = True
	{
	return
	} else
	{SectionNumber = 1
	gosub CalculateSectionNumber
	}
GuiControl,, RoofSection1Z, %RoofSection1ZCalc%
GuiControl,, RoofSection1H, %RoofSection1HCalc%
GuiControl,, RoofSection1HBLL, %RoofSection1HCalc%
GuiControl,, RoofSection1HBLR, %RoofSection1HCalc%
GuiControl,, RoofSection1HBUR, %RoofSection1HCalc%
GuiControl,, RoofSection1HBUL, %RoofSection1HCalc%
If (RoofSection1Box = 1)
{	If (CalcAll = True)
	{
		return
	} else
		{
		gosub CalculateTotal
		}
}
return

CalculateSection2:
Gui, Submit, nohide
If Clear = True
	{
	return
	} else
	{SectionNumber = 2
	gosub CalculateSectionNumber
	}
GuiControl,, RoofSection2Z, %RoofSection2ZCalc%
GuiControl,, RoofSection2H, %RoofSection2HCalc%
GuiControl,, RoofSection2HBLL, %RoofSection2HCalc%
GuiControl,, RoofSection2HBLR, %RoofSection2HCalc%
GuiControl,, RoofSection2HBUR, %RoofSection2HCalc%
GuiControl,, RoofSection2HBUL, %RoofSection2HCalc%
If (RoofSection2Box = 1)
{	If (CalcAll = True)
	{
		return
	} else
		{
		gosub CalculateTotal
		}
}
return

CalculateSection3:
Gui, Submit, nohide
If Clear = True
	{
	return
	} else
	{SectionNumber = 3
	gosub CalculateSectionNumber
	}
GuiControl,, RoofSection3Z, %RoofSection3ZCalc%
GuiControl,, RoofSection3H, %RoofSection3HCalc%
GuiControl,, RoofSection3HBLL, %RoofSection3HCalc%
GuiControl,, RoofSection3HBLR, %RoofSection3HCalc%
GuiControl,, RoofSection3HBUR, %RoofSection3HCalc%
GuiControl,, RoofSection3HBUL, %RoofSection3HCalc%
If (RoofSection3Box = 1)
{	If (CalcAll = True)
	{
		return
	} else
		{
		gosub CalculateTotal
		}
}
return

CalculateSection4:
Gui, Submit, nohide
If Clear = True
	{
	return
	} else
	{SectionNumber = 4
	gosub CalculateSectionNumber
	}
GuiControl,, RoofSection4Z, %RoofSection4ZCalc%
GuiControl,, RoofSection4H, %RoofSection4HCalc%
GuiControl,, RoofSection4HBLL, %RoofSection4HCalc%
GuiControl,, RoofSection4HBLR, %RoofSection4HCalc%
GuiControl,, RoofSection4HBUR, %RoofSection4HCalc%
GuiControl,, RoofSection4HBUL, %RoofSection4HCalc%
If (RoofSection4Box = 1)
{	If (CalcAll = True)
	{
		return
	} else
		{
		gosub CalculateTotal
		}
}
return

CalculateSection5:
Gui, Submit, nohide
If Clear = True
	{
	return
	} else
	{SectionNumber = 5
	gosub CalculateSectionNumber
	}
GuiControl,, RoofSection5Z, %RoofSection5ZCalc%
GuiControl,, RoofSection5H, %RoofSection5HCalc%
GuiControl,, RoofSection5HBLL, %RoofSection5HCalc%
GuiControl,, RoofSection5HBLR, %RoofSection5HCalc%
GuiControl,, RoofSection5HBUR, %RoofSection5HCalc%
GuiControl,, RoofSection5HBUL, %RoofSection5HCalc%
If (RoofSection5Box = 1)
{	If (CalcAll = True)
	{
		return
	} else
		{
		gosub CalculateTotal
		}
}
return

CalculateSection6:
Gui, Submit, nohide
If Clear = True
	{
	return
	} else
	{SectionNumber = 6
	gosub CalculateSectionNumber
	}
GuiControl,, RoofSection6Z, %RoofSection6ZCalc%
GuiControl,, RoofSection6H, %RoofSection6HCalc%
GuiControl,, RoofSection6HBLL, %RoofSection6HCalc%
GuiControl,, RoofSection6HBLR, %RoofSection6HCalc%
GuiControl,, RoofSection6HBUR, %RoofSection6HCalc%
GuiControl,, RoofSection6HBUL, %RoofSection6HCalc%
If (RoofSection6Box = 1)
{	If (CalcAll = True)
	{
		return
	} else
		{
		gosub CalculateTotal
		}
}
return

CalculateSection7:
Gui, Submit, nohide
If Clear = True
	{
	return
	} else
	{SectionNumber = 7
	gosub CalculateSectionNumber
	}
GuiControl,, RoofSection7Z, %RoofSection7ZCalc%
GuiControl,, RoofSection7H, %RoofSection7HCalc%
GuiControl,, RoofSection7HBLL, %RoofSection7HCalc%
GuiControl,, RoofSection7HBLR, %RoofSection7HCalc%
GuiControl,, RoofSection7HBUR, %RoofSection7HCalc%
GuiControl,, RoofSection7HBUL, %RoofSection7HCalc%
If (RoofSection7Box = 1)
{	If (CalcAll = True)
	{
		return
	} else
		{
		gosub CalculateTotal
		}
}
return

CalculateSection8:
Gui, Submit, nohide
If Clear = True
	{
	return
	} else
	{SectionNumber = 8
	gosub CalculateSectionNumber
	}
GuiControl,, RoofSection8Z, %RoofSection8ZCalc%
GuiControl,, RoofSection8H, %RoofSection8HCalc%
GuiControl,, RoofSection8HBLL, %RoofSection8HCalc%
GuiControl,, RoofSection8HBLR, %RoofSection8HCalc%
GuiControl,, RoofSection8HBUR, %RoofSection8HCalc%
GuiControl,, RoofSection8HBUL, %RoofSection8HCalc%
If (RoofSection8Box = 1)
{	If (CalcAll = True)
	{
		return
	} else
		{
		gosub CalculateTotal
		}
}
return

CalculateSection9:
Gui, Submit, nohide
If Clear = True
	{
	return
	} else
	{SectionNumber = 9
	gosub CalculateSectionNumber
	}
GuiControl,, RoofSection9Z, %RoofSection9ZCalc%
GuiControl,, RoofSection9H, %RoofSection9HCalc%
GuiControl,, RoofSection9HBLL, %RoofSection9HCalc%
GuiControl,, RoofSection9HBLR, %RoofSection9HCalc%
GuiControl,, RoofSection9HBUR, %RoofSection9HCalc%
GuiControl,, RoofSection9HBUL, %RoofSection9HCalc%
If (RoofSection9Box = 1)
{	If (CalcAll = True)
	{
		return
	} else
		{
		gosub CalculateTotal
		}
}
return
	
CalculateSectionNumber:
RoofSection%SectionNumber%ZCalc := Rating * RoofSection%SectionNumber%M
RoofSection%SectionNumber%HCalc := RoofSection%SectionNumber%P / RoofSection%SectionNumber%ZCalc
StringLeft, RoofSection%SectionNumber%ZCalc, RoofSection%SectionNumber%ZCalc, 5
StringLeft, RoofSection%SectionNumber%HCalc, RoofSection%SectionNumber%HCalc, 4

If (RoofSection%SectionNumber%Box = 1)
{If RoofSection%SectionNumber%HCalc >= %SystemMinimum%
	{GuiControl, +c00FF00, RoofSection%SectionNumber%H
	GuiControl, +c000000, RoofSection%SectionNumber%HBLL
	GuiControl, +c000000, RoofSection%SectionNumber%HBLR
	GuiControl, +c000000, RoofSection%SectionNumber%HBUR
	GuiControl, +c000000, RoofSection%SectionNumber%HBUL
	} else
		{If RoofSection%SectionNumber%HCalc < %SectionMinimum%
			{GuiControl, +c0000FF, RoofSection%SectionNumber%H, 
			GuiControl, +c25FFFF, RoofSection%SectionNumber%HBLL
			GuiControl, +c25FFFF, RoofSection%SectionNumber%HBLR
			GuiControl, +c25FFFF, RoofSection%SectionNumber%HBUR
			GuiControl, +c25FFFF, RoofSection%SectionNumber%HBUL
			} else
			{If RoofSection%SectionNumber%HCalc >= %SectionMinimum%
				{GuiControl, +cFFFF00, RoofSection%SectionNumber%H
				GuiControl, +c000000, RoofSection%SectionNumber%HBLL
				GuiControl, +c000000, RoofSection%SectionNumber%HBLR
				GuiControl, +c000000, RoofSection%SectionNumber%HBUR
				GuiControl, +c000000, RoofSection%SectionNumber%HBUL
				}
			}
		}
}else
	{GuiControl, +c000000, RoofSection%SectionNumber%H
	GuiControl, +c505050, RoofSection%SectionNumber%HBLL
	GuiControl, +c505050, RoofSection%SectionNumber%HBLR
	GuiControl, +c505050, RoofSection%SectionNumber%HBUR
	GuiControl, +c505050, RoofSection%SectionNumber%HBUL
	}
return

; ###Calculate the totals###
; Note that the tool will alter the color of the
; progress bar to reflect how close to Usage
; the currently-selected roof sections are.
; If it shows as being Above usage, then some
; modules should be removed to bring it to just
; above usage (NY Office definition of 'Usage').

CalculateTotal:
Gui, Submit, nohide
; 1.5 is the number used since it isn't common that
; the most efficient design will have 150% production
; for each module.
GoalCap := GoalMin + (1.5 * (Rating * 1000))

If (Clear = True)
	{
	return
	}
If ((RoofSection1Box = 1) and (RoofSection1M != "") and (RoofSection1P != ""))
	{RoofSection1addM := RoofSection1M
	RoofSection1addP := RoofSection1P
	} else
	{RoofSection1addM := 0
	RoofSection1addP := 0
	}
If ((RoofSection2Box = 1) and (RoofSection2M != "") and (RoofSection2P != ""))
	{RoofSection2addM := RoofSection2M
	RoofSection2addP := RoofSection2P
	} else
	{RoofSection2addM := 0
	RoofSection2addP := 0
	}
If ((RoofSection3Box = 1) and (RoofSection3M != "") and (RoofSection3P != ""))
	{RoofSection3addM := RoofSection3M
	RoofSection3addP := RoofSection3P
	} else
	{RoofSection3addM := 0
	RoofSection3addP := 0
	}
If ((RoofSection4Box = 1) and (RoofSection4M != "") and (RoofSection4P != ""))
	{RoofSection4addM := RoofSection4M
	RoofSection4addP := RoofSection4P
	} else
	{RoofSection4addM := 0
	RoofSection4addP := 0
	}
If ((RoofSection5Box = 1) and (RoofSection5M != "") and (RoofSection5P != ""))
	{RoofSection5addM := RoofSection5M
	RoofSection5addP := RoofSection5P
	} else
	{RoofSection5addM := 0
	RoofSection5addP := 0
	}
If ((RoofSection6Box = 1) and (RoofSection6M != "") and (RoofSection6P != ""))
	{RoofSection6addM := RoofSection6M
	RoofSection6addP := RoofSection6P
	} else
	{RoofSection6addM := 0
	RoofSection6addP := 0
	}
If ((RoofSection7Box = 1) and (RoofSection7M != "") and (RoofSection7P != ""))
	{RoofSection7addM := RoofSection7M
	RoofSection7addP := RoofSection7P
	} else
	{RoofSection7addM := 0
	RoofSection7addP := 0
	}
If ((RoofSection8Box = 1) and (RoofSection8M != "") and (RoofSection8P != ""))
	{RoofSection8addM := RoofSection8M
	RoofSection8addP := RoofSection8P
	} else
	{RoofSection8addM := 0
	RoofSection8addP := 0
	}
If ((RoofSection9Box = 1) and (RoofSection9M != "") and (RoofSection9P != ""))
	{RoofSection9addM := RoofSection9M
	RoofSection9addP := RoofSection9P
	} else
	{RoofSection9addM := 0
	RoofSection9addP := 0
	}
RoofSectionSigmaM := RoofSection1addM + RoofSection2addM + RoofSection3addM + RoofSection4addM + RoofSection5addM + RoofSection6addM + RoofSection7addM + RoofSection8addM + RoofSection9addM
RoofSectionSigmaP := RoofSection1addP + RoofSection2addP + RoofSection3addP + RoofSection4addP + RoofSection5addP + RoofSection6addP + RoofSection7addP + RoofSection8addP + RoofSection9addP
RoofSectionSigmaZCalc := Rating * RoofSectionSigmaM
RoofSectionSigmaHCalc := RoofSectionSigmaP / (Rating * RoofSectionSigmaM)
if (RoofSectionSigmaZCalc <10)
	{StringLeft, RoofSectionSigmaZCalc, RoofSectionSigmaZCalc, 5
	}else
	{StringLeft, RoofSectionSigmaZCalc, RoofSectionSigmaZCalc, 6
	}
StringLeft, RoofSectionSigmaHCalc, RoofSectionSigmaHCalc, 4
ProdPercentage := 100 * (RoofSectionSigmaP / GoalMin)
GuiControl,, ProdProg, %ProdPercentage%

; ###COLOR CONTROLS###
; If a roof section is above the section minimum
; but not the system minimum, its color will turn
; yellow. If it is above the system minimum,
; it will turn green. Otherwise the color will
; default to blue, indicating unusable sections.
; NTS- Replace Edit## with fields' names somehow
; Sample colors below
;	c00FF00 Green
;	c0000FF Blue
;	cFFFF00 Yellow
;	c000000 Black
;	cFFFFFF White
;	c25FFFF Cyan-ish

; ###Now replace the old numbers with the various new calculations###

; ###Totals###
If RoofSectionSigmaM >=%MM%
{GuiControl, +c00FF00, RoofSectionSigmaM
GuiControl, +c000000, RoofSectionSigmaMBLL
GuiControl, +c000000, RoofSectionSigmaMBLR
GuiControl, +c000000, RoofSectionSigmaMBUR
GuiControl, +c000000, RoofSectionSigmaMBUL
} else
	{GuiControl, +c0000FF, RoofSectionSigmaM
	GuiControl, +c25FFFF, RoofSectionSigmaMBLL
	GuiControl, +c25FFFF, RoofSectionSigmaMBLR
	GuiControl, +c25FFFF, RoofSectionSigmaMBUR
	GuiControl, +c25FFFF, RoofSectionSigmaMBUL
	}
If RoofSectionSigmaHCalc >= %SystemMinimum%
{GuiControl, +c00FF00, RoofSectionSigmaH
GuiControl, +c000000, RoofSectionSigmaHBLL
GuiControl, +c000000, RoofSectionSigmaHBLR
GuiControl, +c000000, RoofSectionSigmaHBUR
GuiControl, +c000000, RoofSectionSigmaHBUL
} else
	{GuiControl, +c0000FF, RoofSectionSigmaH
	GuiControl, +c25FFFF, RoofSectionSigmaHBLL
	GuiControl, +c25FFFF, RoofSectionSigmaHBLR
	GuiControl, +c25FFFF, RoofSectionSigmaHBUR
	GuiControl, +c25FFFF, RoofSectionSigmaHBUL
	}

; ###Compare the production estimate and the Goal###
if (GoalMin < 1)
{GuiControl, +c000000, GoalText
GuiControl, +cFF0000, GoalTextBLL
GuiControl, +cFF0000, GoalTextBLR
GuiControl, +cFF0000, GoalTextBUR
GuiControl, +cFF0000, GoalTextBUL
GuiControl,, GoalText,$GOAL
GuiControl,, GoalTextBLL,$GOAL
GuiControl,, GoalTextBLR,$GOAL
GuiControl,, GoalTextBUR,$GOAL
GuiControl,, GoalTextBUL,$GOAL
} else
		{if (RoofSectionSigmaP > GoalCap)
			{GuiControl, +cFFFF00, GoalText
			GuiControl, +cFFFF00, ProdProg
			GuiControl, +c000000, GoalTextBLL
			GuiControl, +c000000, GoalTextBLR
			GuiControl, +c000000, GoalTextBUR
			GuiControl, +c000000, GoalTextBUL
			GuiControl,, GoalText,ABOVE!
			GuiControl,, GoalTextBLL,ABOVE!
			GuiControl,, GoalTextBLR,ABOVE!
			GuiControl,, GoalTextBUR,ABOVE!
			GuiControl,, GoalTextBUL,ABOVE!
			} else
				{if (RoofSectionSigmaP < GoalMin)
					{GuiControl, +c0000FF, GoalText
					GuiControl, +c0000FF, ProdProg
					GuiControl, +c25FFFF, GoalTextBLL
					GuiControl, +c25FFFF, GoalTextBLR
					GuiControl, +c25FFFF, GoalTextBUR
					GuiControl, +c25FFFF, GoalTextBUL
					GuiControl,, GoalText,BELOW
					GuiControl,, GoalTextBLL,BELOW
					GuiControl,, GoalTextBLR,BELOW
					GuiControl,, GoalTextBUR,BELOW
					GuiControl,, GoalTextBUL,BELOW
					} else
						;(RoofSectionSigmaP >= GoalMin)
						{GuiControl, +c00FF00, GoalText
						GuiControl, +c00FF00, ProdProg
						GuiControl, +c000000, GoalTextBLL
						GuiControl, +c000000, GoalTextBLR
						GuiControl, +c000000, GoalTextBUR
						GuiControl, +c000000, GoalTextBUL
						GuiControl,, GoalText,MET!
						GuiControl,, GoalTextBLL,MET!
						GuiControl,, GoalTextBLR,MET!
						GuiControl,, GoalTextBUR,MET!
						GuiControl,, GoalTextBUL,MET!
						}
				}
		}

; Compare the production versus the annual
; usage to get an offset percentage
OffsetPercentage := 100* (RoofSectionSigmaP / Annual)
StringLeft, OffsetPercentage, OffsetPercentage, 4
GuiControl,,OffsetP,%OffsetPercentage%`%
GuiControl,,RoofSectionSigmaM, %RoofSectionSigmaM%
GuiControl,,RoofSectionSigmaMBUL, %RoofSectionSigmaM%
GuiControl,,RoofSectionSigmaMBLL, %RoofSectionSigmaM%
GuiControl,,RoofSectionSigmaMBLR, %RoofSectionSigmaM%
GuiControl,,RoofSectionSigmaMBUR, %RoofSectionSigmaM%
GuiControl,,RoofSectionSigmaP,%RoofSectionSigmaP%
GuiControl,,RoofSectionSigmaZ,%RoofSectionSigmaZCalc%
GuiControl,,RoofSectionSigmaH,%RoofSectionSigmaHCalc%
GuiControl,,RoofSectionSigmaHBLL,%RoofSectionSigmaHCalc%
GuiControl,,RoofSectionSigmaHBUL,%RoofSectionSigmaHCalc%
GuiControl,,RoofSectionSigmaHBLR,%RoofSectionSigmaHCalc%
GuiControl,,RoofSectionSigmaHBUR,%RoofSectionSigmaHCalc%
return

; ###Buttons, and what they do###
; The Calculate button essentially recalculates
; all roof sections that have their boxes checked

ButtonCALCULATE:
CalculateAll:
Gui, Submit, nohide
CalcAll := True
gosub StateInfo
gosub MonthTotal
gosub MPMP
gosub NewDirections
gosub AddSection1
gosub AddSection2
gosub AddSection3
gosub AddSection4
gosub AddSection5
gosub AddSection6
gosub AddSection7
gosub AddSection8
gosub AddSection9
gosub CalculateSection1
gosub CalculateSection2
gosub CalculateSection3
gosub CalculateSection4
gosub CalculateSection5
gosub CalculateSection6
gosub CalculateSection7
gosub CalculateSection8
gosub CalculateSection9
gosub CalculateTotal
CalcAll := False
return

MPMP:
Gui, Submit, nohide
MinPerModPro := Rating * (1000 * Efficiency)
StringLeft, MinPerModPro, MinPerModPro, 3
GuiControl,,PerModProMin, %MinPerModPro% kWh
return

; ###The Refresh button across the top of the GUI###
ButtonRefresh:
WinGetPos, winX, winY,
IniWrite, %BoardHide%, %A_MyDocuments%\IEUserSettings.ini, Main, BoardHide
IniWrite, %TaskBarHide%, %A_MyDocuments%\IEUserSettings.ini, Main, TaskBarHide
IniWrite, %winX%, %A_MyDocuments%\IEUserSettings.ini, Main, xPos
IniWrite, %winY%, %A_MyDocuments%\IEUserSettings.ini, Main, yPos
reload
return

; ###Save Button###
; It saves everything but the text that is
; displayed as a result of calculations!
ButtonhwndSave:
ButtonSave:
Gui, Submit, nohide
; Begin with the common back-end stuff so it can be easily
; found if needed for editing (manually switching module ratings, etc)
IniWrite, %winX%, %A_MyDocuments%\IEUserSettings.ini, Main, xPos
IniWrite, %winY%, %A_MyDocuments%\IEUserSettings.ini, Main, yPos
IniWrite, %BGPIC%, %A_MyDocuments%\IEUserSettings.ini, Main, SavedBackground
IniWrite, %IN%, %A_MyDocuments%\IEUserSettings.ini, main, IN
IniWrite, %BoardHide%, %A_MyDocuments%\IEUserSettings.ini, Main, BoardHide
IniWrite, %TaskBarHide%, %A_MyDocuments%\IEUserSettings.ini, Main, TaskBarHide
; Now save the color information
SetFormat, integer, d
Red_Value += 0
Green_Value += 0
Blue_Value += 0
IniWrite, %Red_Value%, %A_MyDocuments%\IEUserSettings.ini, Color, ValueR
IniWrite, %Green_Value%, %A_MyDocuments%\IEUserSettings.ini, Color, ValueG
IniWrite, %Blue_Value%, %A_MyDocuments%\IEUserSettings.ini, Color, ValueB
IniWrite, %Grey_Value%, %A_MyDocuments%\IEUserSettings.ini, Color, ValueGR
IniWrite, %SavedBackColor%, %A_MyDocuments%\IEUserSettings.ini, Color, BackColor
IniWrite, %SavedFontColor%, %A_MyDocuments%\IEUserSettings.ini, Color, FontColor
; Now save the rest of the stuff
IniWrite, %DownNumber%, %A_ScriptDir%\%FileName%.ini, Main, DownNumber
IniWrite, %Rating%, %A_ScriptDir%\%FileName%.ini, Main, Rating
IniWrite, %Efficiency%, %A_ScriptDir%\%FileName%.ini, Main, Efficiency
IniWrite, %StateFull%, %A_ScriptDir%\%FileName%.ini, Main, StateFull
IniWrite, %StateAB%, %A_ScriptDir%\%FileName%.ini, Main, StateAB
IniWrite, %SystemMinimum%, %A_ScriptDir%\%FileName%.ini, Main, SystemMinimum
IniWrite, %SectionMinimum%, %A_ScriptDir%\%FileName%.ini, Main, SectionMinimum
IniWrite, %AR%, %A_ScriptDir%\%FileName%.ini, main, AR
IniWrite, %LastName%, %A_ScriptDir%\%FileName%.ini, main, LastName
IniWrite, %Address%, %A_ScriptDir%\%FileName%.ini, main, Address
IniWrite, %City%, %A_ScriptDir%\%FileName%.ini, main, City
IniWrite, %Zip%, %A_ScriptDir%\%FileName%.ini, main, Zip
StringReplace, TextBoxConverted, TextBox, `n, $n$, A
IniWrite, %TextBoxConverted%, %A_ScriptDir%\%FileName%.ini, Main, TextBox
StringReplace, TotalsConverted, MonthlyTotalRaw, `n, $n$, A
IniWrite, %TotalsConverted%, %A_ScriptDir%\%FileName%.ini, Main, TotalsBox
IniWrite, %GoalMin%, %A_ScriptDir%\%FileName%.ini, main, GoalMin
IniWrite, %Annual%, %A_ScriptDir%\%FileName%.ini, main, Annual
IniWrite, %RoofSection1T%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1T
IniWrite, %RoofSection1A%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1A
IniWrite, %RoofSection1E%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1E
IniWrite, %RoofSection1M%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1M
IniWrite, %RoofSection1Z%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1Z
IniWrite, %RoofSection1P%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1P
IniWrite, %RoofSection1S%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1S
IniWrite, %RoofSection1F%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1F
IniWrite, %RoofSection1H%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1H
IniWrite, %RoofSection1V%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1V
IniWrite, %RoofSection1D%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1D
IniWrite, %RoofSection1R%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1R
IniWrite, %RoofSection2T%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2T
IniWrite, %RoofSection2A%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2A
IniWrite, %RoofSection2E%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2E
IniWrite, %RoofSection2M%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2M
IniWrite, %RoofSection2Z%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2Z
IniWrite, %RoofSection2P%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2P
IniWrite, %RoofSection2S%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2S
IniWrite, %RoofSection2F%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2F
IniWrite, %RoofSection2H%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2H
IniWrite, %RoofSection2V%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2V
IniWrite, %RoofSection2D%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2D
IniWrite, %RoofSection2R%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2R
IniWrite, %RoofSection3T%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3T
IniWrite, %RoofSection3A%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3A
IniWrite, %RoofSection3E%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3E
IniWrite, %RoofSection3M%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3M
IniWrite, %RoofSection3Z%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3Z
IniWrite, %RoofSection3P%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3P
IniWrite, %RoofSection3S%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3S
IniWrite, %RoofSection3F%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3F
IniWrite, %RoofSection3H%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3H
IniWrite, %RoofSection3V%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3V
IniWrite, %RoofSection3D%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3D
IniWrite, %RoofSection3R%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3R
IniWrite, %RoofSection4T%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4T
IniWrite, %RoofSection4A%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4A
IniWrite, %RoofSection4E%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4E
IniWrite, %RoofSection4M%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4M
IniWrite, %RoofSection4Z%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4Z
IniWrite, %RoofSection4P%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4P
IniWrite, %RoofSection4S%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4S
IniWrite, %RoofSection4F%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4F
IniWrite, %RoofSection4H%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4H
IniWrite, %RoofSection4V%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4V
IniWrite, %RoofSection4D%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4D
IniWrite, %RoofSection4R%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4R
IniWrite, %RoofSection5T%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5T
IniWrite, %RoofSection5A%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5A
IniWrite, %RoofSection5E%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5E
IniWrite, %RoofSection5M%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5M
IniWrite, %RoofSection5Z%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5Z
IniWrite, %RoofSection5P%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5P
IniWrite, %RoofSection5S%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5S
IniWrite, %RoofSection5F%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5F
IniWrite, %RoofSection5H%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5H
IniWrite, %RoofSection5V%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5V
IniWrite, %RoofSection5D%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5D
IniWrite, %RoofSection5R%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5R
IniWrite, %RoofSection6T%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6T
IniWrite, %RoofSection6A%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6A
IniWrite, %RoofSection6E%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6E
IniWrite, %RoofSection6M%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6M
IniWrite, %RoofSection6Z%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6Z
IniWrite, %RoofSection6P%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6P
IniWrite, %RoofSection6S%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6S
IniWrite, %RoofSection6F%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6F
IniWrite, %RoofSection6H%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6H
IniWrite, %RoofSection6V%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6V
IniWrite, %RoofSection6D%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6D
IniWrite, %RoofSection6R%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6R
IniWrite, %RoofSection7T%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7T
IniWrite, %RoofSection7A%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7A
IniWrite, %RoofSection7E%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7E
IniWrite, %RoofSection7M%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7M
IniWrite, %RoofSection7Z%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7Z
IniWrite, %RoofSection7P%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7P
IniWrite, %RoofSection7S%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7S
IniWrite, %RoofSection7F%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7F
IniWrite, %RoofSection7H%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7H
IniWrite, %RoofSection7V%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7V
IniWrite, %RoofSection7D%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7D
IniWrite, %RoofSection7R%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7R
IniWrite, %RoofSection8T%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8T
IniWrite, %RoofSection8A%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8A
IniWrite, %RoofSection8E%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8E
IniWrite, %RoofSection8M%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8M
IniWrite, %RoofSection8Z%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8Z
IniWrite, %RoofSection8P%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8P
IniWrite, %RoofSection8S%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8S
IniWrite, %RoofSection8F%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8F
IniWrite, %RoofSection8H%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8H
IniWrite, %RoofSection8V%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8V
IniWrite, %RoofSection8D%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8D
IniWrite, %RoofSection8R%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8R
IniWrite, %RoofSection9T%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9T
IniWrite, %RoofSection9A%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9A
IniWrite, %RoofSection9E%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9E
IniWrite, %RoofSection9M%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9M
IniWrite, %RoofSection9Z%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9Z
IniWrite, %RoofSection9P%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9P
IniWrite, %RoofSection9S%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9S
IniWrite, %RoofSection9F%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9F
IniWrite, %RoofSection9H%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9H
IniWrite, %RoofSection9V%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9V
IniWrite, %RoofSection9D%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9D
IniWrite, %RoofSection9R%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9R
IniWrite, %RoofSection1Box%, %A_ScriptDir%\%FileName%.ini, main, RoofSection1Box
IniWrite, %RoofSection2Box%, %A_ScriptDir%\%FileName%.ini, main, RoofSection2Box
IniWrite, %RoofSection3Box%, %A_ScriptDir%\%FileName%.ini, main, RoofSection3Box
IniWrite, %RoofSection4Box%, %A_ScriptDir%\%FileName%.ini, main, RoofSection4Box
IniWrite, %RoofSection5Box%, %A_ScriptDir%\%FileName%.ini, main, RoofSection5Box
IniWrite, %RoofSection6Box%, %A_ScriptDir%\%FileName%.ini, main, RoofSection6Box
IniWrite, %RoofSection7Box%, %A_ScriptDir%\%FileName%.ini, main, RoofSection7Box
IniWrite, %RoofSection8Box%, %A_ScriptDir%\%FileName%.ini, main, RoofSection8Box
IniWrite, %RoofSection9Box%, %A_ScriptDir%\%FileName%.ini, main, RoofSection9Box
IniWrite, %DirE%, %A_ScriptDir%\%FileName%.ini, main, DirE
IniWrite, %DirW%, %A_ScriptDir%\%FileName%.ini, main, DirW
IniWrite, %DirN%, %A_ScriptDir%\%FileName%.ini, main, DirN
IniWrite, %DirS%, %A_ScriptDir%\%FileName%.ini, main, DirS
return

ButtonCLEAR:

SetControlDelay, 0
Gui, Submit, nohide
Clear := True
Guicontrol,, DirE
Guicontrol,, DirW
Guicontrol,, DirN
Guicontrol,, DirS
Guicontrol,, AR, $NUM
Guicontrol,, LastName, $LAST_NAME
Guicontrol,, Address, $ADDRESS
Guicontrol,, City, $CITY
Guicontrol,, Zip, $ZIP
Guicontrol,, TextBox,
Guicontrol,, GoalMin,
Guicontrol,, Annual,
Guicontrol,, RoofSection1T
Guicontrol,, RoofSection1A
Guicontrol,, RoofSection1E
Guicontrol,, RoofSection1M
Guicontrol,, RoofSection1Z
Guicontrol,, RoofSection1P
Guicontrol,, RoofSection1S
Guicontrol,, RoofSection1F
Guicontrol,, RoofSection1H
Guicontrol,, RoofSection1HBLL
Guicontrol,, RoofSection1HBLR
Guicontrol,, RoofSection1HBUL
Guicontrol,, RoofSection1HBUR
Guicontrol,, RoofSection1V
Guicontrol,, RoofSection1D
Guicontrol,, RoofSection1R
Guicontrol,, RoofSection2T
Guicontrol,, RoofSection2A
Guicontrol,, RoofSection2E
Guicontrol,, RoofSection2M
Guicontrol,, RoofSection2Z
Guicontrol,, RoofSection2P
Guicontrol,, RoofSection2S
Guicontrol,, RoofSection2F
Guicontrol,, RoofSection2H
Guicontrol,, RoofSection2HBLL
Guicontrol,, RoofSection2HBLR
Guicontrol,, RoofSection2HBUL
Guicontrol,, RoofSection2HBUR
Guicontrol,, RoofSection2V
Guicontrol,, RoofSection2D
Guicontrol,, RoofSection2R
Guicontrol,, RoofSection3T
Guicontrol,, RoofSection3A
Guicontrol,, RoofSection3E
Guicontrol,, RoofSection3M
Guicontrol,, RoofSection3Z
Guicontrol,, RoofSection3P
Guicontrol,, RoofSection3S
Guicontrol,, RoofSection3F
Guicontrol,, RoofSection3H
Guicontrol,, RoofSection3HBLL
Guicontrol,, RoofSection3HBLR
Guicontrol,, RoofSection3HBUL
Guicontrol,, RoofSection3HBUR
Guicontrol,, RoofSection3V
Guicontrol,, RoofSection3D
Guicontrol,, RoofSection3R
Guicontrol,, RoofSection4T
Guicontrol,, RoofSection4A
Guicontrol,, RoofSection4E
Guicontrol,, RoofSection4M
Guicontrol,, RoofSection4Z
Guicontrol,, RoofSection4P
Guicontrol,, RoofSection4S
Guicontrol,, RoofSection4F
Guicontrol,, RoofSection4H
Guicontrol,, RoofSection4HBLL
Guicontrol,, RoofSection4HBLR
Guicontrol,, RoofSection4HBUL
Guicontrol,, RoofSection4HBUR
Guicontrol,, RoofSection4V
Guicontrol,, RoofSection4D
Guicontrol,, RoofSection4R
Guicontrol,, RoofSection5T
Guicontrol,, RoofSection5A
Guicontrol,, RoofSection5E
Guicontrol,, RoofSection5M
Guicontrol,, RoofSection5Z
Guicontrol,, RoofSection5P
Guicontrol,, RoofSection5S
Guicontrol,, RoofSection5F
Guicontrol,, RoofSection5H
Guicontrol,, RoofSection5HBLL
Guicontrol,, RoofSection5HBLR
Guicontrol,, RoofSection5HBUL
Guicontrol,, RoofSection5HBUR
Guicontrol,, RoofSection5V
Guicontrol,, RoofSection5D
Guicontrol,, RoofSection5R
Guicontrol,, RoofSection6T
Guicontrol,, RoofSection6A
Guicontrol,, RoofSection6E
Guicontrol,, RoofSection6M
Guicontrol,, RoofSection6Z
Guicontrol,, RoofSection6P
Guicontrol,, RoofSection6S
Guicontrol,, RoofSection6F
Guicontrol,, RoofSection6H
Guicontrol,, RoofSection6HBLL
Guicontrol,, RoofSection6HBLR
Guicontrol,, RoofSection6HBUL
Guicontrol,, RoofSection6HBUR
Guicontrol,, RoofSection6V
Guicontrol,, RoofSection6D
Guicontrol,, RoofSection6R
Guicontrol,, RoofSection7T
Guicontrol,, RoofSection7A
Guicontrol,, RoofSection7E
Guicontrol,, RoofSection7M
Guicontrol,, RoofSection7Z
Guicontrol,, RoofSection7P
Guicontrol,, RoofSection7S
Guicontrol,, RoofSection7F
Guicontrol,, RoofSection7H
Guicontrol,, RoofSection7HBLL
Guicontrol,, RoofSection7HBLR
Guicontrol,, RoofSection7HBUL
Guicontrol,, RoofSection7HBUR
Guicontrol,, RoofSection7V
Guicontrol,, RoofSection7D
Guicontrol,, RoofSection7R
Guicontrol,, RoofSection8T
Guicontrol,, RoofSection8A
Guicontrol,, RoofSection8E
Guicontrol,, RoofSection8M
Guicontrol,, RoofSection8Z
Guicontrol,, RoofSection8P
Guicontrol,, RoofSection8S
Guicontrol,, RoofSection8F
Guicontrol,, RoofSection8H
Guicontrol,, RoofSection8HBLL
Guicontrol,, RoofSection8HBLR
Guicontrol,, RoofSection8HBUL
Guicontrol,, RoofSection8HBUR
Guicontrol,, RoofSection8V
Guicontrol,, RoofSection8D
Guicontrol,, RoofSection8R
Guicontrol,, RoofSection9T
Guicontrol,, RoofSection9A
Guicontrol,, RoofSection9E
Guicontrol,, RoofSection9M
Guicontrol,, RoofSection9Z
Guicontrol,, RoofSection9P
Guicontrol,, RoofSection9S
Guicontrol,, RoofSection9F
Guicontrol,, RoofSection9H
Guicontrol,, RoofSection9HBLL
Guicontrol,, RoofSection9HBLR
Guicontrol,, RoofSection9HBUL
Guicontrol,, RoofSection9HBUR
Guicontrol,, RoofSection9V
Guicontrol,, RoofSection9D
Guicontrol,, RoofSection9R
Guicontrol,, MonthlyTotalRaw
Guicontrol,, MonthlyTotalFormatted
; This one needs to be cleared to blank,
; otherwise the data it stores can carry
; over to another design
MonthlyTotal := ""
; Default delay of 20 to get it back to normal speed
SetControlDelay, 20
Control, Check,, Button11
Control, Check,, Button12
Control, Check,, Button13
Control, Check,, Button14
Control, Check,, Button15
Control, Check,, Button16
Control, Check,, Button17
Control, Check,, Button18
Control, Check,, Button19
Clear := False
gosub CalculateTotal
return

ButtonHide:
IfWinNotActive, InfoEntry
{
	Gui,Show
	return
}
	Gui, Hide
return

; ###Export Summary button###
; Export all the roof section data into a .txt file
; wherever the tool is located. Useful for LHCs or
; designs likely to be redesigned later.
ButtonSummaryExport:
Gui, Submit, NoHide
FileAppend,
(
Roof	Tilt	Azim	Eyes	Mods	Size	Prod	Hours
1	%RoofSection1T%	%RoofSection1A%	%RoofSection1E%	%RoofSection1M%	%RoofSection1ZCalc%	%RoofSection1P%	%RoofSection1HCalc%
2	%RoofSection2T%	%RoofSection2A%	%RoofSection2E%	%RoofSection2M%	%RoofSection2ZCalc%	%RoofSection2P%	%RoofSection2HCalc%
3	%RoofSection3T%	%RoofSection3A%	%RoofSection3E%	%RoofSection3M%	%RoofSection3ZCalc%	%RoofSection3P%	%RoofSection3HCalc%
4	%RoofSection4T%	%RoofSection4A%	%RoofSection4E%	%RoofSection4M%	%RoofSection4ZCalc%	%RoofSection4P%	%RoofSection4HCalc%
5	%RoofSection5T%	%RoofSection5A%	%RoofSection5E%	%RoofSection5M%	%RoofSection5ZCalc%	%RoofSection5P%	%RoofSection5HCalc%
6	%RoofSection6T%	%RoofSection6A%	%RoofSection6E%	%RoofSection6M%	%RoofSection6ZCalc%	%RoofSection6P%	%RoofSection6HCalc%
7	%RoofSection7T%	%RoofSection7A%	%RoofSection7E%	%RoofSection7M%	%RoofSection7ZCalc%	%RoofSection7P%	%RoofSection7HCalc%
8	%RoofSection8T%	%RoofSection8A%	%RoofSection8E%	%RoofSection8M%	%RoofSection8ZCalc%	%RoofSection8P%	%RoofSection8HCalc%
9	%RoofSection9T%	%RoofSection9A%	%RoofSection9E%	%RoofSection9M%	%RoofSection9ZCalc%	%RoofSection9P%	%RoofSection9HCalc%
For email summaries:
Section 1- %RoofSection1M% modules at %RoofSection1P% kWh (%RoofSection1HCalc% Sun Hours)
Section 2- %RoofSection2M% modules at %RoofSection2P% kWh (%RoofSection2HCalc% Sun Hours)
Section 3- %RoofSection3M% modules at %RoofSection3P% kWh (%RoofSection3HCalc% Sun Hours)
Section 4- %RoofSection4M% modules at %RoofSection4P% kWh (%RoofSection4HCalc% Sun Hours)
Section 5- %RoofSection5M% modules at %RoofSection5P% kWh (%RoofSection5HCalc% Sun Hours)
Section 6- %RoofSection6M% modules at %RoofSection6P% kWh (%RoofSection6HCalc% Sun Hours)
Section 7- %RoofSection7M% modules at %RoofSection7P% kWh (%RoofSection7HCalc% Sun Hours)
Section 8- %RoofSection8M% modules at %RoofSection8P% kWh (%RoofSection8HCalc% Sun Hours)
Section 9- %RoofSection9M% modules at %RoofSection9P% kWh (%RoofSection9HCalc% Sun Hours)
), %AR%_SYSTEM_SUMMARY.txt
return

^0::
sendinput,Section 1- %RoofSection1M% modules at %RoofSection1P% kWh (%RoofSection1HCalc% Sun Hours)`nSection 2- %RoofSection2M% modules at %RoofSection2P% kWh (%RoofSection2HCalc% Sun Hours)`nSection 3- %RoofSection3M% modules at %RoofSection3P% kWh (%RoofSection3HCalc% Sun Hours)`nSection 4- %RoofSection4M% modules at %RoofSection4P% kWh (%RoofSection4HCalc% Sun Hours)`nSection 5- %RoofSection5M% modules at %RoofSection5P% kWh (%RoofSection5HCalc% Sun Hours)`nSection 6- %RoofSection6M% modules at %RoofSection6P% kWh (%RoofSection6HCalc% Sun Hours)`nSection 7- %RoofSection7M% modules at %RoofSection7P% kWh (%RoofSection7HCalc% Sun Hours)`nSection 8- %RoofSection8M% modules at %RoofSection8P% kWh (%RoofSection8HCalc% Sun Hours)`nSection 9- %RoofSection9M% modules at %RoofSection9P% kWh (%RoofSection9HCalc% Sun Hours)
return

; ###Commands button as seen above the tabs on the GUI###
ButtonCommands:
MsgBox,,InfoEntry- Revision No. %Version%,Hold Control and Left-click on items on the Clipboard tab to see tooltips about each one (tips disappear in three seconds)`n`nKey Shortcuts:`nCtrl+Space: Show/Hide`tCtrl+S: Save`nF5/Ctrl+R: Refresh`t`tWin+q: Exit`nCtrl+1: AR#`nCtrl+2: Last Name`nCtrl+3: Context-sensitive entries for Solmetric, Eco-X, MSI, AutoCAD, Pictometry, Nearmaps, and Google (when used in these last three, it enters the address)`nCtrl+4: Monthly totals for Salesforce`nNumpad Enter: Identical to Calculate button when double-tapped`n(For Solmetric and Exo-X make sure you click in the AR box before using the Ctrl+3 shortcut! For AutoCAD, click in the Initials box!)`n`nFor Solmetric, you can quickly change the inverter type by holding Shift (for Enphase), Control (for SolarEdge strings), or Alt (for SE's 10000 string) and right-clicking the Inverter number (note that this is intended for 260-Watt modules).`n`n(For MSI, click in the first box of the Contact section on the last page!)`n`nCtrl+[Numpad 1-9]: Enter that Roof Section's data into the Design Tool, Eco-X, or Salesforce.`n[For the Design Tool, start in the number-of-modules box; for Eco-X, start in the Average Roof Height box; for Salesforce, start in the Azimuth box. Keep in mind that the Salesforce shortcut defaults to the Trina 260-type modules!]`n`nIn AutoCAD, the up and down arrows can be used in the Drawing Properties page to move up and down between boxes. Clicking in a box first is required.`nAdditionally, the "``" (accent) key in the top-left corner of the keyboard behaves the same as the "`'" (single quote) key when AutoCAD is active, so entering dimensions can be done with the right hand on the number pad and the left hand in its rest position on the other side.`n`nFor Cobblestone users, the suneye editing has a shortcut added: "\" will do the job of shift+right to advance suneye. "p" will go to the previous suneye, so you can switch between the two faster.`n`nUT Features include: Change Inverters in PV Designer with Shift+[1-6]. Shift+[1-3] is SolarEdge. Shift+[4-6] is Fronius. In the Fronius Configurator, inverter can be changed with Shift+[1-3].
return

; ###The question-mark button next to the command button###
Button?:
AboutThis:
MsgBox,,InfoEntry- Revision No. %Version%,For quick information, control-left-click on something!`n`nFor use as a digital clipboard & text-entering shortcut tool for projects' commonly-used data.`n`tNote that it is *intended* to be both forward- and backward-compatible, so that the saved data from one version may be used with another. This is especially useful if the saved data is kept with the project, so that re-designed projects can be finished more quickly.`n`tThis GUI was largely made through trial-and-error over a period of months, with some portions extracted from the volunteered works made available by other AHK users (i.e., the AutoHotkey forums and knowledge pages).`n`t`t`t`t`t`-DŠ (T_T)
return

; ###Pressing Escape hides the GUI###
; Using Control-Space hides -and- shows it.
GuiEscape:
^Space::
GoSub ButtonHide
return

; ###The typical X-button in the top right corner###
; also, the Windows key & q closes the tool altogether
#q::
guiclose:
WinGetPos, winX, winY,
IniWrite, %BoardHide%, %A_MyDocuments%\IEUserSettings.ini, Main, BoardHide
IniWrite, %TaskBarHide%, %A_MyDocuments%\IEUserSettings.ini, Main, TaskBarHide
IniWrite, %winX%, %A_MyDocuments%\IEUserSettings.ini, Main, xPos
IniWrite, %winY%, %A_MyDocuments%\IEUserSettings.ini, Main, yPos
ExitApp
return

; ###KEYBOARD SHORTCUTS###
; ^ = Control
; ! = Alt
; # = Windows key
; + = Shift

; ###Context-sensitive commands###
; So long as the tool is active, it will
; replace all keystrokes of "`" as "'", which
; can make entering numbers of feet easier.
#IfWinActive, InfoEntry
`::'
#IfWinActive

; The Enter key on the Number pad performs the Calculate command
; when it is double-tapped within a 200 millisecond time span.
#IfWinActive, InfoEntry
$NumpadEnter::
if NPE_Presses > 0
{
    NPE_Presses += 1
    return
}
NPE_Presses = 1
SetTimer, KeyNPE, 200 ; 200ms wait
return

KeyNPE:
SetTimer, KeyNPE, off
if NPE_Presses = 1 ; The key was pressed once.
{
	send, {NumpadEnter}
}
else if NPE_Presses >= 2 ; The key was pressed twice or more.
{
	Gui, Submit, nohide
	GoSub CalculateAll
}
NPE_Presses = 0
return
#IfWinActive

; Control-S while the tool is active saves the information
#IfWinActive, InfoEntry
^s::
GoSub ButtonSave
return
#IfWinActive

; ###FIELD-ENTERING KEYBOARD SHORTCUTS###
; These are keyed for use for the March 2016 MDNY defaults.
; If the programs, module types, etc, change, these must
; also be changed to enter the data into the proper fields.

; Control-1 sends the AR number
^1::
Gui, Submit, NoHide
sendinput %AR%
return

; Control-2 sends the last name (for customer packets
; or emailing sales reps, etc)
^2:: ;Just the last name
Gui, Submit, NoHide
sendinput %LastName%
return

^3::
Gui, Submit, NoHide
Process, Exist, SunEye.exe
PID1 := errorlevel
;Eco-X's title page is "Eco X Client", use that instead of the PID
Process, Exist, acadlt.exe
PID2 := errorlevel
Process, Exist, MountingSystemsAppHost.exe
PID3 := errorlevel
Process, Exist, EXCEL.exe
PID4 := errorlevel
IfWinActive ahk_pid %PID4%
{
	gosub EnterEcoFastenExcel
	return
}
IfWinActive ahk_pid %PID1%
{
	gosub EnterSOLMETRIC
	return
}
IfWinActive, Eco X Client,
{
	gosub EnterECOX
	return
}
IfWinActive, EcoFasten Calculator,
{
	gosub EnterECOFasten
	return
}
IfWinActive ahk_pid %PID2%
{
	gosub EnterAUTOCAD
	return
}
IfWinActive ahk_pid %PID3%
{
	gosub EnterMSI
	return
}
#if, WinActive("Cobblestone CAD Home") || WinActive("New Tab") || WinActive("PhotoMaps") || WinActive("Pictometry Online")
{
	If Address contains #,
		sendraw %Address%,
	else
		sendinput %Address%,
	sendinput %City% %StateAB%
	return
}
return

EnterEcoFastenExcel:
sendinput %AR%
sendinput {return 3}
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
sendinput {return}
sendinput %City%
sendinput {return}
sendinput %StateAB%
sendinput {return}
sendinput %Zip%
return

EnterSOLMETRIC:
sendinput %AR%
sendinput {tab}
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
sendinput {tab 2}
sendinput %City%
sendinput {tab}
sendinput %StateAB%
sendinput {tab}
sendinput %Zip%
return

EnterECOX:
Gui, Submit, NoHide
sendinput %AR%
sendinput {tab}
sendinput {down}
sendinput {tab 2}
sendinput %AR%
sendinput {tab 4}
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
sendinput {tab 2}
sendinput %City%
sendinput {tab }
sendinput {down %StateFull%}
sendinput {tab 2}
sendinput %Zip%
return

EnterECOFasten:
Gui, Submit, NoHide
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
sendinput {tab}
sendinput %City%
sendinput {tab }
sendinput %State%
sendinput {tab}
sendinput %Zip%
sendinput {tab}
sendinput -
sendinput {tab}
sendinput -
return

EnterMSI:
sendinput %AR%
sendinput {shift down}{tab 2}{shift up}
sendinput %City%, %StateAB%, %Zip%
sendinput {shift}{tab 3}
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
return

EnterAUTOCAD:
Gui, Submit, NoHide
If StateAB contains UT,
	GoSub EnterAUTOCAD_UT
else {
sendinput %IN%
MouseMove, 0, 17, 0, R
Click
sendinput %AR%
MouseMove, 0, 17, 0, R
Click
sendinput %LastName%
MouseMove, 0, 17, 0, R
Click
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
MouseMove, 0, 17, 0, R
Click
sendinput %City%
MouseMove, 0, 34, 0, R
Click
sendinput %Zip%
}
return

#IfWinActive
EnterAUTOCAD_UT:
Gui, Submit, NoHide
sendinput %IN%
MouseMove, 0, 17, 0, R
Click
sendinput %LastName%
MouseMove, 0, 17, 0, R
Click
sendinput %AR%
MouseMove, 0, 17, 0, R
Click
If Address contains #,
	sendraw %Address%
else
	sendinput %Address%
MouseMove, 0, 17, 0, R
Click
sendinput %City%
MouseMove, 0, 34, 0, R
Click
sendinput %Zip%
MouseMove, 0, 51, 0, R
Click
sendinput %RoofSectionSigmaM%
sendinput {WheelDown 6}
MouseMove, 0, -17, 0, R
Click
sendinput %RoofSection1T%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection1A%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection1M%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection1HCalc%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection2T%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection2A%
sendinput {WheelDown 4}
MouseMove, 0, -187, 0, R
Click
sendinput %RoofSection2M%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection2HCalc%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection3T%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection3A%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection3M%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection3HCalc%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection4T%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection4A%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection4M%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection4HCalc%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection5T%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection5A%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection5M%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection5HCalc%
sendinput {WheelDown 5}
MouseMove, 0, -238, 0, R
Click
sendinput %RoofSection6T%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection6A%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection6M%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection6HCalc%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection7T%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection7A%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection7M%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection7HCalc%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection8T%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection8A%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection8M%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection8HCalc%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection9T%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection9A%
MouseMove, 0, 17, 0, R
Click
sendinput %RoofSection9M%
sendinput {WheelDown 1}
MouseMove, 0, -34, 0, R
Click
sendinput %RoofSection9HCalc%
SendInput, {WheelDown 7}
MouseMove, 0, 34, 0, R
Click
SendInput, %OffsetPercentage%
return

; ###Paste the monthly totals###
; Make sure to only use this in the salesforce page
^4:: ;Control-Four
Gui, Submit, nohide
sendinput %MonthlyTotal%
; Using "sendraw %MonthlyTotal%" works, but this version works faster
return

; ###SalesForce entries###
; The different number pad keys are used to send
; information corresponding to each roof section

^Numpad1::
If (RoofSection1Box = 1)
{Gui, Submit, NoHide
	EnteredRoofNumber = 1
	gosub EnterRoofSection
}
return

^Numpad2::
If (RoofSection2Box = 1)
{Gui, Submit, NoHide
	EnteredRoofNumber = 2
	gosub EnterRoofSection
}
return

^Numpad3::
If (RoofSection3Box = 1)
{Gui, Submit, NoHide
	EnteredRoofNumber = 3
	gosub EnterRoofSection
}
return

^Numpad4::
If (RoofSection4Box = 1)
{Gui, Submit, NoHide
	EnteredRoofNumber = 4
	gosub EnterRoofSection
}
return

^Numpad5::
If (RoofSection5Box = 1)
{Gui, Submit, NoHide
	EnteredRoofNumber = 5
	gosub EnterRoofSection
}
return

^Numpad6::
If (RoofSection6Box = 1)
{Gui, Submit, NoHide
	EnteredRoofNumber = 6
	gosub EnterRoofSection
}
return

^Numpad7::
If (RoofSection7Box = 1)
{Gui, Submit, NoHide
	EnteredRoofNumber = 7
	gosub EnterRoofSection
}
return

^Numpad8::
If (RoofSection8Box = 1)
{Gui, Submit, NoHide
	EnteredRoofNumber = 8
	gosub EnterRoofSection
}
return

^Numpad9::
If (RoofSection9Box = 1)
{Gui, Submit, NoHide
	EnteredRoofNumber = 9
	gosub EnterRoofSection
}
return

EnterRoofSection:
Gui, Submit, NoHide
	;First, get the Rafter Spacing by going through a loop
	TempSpacingNumber := EnteredRoofNumber
	TempLHDNumber := EnteredRoofNumber
	TempHeightNumber := EnteredRoofNumber
	loop,
	{
		if TempSpacingNumber > 0
		{
			if (RoofSection%TempSpacingNumber%R != "")
			{RSPC := RoofSection%TempSpacingNumber%R
			break
			return
			}
			else
				String := TempSpacingNumber
			{ TempSpacingNumber := TempSpacingNumber - 1
			}
		}
		else break
	}
	;Next, get the LHD for the roof section entered
	loop,
	{
		if TempLHDNumber > 0
		{
			if (RoofSection%TempLHDNumber%D != "")
			{LHD := RoofSection%TempLHDNumber%D
			break
			return
			}
			else
				String := TempLHDNumber
			{ TempLHDNumber := TempLHDNumber - 1
			}
		}
		else break
	}
	;Lastly, get the average height for the roof section
	loop,
	{
		if TempHeightNumber > 0
		{
			if (RoofSection%TempHeightNumber%V != "")
			{AVRH := RoofSection%TempHeightNumber%V
			break
			return
			}
			else
				String := TempHeightNumber
			{ TempHeightNumber := TempHeightNumber - 1
			}
		}
		else break
	}
	EnteredAzimuth := RoofSection%EnteredRoofNumber%A
	EnteredTilt := RoofSection%EnteredRoofNumber%T
	EnteredProduction := RoofSection%EnteredRoofNumber%P
	EnteredSize := 100 - RoofSection%EnteredRoofNumber%S
	EnteredModules := RoofSection%EnteredRoofNumber%M
	EnteredMounting := RoofSection%EnteredRoofNumber%F
	IfWinActive, Prototype CAD Edit ~ Salesforce - Unlimited Edition - Google Chrome,
	{
		sendinput %EnteredAzimuth%
		sendinput {tab}
		sendinput %EnteredTilt%
		sendinput {tab 3}
		sendinput %EnteredProduction%
		sendinput {tab}
		sendinput %EnteredSize%
		sendinput {tab}
		;go down a specific number of times
		;to get to the right type of module
		sendinput {down %DownNumber%}
		sendinput {tab}
		sendinput  %EnteredModules%
		sendinput {tab}
		return
	}
	; Design_Tool and Design Tool cannot be differentiated
	; by default, so we're just going with "Design"
	Process, Exist, Excel.exe
	PID4 := errorlevel
	IfWinActive ahk_pid %PID4%
	{
		sendinput %EnteredModules%
		sendinput {tab}
		sendinput %EnteredTilt%
		sendinput {tab}
		sendinput %EnteredAzimuth%
		sendinput {tab}
		sendinput %EnteredProduction%
		; sendinput {tab 3} (Not needed in Utah.)
		; sendinput %EnteredMounting% (Not needed in Utah.)
		sendinput {Enter}
		; sendinput +{tab 3}
		return
	}
	IfWinActive, Eco X Client,
	{
	sendinput %AVRH%
	sendinput {tab 2}
	sendinput %LHD%
	sendinput {tab 2}
	sendinput %EnteredTilt%
	sendinput {tab 6}
	sendinput %RSPC%
	sendinput {tab 2}
	sendinput {space}
	if (StateFull = 21) or (StateFull = 31)
		{
		sendinput {tab}
		sendinput {space}
		}
	}
return

; ###MOUSE-OVER HELPS###
#IfWinActive, InfoEntry
^LButton::
MouseGetPos,,,,OutputVarCtl
ControlGetText, TextVar, %OutputVarCtl%, InfoEntry
if (TextVar = "N" or TextVar = "S" or TextVar = "E" or TextVar = "W")
{tooltip Calculated azimuths in 90-degree increments
}
else if TextVar = Down Number:
{tooltip How many times you would have to press 'down'`nto select module type in Salesforce
}
else if TextVar = DI:
{tooltip Designer's initials
}
else if TextVar = `n`n`n`n`n`n`n`n`nSummary Export
{tooltip Export the system's roof section data to a text file.
}
else if TextVar = `n`n`n`n`n`nClipboard
{tooltip Show/hide the clipboard to the right
}
else if TextVar = `n`n`n`n`n`nTaskbar
{tooltip Show/hide the GUI's Taskbar icon and text
}
else if TextVar = `n`n`n`n`n`n`nCommands
{tooltip View commands and shortcuts
}
else if TextVar = `n`n`n`n`nRefresh
{tooltip Refresh the tool without saving anything
}
else if TextVar = `n`n`n`n`nSave
{tooltip Save information to a separate .ini file
}
else if TextVar = `n`n`n`n`nCLEAR
{tooltip Clear/reset all fields
}
else if TextVar = `n`n`n`n`nHide
{tooltip Hide the GUI `n(show again with Ctrl+Space)
}
else if TextVar = `n`n`n`n`n?
{tooltip About the tool
}
else if TextVar = #:
{tooltip Service / AR Number for the account
}
else if TextVar = Last Name:
{tooltip Customer's last name
}
else if TextVar = Goal:
{tooltip Production goal as specified in the Design Tool.
}
else if TextVar = Usage:
{tooltip Annual usage as specified in the Design Tool;`nused for calculating offset percentage and`nlater in the CAD page.
}
else if TextVar = Totals:
{tooltip Totals of each roof section's information.`nCheck or uncheck a roof section below`nto add or remove it from the calculation.
}
else if TextVar = Eyes
{tooltip Sun eyes belonging to`na roof section (i.e. 1-5; 6-10).
}
else if TextVar = Tilt°
{tooltip Tilt in degrees of each roof section.
}
else if TextVar = Azim°
{tooltip Azimuth in degrees of each roof section.
}
else if TextVar = Mods
{tooltip Number of modules on each roof section.
}
else if TextVar = Prod.
{tooltip Solmetric-estimated production for each roof section.
}
else if TextVar = (Size)
{tooltip Calculated size of each roof section.
}
else if TextVar = (Hours)
{tooltip Calculated sun-hours of each roof section.
}
else if TextVar = S`%
{tooltip Solmetric-reported shading percentage`nfor each roof section.
}
else if TextVar = AP
{tooltip Number of attachment points / mounting feet`nfor each roof section.
}
else if TextVar = Avg'
{tooltip Average roof height in whole feet for each roof section;`nif left empty subsequent roof sections will inherit the`nnumber from the previous roof section.
}
else if TextVar = LHD'
{tooltip Least horizontal dimension in whole feet for each`nroof section; will also be inherited if left blank.
}
else if TextVar = Ra"
{tooltip Rafter spacing in whole inches for each roof section;`nwill also be inherited if left blank.
}
else if TextVar = Module Rating (Watts):
{tooltip Module Rating in Watts (i.e. 250; 255; or 260).
}
else if TextVar = Module Efficiency`%:
{tooltip Module minimum efficiency as a percentage (0.80; 0.85; 0.95).
}
else if TextVar = Per-Module Minimum Production:
{tooltip Minimum per-module production; calculated from above rating and efficiency.
}
else if TextVar = Section Minimum:
{tooltip Minimum production in sun hours for each roof section.
}
else if TextVar = System Minimum:
{tooltip Minimum production in sun hours for the entire system.
}
else tooltip Ctrl-Click on something to see available information.
; Set a timer to remove the tooltip after 3 seconds
SetTimer, RemoveToolTip, 3000
return
#IfWinActive

; Removes the tooltip and stops the timer
RemoveToolTip:
SetTimer, RemoveToolTip, Off
ToolTip
return

; Left-foot symbol- use the ` as the ' key
; in AutoCAD for faster dimension entry

#IfWinActive ahk_class AfxMDIFrame100u
`::'
#IfWinActive

Up::
Process, Exist, acadlt.exe
PID_DP := errorlevel
IfWinActive ahk_pid %PID_DP%
{
	MouseMove, 0, -17, 0, R
	Click
	return
}
else send {Up}
return

Down::
Process, Exist, acadlt.exe
PID_DP := errorlevel
IfWinActive ahk_pid %PID_DP%
{
	MouseMove, 0, 17, 0, R
	Click
	return
}
else send {Down}
return

; This allows F5 or Control-R to refresh the tool/solaredge inverter
~F5::
^r::
^RButton::
IfWinActive, InfoEntry
{
GoSub ButtonRefresh
return
}
Process, Exist, SunEye.exe
PID_S := errorlevel
IfWinActive ahk_pid %PID_S%
{
	GoSub CalculateAll
	If (RoofSectionSigmaM <= 7)
		{Msgbox, Too few modules!`nCancel the design!
		return
		}
	else
	{MouseClick, right
	sendinput {down}
	sendinput {enter}
	sendinput {tab}
	sendinput "solaredge"
	sendinput {tab}
	Sleep, 300
		If ((RoofSectionSigmaM >= 8) and (RoofSectionSigmaM <= 19))
			sendinput {down 28} ;Set3800Inverter
		If ((RoofSectionSigmaM >= 20) and (RoofSectionSigmaM <= 30))
			sendinput {down 31} ;Set6000Inverter
		If ((RoofSectionSigmaM >= 31) and (RoofSectionSigmaM <= 38))
			sendinput {down 32} ;Set7600Inverter
		If ((RoofSectionSigmaM >= 39) and (RoofSectionSigmaM <= 57))
			sendinput {down 27} ;Set11400Inverter
		If ((RoofSectionSigmaM >= 58) and (RoofSectionSigmaM <= 76))
			sendinput {down 32} ;Set7600Inverter
	sendinput {enter}
	Sleep, 700
	MouseClick, left
	sendinput {left}
	sendinput {down}
	MouseMove, 0, 16, 0, R
	return
	}
}
return

#IfWinActive Solmetric SunEye
+RButton::
	;Keywait Shift
	Keywait RButton
	MouseClick, right
	sendinput {down}
	sendinput {enter}
	sendinput {tab}
	sendinput "enphase"
	sendinput {tab}
	sendinput {down 16}
	sendinput {enter}
	Sleep, 700
	MouseClick, left
	sendinput {left}
	sendinput {down}
	return
return
#IfWinActive

!RButton::
Process, Exist, SunEye.exe
PID_S := errorlevel
IfWinActive ahk_pid %PID_S%
{
	MouseClick, right
	sendinput {down}
	sendinput {enter}
	sendinput {tab}
	sendinput "solaredge"
	sendinput {tab}
	sendinput {down 26}
	sendinput {enter}
	Sleep, 700
	MouseClick, left
	sendinput {left}
	sendinput {down}
	return
}
else send {Alt}{RButton}
return

GuiButtonIcon(Handle, File, Index := 1, Options := "" )
{
	RegExMatch(Options, "i)w\K\d+", W), (W="") ? W := 16 :
	RegExMatch(Options, "i)h\K\d+", H), (H="") ? H := 16 :
	RegExMatch(Options, "i)s\K\d+", S), S ? W := H := S :
	RegExMatch(Options, "i)l\K\d+", L), (L="") ? L := 0 :
	RegExMatch(Options, "i)t\K\d+", T), (T="") ? T := 0 :
	RegExMatch(Options, "i)r\K\d+", R), (R="") ? R := 0 :
	RegExMatch(Options, "i)b\K\d+", B), (B="") ? B := 0 :
	RegExMatch(Options, "i)a\K\d+", A), (A="") ? A := 4 :
	Psz := A_PtrSize = "" ? 4 : A_PtrSize, DW := "UInt", Ptr := A_PtrSize = "" ? DW : "Ptr"
	VarSetCapacity( button_il, 20 + Psz, 0 )
	NumPut( normal_il := DllCall( "ImageList_Create", DW, W, DW, H, DW, 0x21, DW, 1, DW, 1 ), button_il, 0, Ptr )	; Width & Height
	NumPut( L, button_il, 0 + Psz, DW )		; Left Margin
	NumPut( T, button_il, 4 + Psz, DW )		; Top Margin
	NumPut( R, button_il, 8 + Psz, DW )		; Right Margin
	NumPut( B, button_il, 12 + Psz, DW )	; Bottom Margin	
	NumPut( A, button_il, 16 + Psz, DW )	; Alignment
	SendMessage, BCM_SETIMAGELIST := 5634, 0, &button_il,, AHK_ID %Handle%
	return IL_Add( normal_il, File, Index )
}

ButtonClipboard:
if (BoardHide = 1)
	{BoardHide = 0
	Gui, Hide
	Gui, Show, w880
	return
	} else {BoardHide = 1
	Gui, Hide
	Gui, Show, w560
	return
	}
return

ButtonTaskbar:
if (TaskBarHide = 1)
	{TaskBarHide = 0
	Gui, +ToolWindow
	Gui, Show,
	return
	} else {TaskBarHide = 1
	Gui, -ToolWindow
	Gui, Show,
	return
	}
return

;^ = Control
;# = WinKey
;! = Alt
;+ = Shift

~^!LButton::
Gui, Submit, NoHide
Clipboard=
keywait LButton
IfWinActive ahk_class AcrobatSDIWindow
{
	sleep 100
	;Send ^c
	Send {Ctrl Down}c{Ctrl Up}
	sleep 50
	GuiControl,, MonthlyTotalraw,%Clipboard%
	sleep 50
	Send {Ctrl Down}c{Ctrl Up}
	sleep 50
	GuiControl,, MonthlyTotalraw,%Clipboard%
	sleep 50
	;gosub MonthTotal ;same as next three lines
	Gui, Submit, nohide
	StringReplace, MonthlyTotal, MonthlyTotalRaw, `n, `t, A
	GuiControl,, MonthlyTotalFormatted,%MonthlyTotal%
	return
}
return

#IfWinActive Cobblestone CAD Home - Google Chrome
\::
	Send {Right 1}
return
;p::
;	Send {Left 1}
;return
#IfWinActive

!F2::
send %FileName%
return

; NATES CODE UT
; ######################################
; #                                    #
; #            PLEASE READ             #
; #                                    #
; ######################################
;
; This code is not officially written by DS, the author of this program.
; I however feel it has a place in our tool so I'm putting it here.

; Send SolarEdge 3800 in PV Designer
#IfWinActive Select Inverter
+1::
sendInput, {tab}
sendInput, s
sendInput, {tab}
sendInput, {home}
sendInput, {down 2}
return

; Send SolarEdge 7600 in PV Designer
#IfWinActive Select Inverter
+2::
sendInput, {tab}
sendInput, s
sendInput, {tab}
sendInput, {home}
sendInput, {down 6}
return

; Send SolarEdge 11400 in PV Designer
#IfWinActive Select Inverter
+3::
sendInput, {tab}
sendInput, s
sendInput, {tab}
sendInput, {home}
sendInput, {down 1}
return

; Send Fronius 3.8-1 in PV Designer
#IfWinActive Select Inverter
+4::
sendInput, {tab}
sendInput, f
sendInput, {tab}
sendInput, {end}
sendInput, {up 4}
return

; Send Fronius 6.0-1 in PV Designer
#IfWinActive Select Inverter
+5::
sendInput, {tab}
sendInput, f
sendInput, {tab}
sendInput, {end}
sendInput, {up 2}
return

; Send Fronius 7.6-1 in PV Designer
#IfWinActive Select Inverter
+6::
sendInput, {tab}
sendInput, f
sendInput, {tab}
sendInput, {end}
sendInput, {up 1}
return

; To send Fronius 3.8-1 in Fronius Configurator
#IfWinActive Fronius IG Configuration Tool - Google Chrome
+1::
sendInput, {tab 3}
sendInput, {up 5}
sendInput, {tab 2}
sendInput, Jinko
sendInput, {tab}
sendInput, JKM-265P
sendInput, {tab 2}
sendInput, {up 1}
sendInput, {tab 1}
sendInput, {up 1}
sendInput, {tab 1}
sendInput, {space}
return

; To send Fronius 6.0-1 in Fronius Configurator
#IfWinActive Fronius IG Configuration Tool - Google Chrome
+2::
sendInput, {tab 3}
sendInput, {up 3}
sendInput, {tab 2}
sendInput, Jinko
sendInput, {tab}
sendInput, JKM-265P
sendInput, {tab 2}
sendInput, {up 1}
sendInput, {tab 1}
sendInput, {up 1}
sendInput, {tab 1}
sendInput, {space}
return

; To send Fronius 7.6-1 in Fronius Configurator
#IfWinActive Fronius IG Configuration Tool - Google Chrome
+3::
sendInput, {tab 3}
sendInput, {up 2}
sendInput, {tab 2}
sendInput, Jinko
sendInput, {tab}
sendInput, JKM-265P
sendInput, {tab 2}
sendInput, {up 1}
sendInput, {tab 1}
sendInput, {up 1}
sendInput, {tab 1}
sendInput, {space}
return