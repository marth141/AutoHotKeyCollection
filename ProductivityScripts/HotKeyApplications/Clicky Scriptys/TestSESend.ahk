; Insert from N.Casados
; Send SolarEdge 3800 in PV Designer
#IfWinActive,Select Inverter
+1::
sendInput, {tab}
sendInput, s
sendInput, {tab}
sendInput, {home}
sendInput, {down 2}
return
#IfWinActive

; Send SolarEdge 7600 in PV Designer
#IfWinActive,Select Inverter
+2::
sendInput, {tab}
sendInput, s
sendInput, {tab}
sendInput, {home}
sendInput, {down 6}
return

; Send SolarEdge 11400 in PV Designer
#IfWinActive,Select Inverter
+3::
sendInput, {tab}
sendInput, s
sendInput, {tab}
sendInput, {home}
sendInput, {down 1}
return