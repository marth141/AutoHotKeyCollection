; Fronius Script for Choosing Fronius Inverters easy.

; To send Fronius 3.8-1
#IfWinActive Fronius IG Configuration Tool - Google Chrome
+1::
send, {tab 3}
send, {up 5}
send, {tab 2}
send, Jinko
send, {tab}
send, JKM-265P
send, {tab 4}
send, {space}
return

; To send Fronius 6.0-1
#IfWinActive Fronius IG Configuration Tool - Google Chrome
+2::
send, {tab 3}
send, {up 3}
send, {tab 2}
send, Jinko
send, {tab}
send, JKM-265P
send, {tab 4}
send, {space}
return

; To send Fronius 7.6-1
#IfWinActive Fronius IG Configuration Tool - Google Chrome
+3::
send, {tab 3}
send, {up 2}
send, {tab 2}
send, Jinko
send, {tab}
send, JKM-265P
send, {tab 4}
send, {space}
return

; Double press escape for AutoCAD
Esc::
Process, Exist, acadlt.exe
PID_DP := errorlevel
IfWinActive ahk_pid %PID_DP%
	{
		Send, {Esc 2}
	}
return

; Set up a folder to be for pictures
#IfWinActive Site-Survey Properties
+1::
send, ^+{tab 1}
send, p
send, {tab}
send, {space}
send, {enter}
return

; Send SolarEdge 3800
#IfWinActive ahk_class HwndWrapper[SunEye.exe;;7ab2d8f2-a57a-4e97-b213-300ff4394895]
+1::
send, {tab}
send, s
send, {tab}
send, {home}
send, {down 2}
return

; Send SolarEdge 7600
#IfWinActive Select Inverter
+2::
send, {tab}
send, s
send, {tab}
send, {home}
send, {down 6}
return

; Send SolarEdge 11400
#IfWinActive Select Inverter
+3::
send, {tab}
send, s
send, {tab}
send, {home}
send, {down 1}
return

; Send Fronius 3.8-1
#IfWinActive Select Inverter
+4::
send, {tab}
send, f
send, {tab}
send, {end}
send, {up 4}
return

; Send Fronius 6.0-1
#IfWinActive Select Inverter
+5::
send, {tab}
send, f
send, {tab}
send, {end}
send, {up 2}
return

; Send Fronius 7.6-1
#IfWinActive Select Inverter
+6::
send, {tab}
send, f
send, {tab}
send, {end}
send, {up 1}
return