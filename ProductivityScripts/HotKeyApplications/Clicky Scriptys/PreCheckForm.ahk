﻿; This script was created using Pulover's Macro Creator
; www.macrocreator.com

#NoEnv
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Window
SendMode Input
#SingleInstance Force
SetTitleMatchMode 2
#WinActivateForce
SetControlDelay 1
SetWinDelay 0
SetKeyDelay -1
SetMouseDelay -1
SetBatchLines -1


!F3::
PreCheck:
IfWinActive, Predesign Checklist v1.3 - Google Chrome
{
    WinActivate, Predesign Checklist v1.3 - Google Chrome ahk_class Chrome_WidgetWin_1
    Sleep, 10
    Send, {Tab}
    Sleep, 10
    Send, {Tab}
    Sleep, 10
    Send, {Space}
    Sleep, 10
    Send, {Tab}
    Sleep, 10
    Send, {Numpad2}
    Sleep, 10
    Send, {Numpad2}
    Sleep, 10
    Send, {Numpad8}
    Sleep, 10
    Send, {Numpad2}
    Sleep, 10
    Send, {Numpad0}
    Sleep, 10
    Send, {Numpad1}
    Sleep, 10
    Send, {Numpad7}
    Sleep, 10
    Send, {Tab}
    Sleep, 10
    Send, {N}
    Sleep, 10
    Send, {a}
    Sleep, 10
    Send, {t}
    Sleep, 10
    Send, {h}
    Sleep, 10
    Send, {a}
    Sleep, 10
    Send, {n}
    Sleep, 10
    Send, {Space}
    Sleep, 10
    Send, {C}
    Sleep, 10
    Send, {a}
    Sleep, 10
    Send, {s}
    Sleep, 10
    Send, {a}
    Sleep, 10
    Send, {d}
    Sleep, 10
    Send, {o}
    Sleep, 10
    Send, {s}
    Sleep, 10
    Send, {Tab}
    Sleep, 10
    Send, {h}
    Sleep, 10
    Send, {h}
    Sleep, 10
    Send, {Tab}
    Sleep, 10
}
Return

